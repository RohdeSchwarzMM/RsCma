FmStereo
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Demodulation.FmStereo.FmStereoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.limit.demodulation.fmStereo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_FmStereo_Adeviation.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_FmStereo_Mdeviation.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_FmStereo_PfError.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_FmStereo_PiDeviation.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_FmStereo_RdsDeviation.rst