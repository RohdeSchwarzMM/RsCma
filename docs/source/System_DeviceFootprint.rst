DeviceFootprint
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:DFPRint

.. code-block:: python

	SYSTem:DFPRint



.. autoclass:: RsCma.Implementations.System.DeviceFootprint.DeviceFootprintCls
	:members:
	:undoc-members:
	:noindex: