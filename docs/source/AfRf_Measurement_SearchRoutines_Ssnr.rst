Ssnr
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Ssnr.SsnrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.searchRoutines.ssnr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_SearchRoutines_Ssnr_Average.rst
	AfRf_Measurement_SearchRoutines_Ssnr_Current.rst
	AfRf_Measurement_SearchRoutines_Ssnr_Deviation.rst
	AfRf_Measurement_SearchRoutines_Ssnr_Maximum.rst