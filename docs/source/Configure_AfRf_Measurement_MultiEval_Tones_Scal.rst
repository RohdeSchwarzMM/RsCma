Scal
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SCAL:STANdard
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SCAL:CFGenerator

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SCAL:STANdard
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SCAL:CFGenerator



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Scal.ScalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.tones.scal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Tones_Scal_Frequency.rst
	Configure_AfRf_Measurement_MultiEval_Tones_Scal_UserDefined.rst