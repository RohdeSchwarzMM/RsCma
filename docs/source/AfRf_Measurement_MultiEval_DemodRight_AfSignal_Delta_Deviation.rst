Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodRight.AfSignal.Delta.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: