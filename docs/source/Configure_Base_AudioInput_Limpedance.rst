Limpedance
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:AIN<nr>:LIMPedance

.. code-block:: python

	CONFigure:BASE:AIN<nr>:LIMPedance



.. autoclass:: RsCma.Implementations.Configure.Base.AudioInput.Limpedance.LimpedanceCls
	:members:
	:undoc-members:
	:noindex: