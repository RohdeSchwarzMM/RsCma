Extreme
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:EXTReme
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:EXTReme
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:EXTReme

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:EXTReme
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:EXTReme
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:EXTReme



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.Voip.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: