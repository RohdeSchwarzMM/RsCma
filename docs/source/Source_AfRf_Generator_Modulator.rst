Modulator
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:MODulator:FDEViation
	single: SOURce:AFRF:GENerator<Instance>:MODulator:PDEViation
	single: SOURce:AFRF:GENerator<Instance>:MODulator:MDEPth
	single: SOURce:AFRF:GENerator<Instance>:MODulator

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:MODulator:FDEViation
	SOURce:AFRF:GENerator<Instance>:MODulator:PDEViation
	SOURce:AFRF:GENerator<Instance>:MODulator:MDEPth
	SOURce:AFRF:GENerator<Instance>:MODulator



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Modulator.ModulatorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.modulator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Modulator_Enable.rst
	Source_AfRf_Generator_Modulator_FmStereo.rst