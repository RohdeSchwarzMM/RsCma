Delta
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FREQuency:DELTa:MODE
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FREQuency:DELTa:USER
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FREQuency:DELTa:MEASured

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FREQuency:DELTa:MODE
	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FREQuency:DELTa:USER
	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FREQuency:DELTa:MEASured



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.Frequency.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.demodulation.frequency.delta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Demodulation_Frequency_Delta_Update.rst