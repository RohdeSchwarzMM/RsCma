Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SCAL:FREQuency:RESet
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SCAL:FREQuency

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SCAL:FREQuency:RESet
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SCAL:FREQuency



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Scal.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: