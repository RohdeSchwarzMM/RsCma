Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:SSNR:DEViation
	single: READ:AFRF:MEASurement<Instance>:SROutines:SSNR:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:SSNR:DEViation
	READ:AFRF:MEASurement<Instance>:SROutines:SSNR:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Ssnr.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: