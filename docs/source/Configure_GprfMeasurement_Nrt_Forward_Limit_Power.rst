Power
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:FWARd:LIMit:POWer

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRT:FWARd:LIMit:POWer



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.Forward.Limit.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: