Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:FREQuency:DELTa:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:FREQuency:DELTa:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:FREQuency:DELTa:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:FREQuency:DELTa:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Voip.Frequency.Delta.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: