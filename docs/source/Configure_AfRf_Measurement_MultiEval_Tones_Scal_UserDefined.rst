UserDefined
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SCAL:UDEFined:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SCAL:UDEFined:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Scal.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: