Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:SQUality:TRACe
	single: READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:SQUality:TRACe

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:SQUality:TRACe
	READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:SQUality:TRACe



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsquelch.SignalQuality.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: