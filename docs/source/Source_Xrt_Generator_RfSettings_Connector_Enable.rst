Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:XRT:GENerator<Instance>:RFSettings:CONNector<nr>:ENABle

.. code-block:: python

	SOURce:XRT:GENerator<Instance>:RFSettings:CONNector<nr>:ENABle



.. autoclass:: RsCma.Implementations.Source.Xrt.Generator.RfSettings.Connector.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: