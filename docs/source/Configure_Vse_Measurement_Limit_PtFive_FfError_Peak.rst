Peak
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:PTFive:FFERor:PEAK

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:PTFive:FFERor:PEAK



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.PtFive.FfError.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: