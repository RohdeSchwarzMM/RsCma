UsbPower
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.UsbPower.UsbPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.fft.demodulation.usbPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Fft_Demodulation_UsbPower_Average.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation_UsbPower_Current.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation_UsbPower_Maximum.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation_UsbPower_Minimum.rst