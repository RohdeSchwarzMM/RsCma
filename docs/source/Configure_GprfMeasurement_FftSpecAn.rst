FftSpecAn
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:TOUT
	single: CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:AMODe
	single: CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:DETector
	single: CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:FFTLength
	single: CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:FSPan
	single: CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:MOEXception
	single: CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:REPetition
	single: CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:RCOupling
	single: CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:SCOunt

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:TOUT
	CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:AMODe
	CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:DETector
	CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:FFTLength
	CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:FSPan
	CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:MOEXception
	CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:REPetition
	CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:RCOupling
	CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:SCOunt



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.FftSpecAn.FftSpecAnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.fftSpecAn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_FftSpecAn_Marker.rst
	Configure_GprfMeasurement_FftSpecAn_PeakSearch.rst