Application
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:DISPlay:APPLication:SELect

.. code-block:: python

	CONFigure:DISPlay:APPLication:SELect



.. autoclass:: RsCma.Implementations.Configure.Display.Application.ApplicationCls
	:members:
	:undoc-members:
	:noindex: