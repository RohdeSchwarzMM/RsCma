Measurement
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: STOP:VSE:MEASurement<Instance>
	single: ABORt:VSE:MEASurement<Instance>

.. code-block:: python

	STOP:VSE:MEASurement<Instance>
	ABORt:VSE:MEASurement<Instance>



.. autoclass:: RsCma.Implementations.Vse.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.vse.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Vse_Measurement_Cons.rst
	Vse_Measurement_Dmr.rst
	Vse_Measurement_Dpmr.rst
	Vse_Measurement_Ediagram.rst
	Vse_Measurement_Evm.rst
	Vse_Measurement_FdError.rst
	Vse_Measurement_Fdeviation.rst
	Vse_Measurement_FfError.rst
	Vse_Measurement_Lte.rst
	Vse_Measurement_Merror.rst
	Vse_Measurement_Nxdn.rst
	Vse_Measurement_Perror.rst
	Vse_Measurement_PowerVsTime.rst
	Vse_Measurement_PtFive.rst
	Vse_Measurement_RfCarrier.rst
	Vse_Measurement_Sdistribute.rst
	Vse_Measurement_Spectrum.rst
	Vse_Measurement_State.rst
	Vse_Measurement_Tetra.rst