Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:ACP:POWer:MAXimum
	single: READ:GPRF:MEASurement<Instance>:ACP:POWer:MAXimum
	single: CALCulate:GPRF:MEASurement<Instance>:ACP:POWer:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:ACP:POWer:MAXimum
	READ:GPRF:MEASurement<Instance>:ACP:POWer:MAXimum
	CALCulate:GPRF:MEASurement<Instance>:ACP:POWer:MAXimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Power.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: