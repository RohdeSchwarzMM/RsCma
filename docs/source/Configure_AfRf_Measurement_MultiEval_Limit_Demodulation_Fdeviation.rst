Fdeviation
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Demodulation.Fdeviation.FdeviationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.limit.demodulation.fdeviation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_Fdeviation_Peak.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_Fdeviation_Rms.rst