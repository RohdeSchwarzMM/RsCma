TocLength
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:TONes:DCS:TOCLength

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:TONes:DCS:TOCLength



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Tones.Dcs.TocLength.TocLengthCls
	:members:
	:undoc-members:
	:noindex: