AfFft
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:AFFFt:SCOunt

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:AFFFt:SCOunt



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.AfFft.AfFftCls
	:members:
	:undoc-members:
	:noindex: