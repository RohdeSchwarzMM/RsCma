RfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:VOR:RFSettings:LEVel
	single: SOURce:AVIonics:GENerator<Instance>:VOR:RFSettings:FREQuency

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:VOR:RFSettings:LEVel
	SOURce:AVIonics:GENerator<Instance>:VOR:RFSettings:FREQuency



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Vor.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.vor.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_Vor_RfSettings_RfOut.rst