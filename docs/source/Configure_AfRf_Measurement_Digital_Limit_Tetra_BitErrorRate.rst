BitErrorRate
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:LIMit:TETRa:BERate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:LIMit:TETRa:BERate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Limit.Tetra.BitErrorRate.BitErrorRateCls
	:members:
	:undoc-members:
	:noindex: