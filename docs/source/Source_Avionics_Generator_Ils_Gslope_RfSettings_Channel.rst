Channel
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:RFSettings:CHANnel

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:RFSettings:CHANnel



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Gslope.RfSettings.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: