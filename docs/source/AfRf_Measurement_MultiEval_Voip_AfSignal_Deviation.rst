Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Voip.AfSignal.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: