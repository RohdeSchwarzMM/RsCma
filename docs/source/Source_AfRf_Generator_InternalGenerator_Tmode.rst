Tmode
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:TMODe

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:TMODe



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.Tmode.TmodeCls
	:members:
	:undoc-members:
	:noindex: