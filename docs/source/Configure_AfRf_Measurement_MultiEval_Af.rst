Af
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:AF:SCOunt

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:AF:SCOunt



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Af.AfCls
	:members:
	:undoc-members:
	:noindex: