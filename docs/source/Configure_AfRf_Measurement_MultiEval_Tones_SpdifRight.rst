SpdifRight
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight:MODE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight:MODE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.SpdifRight.SpdifRightCls
	:members:
	:undoc-members:
	:noindex: