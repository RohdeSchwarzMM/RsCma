First
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AOUT:FIRSt:LEVel

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AOUT:FIRSt:LEVel



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioOutput.First.FirstCls
	:members:
	:undoc-members:
	:noindex: