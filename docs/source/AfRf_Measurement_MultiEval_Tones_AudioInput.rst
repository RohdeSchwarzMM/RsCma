AudioInput<AudioInput>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.afRf.measurement.multiEval.tones.audioInput.repcap_audioInput_get()
	driver.afRf.measurement.multiEval.tones.audioInput.repcap_audioInput_set(repcap.AudioInput.Nr1)



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.AudioInput.AudioInputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.tones.audioInput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Tones_AudioInput_Repetitions.rst
	AfRf_Measurement_MultiEval_Tones_AudioInput_Sequence.rst