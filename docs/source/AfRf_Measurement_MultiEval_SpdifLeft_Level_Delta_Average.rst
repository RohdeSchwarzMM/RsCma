Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:LEVel:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:LEVel:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:LEVel:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:LEVel:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifLeft.Level.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: