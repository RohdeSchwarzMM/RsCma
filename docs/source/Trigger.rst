Trigger
----------------------------------------





.. autoclass:: RsCma.Implementations.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_AfRf.rst
	Trigger_Base.rst
	Trigger_GprfMeasurement.rst