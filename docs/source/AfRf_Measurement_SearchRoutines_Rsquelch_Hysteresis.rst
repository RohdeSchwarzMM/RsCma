Hysteresis
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:HYSTeresis
	single: CALCulate:AFRF:MEASurement<Instance>:SROutines:RSQuelch:HYSTeresis
	single: READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:HYSTeresis

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:HYSTeresis
	CALCulate:AFRF:MEASurement<Instance>:SROutines:RSQuelch:HYSTeresis
	READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:HYSTeresis



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsquelch.Hysteresis.HysteresisCls
	:members:
	:undoc-members:
	:noindex: