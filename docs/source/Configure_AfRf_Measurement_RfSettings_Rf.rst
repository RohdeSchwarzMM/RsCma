Rf
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:RFSettings:RF:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:RFSettings:RF:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.RfSettings.Rf.RfCls
	:members:
	:undoc-members:
	:noindex: