Weighting
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FILTer:WEIGhting

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FILTer:WEIGhting



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.FilterPy.Weighting.WeightingCls
	:members:
	:undoc-members:
	:noindex: