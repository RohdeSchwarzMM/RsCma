TypePy
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:IQRecorder:FILTer:TYPE

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:IQRecorder:FILTer:TYPE



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.IqRecorder.FilterPy.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: