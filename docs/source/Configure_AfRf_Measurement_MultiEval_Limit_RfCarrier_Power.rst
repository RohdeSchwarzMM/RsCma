Power
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:RFCarrier:POWer

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:RFCarrier:POWer



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.RfCarrier.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: