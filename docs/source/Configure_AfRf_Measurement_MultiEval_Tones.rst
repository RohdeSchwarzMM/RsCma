Tones
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.TonesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.tones.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Tones_AudioInput.rst
	Configure_AfRf_Measurement_MultiEval_Tones_Dcs.rst
	Configure_AfRf_Measurement_MultiEval_Tones_Demodulation.rst
	Configure_AfRf_Measurement_MultiEval_Tones_Dialing.rst
	Configure_AfRf_Measurement_MultiEval_Tones_Dtmf.rst
	Configure_AfRf_Measurement_MultiEval_Tones_Fdialing.rst
	Configure_AfRf_Measurement_MultiEval_Tones_Scal.rst
	Configure_AfRf_Measurement_MultiEval_Tones_SelCall.rst
	Configure_AfRf_Measurement_MultiEval_Tones_SpdifLeft.rst
	Configure_AfRf_Measurement_MultiEval_Tones_SpdifRight.rst
	Configure_AfRf_Measurement_MultiEval_Tones_Voip.rst