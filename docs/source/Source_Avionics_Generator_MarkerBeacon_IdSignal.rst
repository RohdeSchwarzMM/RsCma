IdSignal
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:MBEacon:IDSignal:ENABle
	single: SOURce:AVIonics:GENerator<Instance>:MBEacon:IDSignal:MDEPth
	single: SOURce:AVIonics:GENerator<Instance>:MBEacon:IDSignal:FREQuency

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:MBEacon:IDSignal:ENABle
	SOURce:AVIonics:GENerator<Instance>:MBEacon:IDSignal:MDEPth
	SOURce:AVIonics:GENerator<Instance>:MBEacon:IDSignal:FREQuency



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.MarkerBeacon.IdSignal.IdSignalCls
	:members:
	:undoc-members:
	:noindex: