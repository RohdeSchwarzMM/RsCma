RfLevel
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsensitivity.RfLevel.RfLevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.searchRoutines.rsensitivity.rfLevel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_SearchRoutines_Rsensitivity_RfLevel_Trace.rst