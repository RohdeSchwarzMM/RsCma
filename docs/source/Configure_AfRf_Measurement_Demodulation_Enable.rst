Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: