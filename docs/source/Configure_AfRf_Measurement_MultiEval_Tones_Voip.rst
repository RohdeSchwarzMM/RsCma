Voip
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP:MODE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP:MODE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Voip.VoipCls
	:members:
	:undoc-members:
	:noindex: