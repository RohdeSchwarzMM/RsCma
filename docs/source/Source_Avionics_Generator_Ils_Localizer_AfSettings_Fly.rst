Fly
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:FLY:IDIRection
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:FLY

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:FLY:IDIRection
	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:FLY



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Localizer.AfSettings.Fly.FlyCls
	:members:
	:undoc-members:
	:noindex: