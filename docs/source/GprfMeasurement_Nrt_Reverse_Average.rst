Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:NRT:REVerse:AVERage
	single: FETCh:GPRF:MEASurement<Instance>:NRT:REVerse:AVERage
	single: READ:GPRF:MEASurement<Instance>:NRT:REVerse:AVERage

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:NRT:REVerse:AVERage
	FETCh:GPRF:MEASurement<Instance>:NRT:REVerse:AVERage
	READ:GPRF:MEASurement<Instance>:NRT:REVerse:AVERage



.. autoclass:: RsCma.Implementations.GprfMeasurement.Nrt.Reverse.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: