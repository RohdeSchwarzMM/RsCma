Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:AVERage
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:AVERage

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:AVERage
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.SpdifLeft.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: