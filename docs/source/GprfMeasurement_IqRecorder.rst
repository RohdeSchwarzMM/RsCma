IqRecorder
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:IQRecorder
	single: ABORt:GPRF:MEASurement<Instance>:IQRecorder
	single: STOP:GPRF:MEASurement<Instance>:IQRecorder
	single: READ:GPRF:MEASurement<Instance>:IQRecorder
	single: FETCh:GPRF:MEASurement<Instance>:IQRecorder

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:IQRecorder
	ABORt:GPRF:MEASurement<Instance>:IQRecorder
	STOP:GPRF:MEASurement<Instance>:IQRecorder
	READ:GPRF:MEASurement<Instance>:IQRecorder
	FETCh:GPRF:MEASurement<Instance>:IQRecorder



.. autoclass:: RsCma.Implementations.GprfMeasurement.IqRecorder.IqRecorderCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.iqRecorder.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_IqRecorder_Bin.rst
	GprfMeasurement_IqRecorder_Reliability.rst
	GprfMeasurement_IqRecorder_State.rst
	GprfMeasurement_IqRecorder_SymbolRate.rst
	GprfMeasurement_IqRecorder_Talignment.rst