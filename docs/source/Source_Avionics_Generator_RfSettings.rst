RfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:RFSettings:CONNector
	single: SOURce:AVIonics:GENerator<Instance>:RFSettings:EATTenuation

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:RFSettings:CONNector
	SOURce:AVIonics:GENerator<Instance>:RFSettings:EATTenuation



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: