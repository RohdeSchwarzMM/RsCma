Swt
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:SWT:AUTO
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:SWT

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:SWT:AUTO
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:SWT



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.FreqSweep.Swt.SwtCls
	:members:
	:undoc-members:
	:noindex: