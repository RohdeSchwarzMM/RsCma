Absolute
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:MARKer<nr>:ABSolute

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:MARKer<nr>:ABSolute



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Frequency.Marker.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex: