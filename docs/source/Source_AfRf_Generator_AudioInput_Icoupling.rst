Icoupling
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:AIN<nr>:ICOupling

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:AIN<nr>:ICOupling



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.AudioInput.Icoupling.IcouplingCls
	:members:
	:undoc-members:
	:noindex: