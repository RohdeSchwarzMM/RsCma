Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:FFERror:CURRent
	single: READ:VSE:MEASurement<Instance>:FFERror:CURRent
	single: CALCulate:VSE:MEASurement<Instance>:FFERror:CURRent

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:FFERror:CURRent
	READ:VSE:MEASurement<Instance>:FFERror:CURRent
	CALCulate:VSE:MEASurement<Instance>:FFERror:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.FfError.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: