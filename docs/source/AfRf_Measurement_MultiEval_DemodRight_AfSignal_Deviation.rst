Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodRight.AfSignal.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: