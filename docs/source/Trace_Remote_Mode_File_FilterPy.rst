FilterPy
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRACe:REMote:MODE:FILE<instrument>:FILTer

.. code-block:: python

	TRACe:REMote:MODE:FILE<instrument>:FILTer



.. autoclass:: RsCma.Implementations.Trace.Remote.Mode.File.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: