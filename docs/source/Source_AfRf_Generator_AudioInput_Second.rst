Second
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:AIN:SECond:MLEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:AIN:SECond:MLEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.AudioInput.Second.SecondCls
	:members:
	:undoc-members:
	:noindex: