Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Voip.AfSignal.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: