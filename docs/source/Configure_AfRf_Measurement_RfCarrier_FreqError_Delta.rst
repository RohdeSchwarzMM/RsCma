Delta
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:RFCarrier:FERRor:DELTa:MODE
	single: CONFigure:AFRF:MEASurement<Instance>:RFCarrier:FERRor:DELTa:USER
	single: CONFigure:AFRF:MEASurement<Instance>:RFCarrier:FERRor:DELTa:MEASured

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:RFCarrier:FERRor:DELTa:MODE
	CONFigure:AFRF:MEASurement<Instance>:RFCarrier:FERRor:DELTa:USER
	CONFigure:AFRF:MEASurement<Instance>:RFCarrier:FERRor:DELTa:MEASured



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.RfCarrier.FreqError.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.rfCarrier.freqError.delta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_RfCarrier_FreqError_Delta_Update.rst