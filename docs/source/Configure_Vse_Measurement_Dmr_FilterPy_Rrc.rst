Rrc
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:DMR:FILTer:RRC:ROFFfactor

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:DMR:FILTer:RRC:ROFFfactor



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Dmr.FilterPy.Rrc.RrcCls
	:members:
	:undoc-members:
	:noindex: