RfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:XRT:RFSettings:CONNector
	single: CONFigure:VSE:MEASurement<Instance>:XRT:RFSettings:FREQuency
	single: CONFigure:VSE:MEASurement<Instance>:XRT:RFSettings:ENPower

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:XRT:RFSettings:CONNector
	CONFigure:VSE:MEASurement<Instance>:XRT:RFSettings:FREQuency
	CONFigure:VSE:MEASurement<Instance>:XRT:RFSettings:ENPower



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Xrt.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: