Result
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:RESult:OVERview
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:RESult:OSCilloscope
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:RESult:FFT

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:RESult:OVERview
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:RESult:OSCilloscope
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:RESult:FFT



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: