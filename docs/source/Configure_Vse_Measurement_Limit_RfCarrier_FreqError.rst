FreqError
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:RFCarrier:FERRor

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:RFCarrier:FERRor



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.RfCarrier.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex: