Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: DISPlay:GPRF:MEASurement<Instance>:ACP:TRACe

.. code-block:: python

	DISPlay:GPRF:MEASurement<Instance>:ACP:TRACe



.. autoclass:: RsCma.Implementations.Display.GprfMeasurement.Acp.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: