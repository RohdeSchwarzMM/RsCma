Sfactor
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FILTer:DWIDth:SFACtor

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FILTer:DWIDth:SFACtor



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.FilterPy.Dwidth.Sfactor.SfactorCls
	:members:
	:undoc-members:
	:noindex: