Vswr
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:VSWR:MODE

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:VSWR:MODE



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.Vswr.VswrCls
	:members:
	:undoc-members:
	:noindex: