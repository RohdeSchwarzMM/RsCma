Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifRight.AfSignal.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: