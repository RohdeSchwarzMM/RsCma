Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.FreqError.Delta.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: