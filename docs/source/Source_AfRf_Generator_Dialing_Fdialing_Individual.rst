Individual
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:INDividual:DTIMe
	single: SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:INDividual:DPAuse

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:INDividual:DTIMe
	SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:INDividual:DPAuse



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.Fdialing.Individual.IndividualCls
	:members:
	:undoc-members:
	:noindex: