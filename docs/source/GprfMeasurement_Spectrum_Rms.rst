Rms
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.spectrum.rms.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Spectrum_Rms_Average.rst
	GprfMeasurement_Spectrum_Rms_Current.rst
	GprfMeasurement_Spectrum_Rms_Maximum.rst
	GprfMeasurement_Spectrum_Rms_Minimum.rst