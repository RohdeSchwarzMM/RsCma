Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:MERRor:MAXimum
	single: READ:VSE:MEASurement<Instance>:MERRor:MAXimum
	single: CALCulate:VSE:MEASurement<Instance>:MERRor:MAXimum

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:MERRor:MAXimum
	READ:VSE:MEASurement<Instance>:MERRor:MAXimum
	CALCulate:VSE:MEASurement<Instance>:MERRor:MAXimum



.. autoclass:: RsCma.Implementations.Vse.Measurement.Merror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: