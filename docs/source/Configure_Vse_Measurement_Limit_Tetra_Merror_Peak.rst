Peak
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:TETRa:MERRor:PEAK

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:TETRa:MERRor:PEAK



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Tetra.Merror.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: