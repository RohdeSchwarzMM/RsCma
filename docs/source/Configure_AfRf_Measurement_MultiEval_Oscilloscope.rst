Oscilloscope
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Oscilloscope.OscilloscopeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.oscilloscope.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Oscilloscope_AudioInput.rst
	Configure_AfRf_Measurement_MultiEval_Oscilloscope_Demodulation.rst
	Configure_AfRf_Measurement_MultiEval_Oscilloscope_Spdif.rst
	Configure_AfRf_Measurement_MultiEval_Oscilloscope_Voip.rst