Counter
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: INITiate:AFRF:MEASurement<Instance>:FREQuency:COUNter
	single: ABORt:AFRF:MEASurement<Instance>:FREQuency:COUNter
	single: FETCh:AFRF:MEASurement<Instance>:FREQuency:COUNter

.. code-block:: python

	INITiate:AFRF:MEASurement<Instance>:FREQuency:COUNter
	ABORt:AFRF:MEASurement<Instance>:FREQuency:COUNter
	FETCh:AFRF:MEASurement<Instance>:FREQuency:COUNter



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Frequency.Counter.CounterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.frequency.counter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_Frequency_Counter_FreqError.rst