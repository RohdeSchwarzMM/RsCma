UserDefined
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DTMF:UDEFined:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DTMF:UDEFined:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Dtmf.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: