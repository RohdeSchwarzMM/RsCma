Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:CURRent
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:CURRent

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:CURRent
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.AudioInput.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: