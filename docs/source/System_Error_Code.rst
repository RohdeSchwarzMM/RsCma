Code
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:ERRor:CODE:ALL
	single: SYSTem:ERRor:CODE[:NEXT]

.. code-block:: python

	SYSTem:ERRor:CODE:ALL
	SYSTem:ERRor:CODE[:NEXT]



.. autoclass:: RsCma.Implementations.System.Error.Code.CodeCls
	:members:
	:undoc-members:
	:noindex: