Acp
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:ACP
	single: STOP:GPRF:MEASurement<Instance>:ACP
	single: ABORt:GPRF:MEASurement<Instance>:ACP

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:ACP
	STOP:GPRF:MEASurement<Instance>:ACP
	ABORt:GPRF:MEASurement<Instance>:ACP



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.AcpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.acp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Acp_Aclr.rst
	GprfMeasurement_Acp_Obw.rst
	GprfMeasurement_Acp_Power.rst
	GprfMeasurement_Acp_State.rst