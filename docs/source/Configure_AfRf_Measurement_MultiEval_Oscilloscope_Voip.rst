Voip
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:XDIVision
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:MTIMe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:XDIVision
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:MTIMe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Oscilloscope.Voip.VoipCls
	:members:
	:undoc-members:
	:noindex: