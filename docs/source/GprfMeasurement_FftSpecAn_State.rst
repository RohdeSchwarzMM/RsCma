State
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:STATe



.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.fftSpecAn.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_FftSpecAn_State_All.rst