Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:AOUT<nr>:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:AOUT<nr>:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.AudioOutput.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: