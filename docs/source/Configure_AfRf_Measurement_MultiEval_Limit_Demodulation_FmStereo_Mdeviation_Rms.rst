Rms
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:DEModulation:FMSTereo:MDEViation:RMS

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:DEModulation:FMSTereo:MDEViation:RMS



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Demodulation.FmStereo.Mdeviation.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: