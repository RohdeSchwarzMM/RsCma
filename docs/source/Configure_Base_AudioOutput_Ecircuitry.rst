Ecircuitry
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:AOUT<nr>:ECIRcuitry

.. code-block:: python

	CONFigure:BASE:AOUT<nr>:ECIRcuitry



.. autoclass:: RsCma.Implementations.Configure.Base.AudioOutput.Ecircuitry.EcircuitryCls
	:members:
	:undoc-members:
	:noindex: