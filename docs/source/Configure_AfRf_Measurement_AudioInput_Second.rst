Second
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN:SECond:MLEVel

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN:SECond:MLEVel



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Second.SecondCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.audioInput.second.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_AudioInput_Second_Level.rst