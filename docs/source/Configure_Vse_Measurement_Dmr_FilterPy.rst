FilterPy
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:DMR:FILTer

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:DMR:FILTer



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Dmr.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.dmr.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Dmr_FilterPy_Rrc.rst