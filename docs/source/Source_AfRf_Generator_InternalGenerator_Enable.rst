Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: