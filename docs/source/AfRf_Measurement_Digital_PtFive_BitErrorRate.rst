BitErrorRate
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.PtFive.BitErrorRate.BitErrorRateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.digital.ptFive.bitErrorRate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_Digital_PtFive_BitErrorRate_Current.rst
	AfRf_Measurement_Digital_PtFive_BitErrorRate_Maximum.rst