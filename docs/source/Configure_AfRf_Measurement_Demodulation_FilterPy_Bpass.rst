Bpass
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:BPASs:ENABle
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:BPASs:CFRequency
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:BPASs:BWIDth

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:BPASs:ENABle
	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:BPASs:CFRequency
	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:BPASs:BWIDth



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.FilterPy.Bpass.BpassCls
	:members:
	:undoc-members:
	:noindex: