Mlevel
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:AIN<nr>:MLEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:AIN<nr>:MLEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.AudioInput.Mlevel.MlevelCls
	:members:
	:undoc-members:
	:noindex: