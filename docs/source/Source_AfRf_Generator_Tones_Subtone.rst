Subtone
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:TONes:SUBTone:ENABle
	single: SOURce:AFRF:GENerator<Instance>:TONes:SUBTone:FREQuency

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:TONes:SUBTone:ENABle
	SOURce:AFRF:GENerator<Instance>:TONes:SUBTone:FREQuency



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Tones.Subtone.SubtoneCls
	:members:
	:undoc-members:
	:noindex: