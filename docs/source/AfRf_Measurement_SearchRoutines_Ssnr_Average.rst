Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:SSNR:AVERage
	single: READ:AFRF:MEASurement<Instance>:SROutines:SSNR:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:SSNR:AVERage
	READ:AFRF:MEASurement<Instance>:SROutines:SSNR:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Ssnr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: