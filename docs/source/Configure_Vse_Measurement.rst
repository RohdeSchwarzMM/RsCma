Measurement
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:CREPetition
	single: CONFigure:VSE:MEASurement<Instance>:SCOunt
	single: CONFigure:VSE:MEASurement<Instance>:REPetition
	single: CONFigure:VSE:MEASurement<Instance>:SCONdition
	single: CONFigure:VSE:MEASurement<Instance>:RCOupling
	single: CONFigure:VSE:MEASurement<Instance>:TOUT
	single: CONFigure:VSE:MEASurement<Instance>:STANdard

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:CREPetition
	CONFigure:VSE:MEASurement<Instance>:SCOunt
	CONFigure:VSE:MEASurement<Instance>:REPetition
	CONFigure:VSE:MEASurement<Instance>:SCONdition
	CONFigure:VSE:MEASurement<Instance>:RCOupling
	CONFigure:VSE:MEASurement<Instance>:TOUT
	CONFigure:VSE:MEASurement<Instance>:STANdard



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Custom.rst
	Configure_Vse_Measurement_Dmr.rst
	Configure_Vse_Measurement_Dpmr.rst
	Configure_Vse_Measurement_IqRecorder.rst
	Configure_Vse_Measurement_Limit.rst
	Configure_Vse_Measurement_Nxdn.rst
	Configure_Vse_Measurement_PtFive.rst
	Configure_Vse_Measurement_Result.rst
	Configure_Vse_Measurement_Tetra.rst
	Configure_Vse_Measurement_Xrt.rst