StandardDev
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:SDEViation
	single: READ:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:SDEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:SDEViation
	READ:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:SDEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Ttl.BitErrorRate.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: