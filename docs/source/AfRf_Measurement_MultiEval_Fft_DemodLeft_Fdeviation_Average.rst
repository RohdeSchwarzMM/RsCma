Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.DemodLeft.Fdeviation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: