UserDefined
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:ENABle
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:IMODulation
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:REPetition
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:PAUSe
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:BANDwidth
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:ILENgth
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:ROFactor
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:FILTer
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:SLENgth
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:DRATe
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:SVALue
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:PATTern
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:SDEViation
	single: SOURce:AFRF:GENerator<Instance>:UDEFined:MODE

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:UDEFined:ENABle
	SOURce:AFRF:GENerator<Instance>:UDEFined:IMODulation
	SOURce:AFRF:GENerator<Instance>:UDEFined:REPetition
	SOURce:AFRF:GENerator<Instance>:UDEFined:PAUSe
	SOURce:AFRF:GENerator<Instance>:UDEFined:BANDwidth
	SOURce:AFRF:GENerator<Instance>:UDEFined:ILENgth
	SOURce:AFRF:GENerator<Instance>:UDEFined:ROFactor
	SOURce:AFRF:GENerator<Instance>:UDEFined:FILTer
	SOURce:AFRF:GENerator<Instance>:UDEFined:SLENgth
	SOURce:AFRF:GENerator<Instance>:UDEFined:DRATe
	SOURce:AFRF:GENerator<Instance>:UDEFined:SVALue
	SOURce:AFRF:GENerator<Instance>:UDEFined:PATTern
	SOURce:AFRF:GENerator<Instance>:UDEFined:SDEViation
	SOURce:AFRF:GENerator<Instance>:UDEFined:MODE



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: