Generator
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DSOurce
	single: SOURce:AFRF:GENerator<Instance>:MSCHeme

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DSOurce
	SOURce:AFRF:GENerator<Instance>:MSCHeme



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Arb.rst
	Source_AfRf_Generator_AudioInput.rst
	Source_AfRf_Generator_AudioOutput.rst
	Source_AfRf_Generator_Cdefinition.rst
	Source_AfRf_Generator_Dialing.rst
	Source_AfRf_Generator_Digital.rst
	Source_AfRf_Generator_Dmr.rst
	Source_AfRf_Generator_Dpmr.rst
	Source_AfRf_Generator_FilterPy.rst
	Source_AfRf_Generator_Interferer.rst
	Source_AfRf_Generator_InternalGenerator.rst
	Source_AfRf_Generator_Modulator.rst
	Source_AfRf_Generator_Nxdn.rst
	Source_AfRf_Generator_Pocsag.rst
	Source_AfRf_Generator_PtFive.rst
	Source_AfRf_Generator_Reliability.rst
	Source_AfRf_Generator_RfSettings.rst
	Source_AfRf_Generator_Sout.rst
	Source_AfRf_Generator_State.rst
	Source_AfRf_Generator_Tones.rst
	Source_AfRf_Generator_UserDefined.rst
	Source_AfRf_Generator_Voip.rst
	Source_AfRf_Generator_Zigbee.rst