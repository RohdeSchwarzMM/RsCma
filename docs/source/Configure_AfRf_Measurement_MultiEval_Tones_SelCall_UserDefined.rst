UserDefined
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:UDEFined:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:UDEFined:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.SelCall.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: