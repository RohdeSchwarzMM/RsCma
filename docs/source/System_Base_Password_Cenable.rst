Cenable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:PASSword[:CENable]:STATe
	single: SYSTem:BASE:PASSword[:CENable]

.. code-block:: python

	SYSTem:BASE:PASSword[:CENable]:STATe
	SYSTem:BASE:PASSword[:CENable]



.. autoclass:: RsCma.Implementations.System.Base.Password.Cenable.CenableCls
	:members:
	:undoc-members:
	:noindex: