Obw
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Obw.ObwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.acp.obw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Acp_Obw_Average.rst
	GprfMeasurement_Acp_Obw_Current.rst
	GprfMeasurement_Acp_Obw_Maximum.rst