Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.FskDeviation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: