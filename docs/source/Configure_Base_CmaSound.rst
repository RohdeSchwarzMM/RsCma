CmaSound
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:CMASound:VOLume
	single: CONFigure:BASE:CMASound:SOURce
	single: CONFigure:BASE:CMASound:SQUelch

.. code-block:: python

	CONFigure:BASE:CMASound:VOLume
	CONFigure:BASE:CMASound:SOURce
	CONFigure:BASE:CMASound:SQUelch



.. autoclass:: RsCma.Implementations.Configure.Base.CmaSound.CmaSoundCls
	:members:
	:undoc-members:
	:noindex: