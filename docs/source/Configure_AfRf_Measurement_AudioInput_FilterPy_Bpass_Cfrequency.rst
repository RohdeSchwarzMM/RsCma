Cfrequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FILTer:BPASs:CFRequency

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FILTer:BPASs:CFRequency



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.FilterPy.Bpass.Cfrequency.CfrequencyCls
	:members:
	:undoc-members:
	:noindex: