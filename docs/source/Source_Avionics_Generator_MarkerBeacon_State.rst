State
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:MBEacon:STATe
	single: SOURce:AVIonics:GENerator<Instance>:MBEacon:STATe:ALL

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:MBEacon:STATe
	SOURce:AVIonics:GENerator<Instance>:MBEacon:STATe:ALL



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.MarkerBeacon.State.StateCls
	:members:
	:undoc-members:
	:noindex: