RfCarrier
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.RfCarrierCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.rfCarrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_RfCarrier_Average.rst
	AfRf_Measurement_MultiEval_RfCarrier_Current.rst
	AfRf_Measurement_MultiEval_RfCarrier_Deviation.rst
	AfRf_Measurement_MultiEval_RfCarrier_FreqError.rst
	AfRf_Measurement_MultiEval_RfCarrier_Frequency.rst
	AfRf_Measurement_MultiEval_RfCarrier_Maximum.rst
	AfRf_Measurement_MultiEval_RfCarrier_Minimum.rst
	AfRf_Measurement_MultiEval_RfCarrier_PePower.rst
	AfRf_Measurement_MultiEval_RfCarrier_Power.rst