ThdNoise
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:AIN<Nr>:THDNoise

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:AIN<Nr>:THDNoise



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.AudioInput.ThdNoise.ThdNoiseCls
	:members:
	:undoc-members:
	:noindex: