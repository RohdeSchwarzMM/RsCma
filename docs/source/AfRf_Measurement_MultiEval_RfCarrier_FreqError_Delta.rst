Delta
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.FreqError.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.rfCarrier.freqError.delta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_RfCarrier_FreqError_Delta_Average.rst
	AfRf_Measurement_MultiEval_RfCarrier_FreqError_Delta_Current.rst
	AfRf_Measurement_MultiEval_RfCarrier_FreqError_Delta_Deviation.rst
	AfRf_Measurement_MultiEval_RfCarrier_FreqError_Delta_Maximum.rst
	AfRf_Measurement_MultiEval_RfCarrier_FreqError_Delta_Minimum.rst