Rms
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:DPMR:MERRor:RMS

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:DPMR:MERRor:RMS



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Dpmr.Merror.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: