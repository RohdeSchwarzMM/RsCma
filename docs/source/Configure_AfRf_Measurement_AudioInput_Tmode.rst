Tmode
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:TMODe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:TMODe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Tmode.TmodeCls
	:members:
	:undoc-members:
	:noindex: