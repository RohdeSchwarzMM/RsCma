Rsquelch
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:RSQuelch:SOTime
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:RSQuelch:LVLTolerance
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:RSQuelch:EXTent

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:RSQuelch:SOTime
	CONFigure:AFRF:MEASurement<Instance>:SROutines:RSQuelch:LVLTolerance
	CONFigure:AFRF:MEASurement<Instance>:SROutines:RSQuelch:EXTent



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Rsquelch.RsquelchCls
	:members:
	:undoc-members:
	:noindex: