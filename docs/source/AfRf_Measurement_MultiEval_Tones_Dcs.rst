Dcs
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.DcsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.tones.dcs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Tones_Dcs_BitErrorRate.rst
	AfRf_Measurement_MultiEval_Tones_Dcs_Cword.rst
	AfRf_Measurement_MultiEval_Tones_Dcs_Dmatches.rst
	AfRf_Measurement_MultiEval_Tones_Dcs_FskDeviation.rst
	AfRf_Measurement_MultiEval_Tones_Dcs_LcWord.rst
	AfRf_Measurement_MultiEval_Tones_Dcs_TocLength.rst