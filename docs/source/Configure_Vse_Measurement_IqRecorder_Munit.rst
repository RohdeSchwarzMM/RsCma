Munit
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:IQRecorder:MUNit

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:IQRecorder:MUNit



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.IqRecorder.Munit.MunitCls
	:members:
	:undoc-members:
	:noindex: