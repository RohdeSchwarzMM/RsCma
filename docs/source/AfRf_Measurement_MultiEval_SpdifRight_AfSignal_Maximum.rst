Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifRight.AfSignal.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: