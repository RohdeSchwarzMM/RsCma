Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:RFLevel:TRACe
	single: READ:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:RFLevel:TRACe

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:RFLevel:TRACe
	READ:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:RFLevel:TRACe



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsensitivity.RfLevel.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: