CumulativeDistribFnc
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:FWARd:LIMit:CCDF

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRT:FWARd:LIMit:CCDF



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.Forward.Limit.CumulativeDistribFnc.CumulativeDistribFncCls
	:members:
	:undoc-members:
	:noindex: