SelCall
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<instance>:MEValuation:TONes:SELCall:DTLength
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:CFGenerator
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:SLENgth
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:STANdard
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:MACCuracy

.. code-block:: python

	CONFigure:AFRF:MEASurement<instance>:MEValuation:TONes:SELCall:DTLength
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:CFGenerator
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:SLENgth
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:STANdard
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:MACCuracy



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.SelCall.SelCallCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.tones.selCall.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Tones_SelCall_Frequency.rst
	Configure_AfRf_Measurement_MultiEval_Tones_SelCall_UserDefined.rst