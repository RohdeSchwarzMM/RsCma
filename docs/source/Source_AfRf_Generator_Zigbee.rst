Zigbee
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:ZIGBee:SNUMber
	single: SOURce:AFRF:GENerator<Instance>:ZIGBee:DPAN
	single: SOURce:AFRF:GENerator<Instance>:ZIGBee:DADDress
	single: SOURce:AFRF:GENerator<Instance>:ZIGBee:SPAN
	single: SOURce:AFRF:GENerator<Instance>:ZIGBee:SADDress
	single: SOURce:AFRF:GENerator<Instance>:ZIGBee:PAYLoad
	single: SOURce:AFRF:GENerator<Instance>:ZIGBee:MODE
	single: SOURce:AFRF:GENerator<Instance>:ZIGBee:SDEViation
	single: SOURce:AFRF:GENerator<Instance>:ZIGBee:SRATe

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:ZIGBee:SNUMber
	SOURce:AFRF:GENerator<Instance>:ZIGBee:DPAN
	SOURce:AFRF:GENerator<Instance>:ZIGBee:DADDress
	SOURce:AFRF:GENerator<Instance>:ZIGBee:SPAN
	SOURce:AFRF:GENerator<Instance>:ZIGBee:SADDress
	SOURce:AFRF:GENerator<Instance>:ZIGBee:PAYLoad
	SOURce:AFRF:GENerator<Instance>:ZIGBee:MODE
	SOURce:AFRF:GENerator<Instance>:ZIGBee:SDEViation
	SOURce:AFRF:GENerator<Instance>:ZIGBee:SRATe



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Zigbee.ZigbeeCls
	:members:
	:undoc-members:
	:noindex: