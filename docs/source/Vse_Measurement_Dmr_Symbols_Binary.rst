Binary
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:DMR:SYMBols:BINary
	single: READ:VSE:MEASurement<Instance>:DMR:SYMBols:BINary

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:DMR:SYMBols:BINary
	READ:VSE:MEASurement<Instance>:DMR:SYMBols:BINary



.. autoclass:: RsCma.Implementations.Vse.Measurement.Dmr.Symbols.Binary.BinaryCls
	:members:
	:undoc-members:
	:noindex: