Icoupling
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:ICOupling

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:ICOupling



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Icoupling.IcouplingCls
	:members:
	:undoc-members:
	:noindex: