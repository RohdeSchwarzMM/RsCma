FormatPy
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRACe:REMote:MODE:FILE<instrument>:FORMat

.. code-block:: python

	TRACe:REMote:MODE:FILE<instrument>:FORMat



.. autoclass:: RsCma.Implementations.Trace.Remote.Mode.File.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex: