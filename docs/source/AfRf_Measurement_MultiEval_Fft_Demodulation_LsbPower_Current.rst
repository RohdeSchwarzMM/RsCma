Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.LsbPower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: