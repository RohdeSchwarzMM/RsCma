Nrt
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:SCOunt
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:REPetition
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:RCOupling
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:FREQuency
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:CCDF
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:BWIDth
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:RESolution
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:DIRection
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:PEPHoldtime
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:DEVice

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRT:SCOunt
	CONFigure:GPRF:MEASurement<Instance>:NRT:REPetition
	CONFigure:GPRF:MEASurement<Instance>:NRT:RCOupling
	CONFigure:GPRF:MEASurement<Instance>:NRT:FREQuency
	CONFigure:GPRF:MEASurement<Instance>:NRT:CCDF
	CONFigure:GPRF:MEASurement<Instance>:NRT:BWIDth
	CONFigure:GPRF:MEASurement<Instance>:NRT:RESolution
	CONFigure:GPRF:MEASurement<Instance>:NRT:DIRection
	CONFigure:GPRF:MEASurement<Instance>:NRT:PEPHoldtime
	CONFigure:GPRF:MEASurement<Instance>:NRT:DEVice



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.NrtCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.nrt.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_Nrt_Attenuation.rst
	Configure_GprfMeasurement_Nrt_Forward.rst
	Configure_GprfMeasurement_Nrt_Reverse.rst