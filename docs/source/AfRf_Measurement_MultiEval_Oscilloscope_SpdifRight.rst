SpdifRight
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.SpdifRight.SpdifRightCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.oscilloscope.spdifRight.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Oscilloscope_SpdifRight_PowerVsTime.rst