Measurement
----------------------------------------





.. autoclass:: RsCma.Implementations.Display.AfRf.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.afRf.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_AfRf_Measurement_Application.rst
	Display_AfRf_Measurement_Audio.rst
	Display_AfRf_Measurement_Digital.rst
	Display_AfRf_Measurement_Routines.rst