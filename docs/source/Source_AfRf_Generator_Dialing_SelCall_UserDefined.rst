UserDefined
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:UDEFined:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:UDEFined:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.SelCall.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: