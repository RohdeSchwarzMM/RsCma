Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FREQuency:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:FREQuency:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.Frequency.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: