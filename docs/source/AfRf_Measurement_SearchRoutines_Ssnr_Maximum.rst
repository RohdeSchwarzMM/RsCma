Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:SSNR:MAXimum
	single: READ:AFRF:MEASurement<Instance>:SROutines:SSNR:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:SSNR:MAXimum
	READ:AFRF:MEASurement<Instance>:SROutines:SSNR:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Ssnr.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: