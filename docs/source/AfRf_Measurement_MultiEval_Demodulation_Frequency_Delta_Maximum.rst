Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FREQuency:DELTa:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FREQuency:DELTa:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FREQuency:DELTa:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FREQuency:DELTa:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Frequency.Delta.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: