Value
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:REVerse:VALue:ENABle

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRT:REVerse:VALue:ENABle



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.Reverse.Value.ValueCls
	:members:
	:undoc-members:
	:noindex: