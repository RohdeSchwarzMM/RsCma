Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:TETRa:FERRor:CURRent
	single: CALCulate:AFRF:MEASurement<Instance>:DIGital:TETRa:FERRor:CURRent
	single: READ:AFRF:MEASurement<Instance>:DIGital:TETRa:FERRor:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:TETRa:FERRor:CURRent
	CALCulate:AFRF:MEASurement<Instance>:DIGital:TETRa:FERRor:CURRent
	READ:AFRF:MEASurement<Instance>:DIGital:TETRa:FERRor:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Tetra.FreqError.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: