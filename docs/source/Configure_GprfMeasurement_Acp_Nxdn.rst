Nxdn
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:NXDN:TRANsmission

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:ACP:NXDN:TRANsmission



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Acp.Nxdn.NxdnCls
	:members:
	:undoc-members:
	:noindex: