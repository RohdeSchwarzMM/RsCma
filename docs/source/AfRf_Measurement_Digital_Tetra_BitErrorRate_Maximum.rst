Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:TETRa:BERate:MAXimum
	single: CALCulate:AFRF:MEASurement<Instance>:DIGital:TETRa:BERate:MAXimum
	single: READ:AFRF:MEASurement<Instance>:DIGital:TETRa:BERate:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:TETRa:BERate:MAXimum
	CALCulate:AFRF:MEASurement<Instance>:DIGital:TETRa:BERate:MAXimum
	READ:AFRF:MEASurement<Instance>:DIGital:TETRa:BERate:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Tetra.BitErrorRate.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: