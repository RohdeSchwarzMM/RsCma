Power
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SENSe:BASE:POWer:OMODe

.. code-block:: python

	SENSe:BASE:POWer:OMODe



.. autoclass:: RsCma.Implementations.Sense.Base.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: