Uri
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:URI:CMA
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:URI:IP
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:URI:PORT
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:URI:USER

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:URI:CMA
	CONFigure:AFRF:MEASurement<Instance>:VOIP:URI:IP
	CONFigure:AFRF:MEASurement<Instance>:VOIP:URI:PORT
	CONFigure:AFRF:MEASurement<Instance>:VOIP:URI:USER



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.Uri.UriCls
	:members:
	:undoc-members:
	:noindex: