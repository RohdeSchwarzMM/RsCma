Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:MARKer:ENABle:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:MARKer:ENABle:ALL



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.FftSpecAn.Marker.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: