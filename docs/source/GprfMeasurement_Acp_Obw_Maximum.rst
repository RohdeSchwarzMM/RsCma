Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:ACP:OBW:MAXimum
	single: READ:GPRF:MEASurement<Instance>:ACP:OBW:MAXimum
	single: CALCulate:GPRF:MEASurement<Instance>:ACP:OBW:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:ACP:OBW:MAXimum
	READ:GPRF:MEASurement<Instance>:ACP:OBW:MAXimum
	CALCulate:GPRF:MEASurement<Instance>:ACP:OBW:MAXimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Obw.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: