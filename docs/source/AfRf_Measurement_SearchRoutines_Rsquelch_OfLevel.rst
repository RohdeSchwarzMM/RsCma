OfLevel
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:OFLevel
	single: CALCulate:AFRF:MEASurement<Instance>:SROutines:RSQuelch:OFLevel
	single: READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:OFLevel

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:OFLevel
	CALCulate:AFRF:MEASurement<Instance>:SROutines:RSQuelch:OFLevel
	READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:OFLevel



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsquelch.OfLevel.OfLevelCls
	:members:
	:undoc-members:
	:noindex: