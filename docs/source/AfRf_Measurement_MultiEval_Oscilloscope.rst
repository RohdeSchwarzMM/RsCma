Oscilloscope
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.OscilloscopeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.oscilloscope.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Oscilloscope_AudioInput.rst
	AfRf_Measurement_MultiEval_Oscilloscope_DemodLeft.rst
	AfRf_Measurement_MultiEval_Oscilloscope_DemodRight.rst
	AfRf_Measurement_MultiEval_Oscilloscope_Demodulation.rst
	AfRf_Measurement_MultiEval_Oscilloscope_SpdifLeft.rst
	AfRf_Measurement_MultiEval_Oscilloscope_SpdifRight.rst
	AfRf_Measurement_MultiEval_Oscilloscope_Voip.rst