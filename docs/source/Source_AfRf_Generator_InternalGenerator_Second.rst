Second
----------------------------------------





.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.Second.SecondCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.internalGenerator.second.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_InternalGenerator_Second_MultiTone.rst