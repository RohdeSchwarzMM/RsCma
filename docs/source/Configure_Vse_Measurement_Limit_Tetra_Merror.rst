Merror
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Tetra.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.limit.tetra.merror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Limit_Tetra_Merror_Peak.rst
	Configure_Vse_Measurement_Limit_Tetra_Merror_Rms.rst