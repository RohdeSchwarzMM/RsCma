Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Power.Delta.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: