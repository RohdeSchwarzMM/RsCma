Sequence
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft:SEQuence
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft:SEQuence

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft:SEQuence
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft:SEQuence



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.SpdifLeft.Sequence.SequenceCls
	:members:
	:undoc-members:
	:noindex: