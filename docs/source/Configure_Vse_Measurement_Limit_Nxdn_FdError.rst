FdError
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:NXDN:FDERor

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:NXDN:FDERor



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Nxdn.FdError.FdErrorCls
	:members:
	:undoc-members:
	:noindex: