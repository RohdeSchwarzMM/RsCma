Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:DMR:BERate:MAXimum
	single: CALCulate:AFRF:MEASurement<Instance>:DIGital:DMR:BERate:MAXimum
	single: READ:AFRF:MEASurement<Instance>:DIGital:DMR:BERate:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:DMR:BERate:MAXimum
	CALCulate:AFRF:MEASurement<Instance>:DIGital:DMR:BERate:MAXimum
	READ:AFRF:MEASurement<Instance>:DIGital:DMR:BERate:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Dmr.BitErrorRate.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: