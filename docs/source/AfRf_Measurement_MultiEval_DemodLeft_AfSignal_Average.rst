Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodLeft.AfSignal.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: