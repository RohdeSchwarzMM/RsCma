ModDepth
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.ModDepth.ModDepthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.demodulation.modDepth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Demodulation_ModDepth_Delta.rst