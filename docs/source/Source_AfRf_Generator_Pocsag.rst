Pocsag
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:POCSag:REPetition
	single: SOURce:AFRF:GENerator<Instance>:POCSag:SDEViation
	single: SOURce:AFRF:GENerator<Instance>:POCSag:IMODulation
	single: SOURce:AFRF:GENerator<Instance>:POCSag:SRATe
	single: SOURce:AFRF:GENerator<Instance>:POCSag:PADDress
	single: SOURce:AFRF:GENerator<Instance>:POCSag:FBITs
	single: SOURce:AFRF:GENerator<Instance>:POCSag:PTYPe
	single: SOURce:AFRF:GENerator<Instance>:POCSag:MESSage
	single: SOURce:AFRF:GENerator<Instance>:POCSag:MODE

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:POCSag:REPetition
	SOURce:AFRF:GENerator<Instance>:POCSag:SDEViation
	SOURce:AFRF:GENerator<Instance>:POCSag:IMODulation
	SOURce:AFRF:GENerator<Instance>:POCSag:SRATe
	SOURce:AFRF:GENerator<Instance>:POCSag:PADDress
	SOURce:AFRF:GENerator<Instance>:POCSag:FBITs
	SOURce:AFRF:GENerator<Instance>:POCSag:PTYPe
	SOURce:AFRF:GENerator<Instance>:POCSag:MESSage
	SOURce:AFRF:GENerator<Instance>:POCSag:MODE



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Pocsag.PocsagCls
	:members:
	:undoc-members:
	:noindex: