Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FREQuency:ATGFrequency
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FREQuency

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:FREQuency:ATGFrequency
	CONFigure:AFRF:MEASurement<Instance>:VOIP:FREQuency



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.voip.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Voip_Frequency_Delta.rst