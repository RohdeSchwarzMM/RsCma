Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.LsbPower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: