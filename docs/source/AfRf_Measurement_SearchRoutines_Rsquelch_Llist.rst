Llist
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsquelch.Llist.LlistCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.searchRoutines.rsquelch.llist.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_SearchRoutines_Rsquelch_Llist_Trace.rst