Range
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:XRT:GENerator<Instance>:ARB:SAMPles:RANGe

.. code-block:: python

	SOURce:XRT:GENerator<Instance>:ARB:SAMPles:RANGe



.. autoclass:: RsCma.Implementations.Source.Xrt.Generator.Arb.Samples.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: