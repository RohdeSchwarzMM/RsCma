Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:NOTCh<Num>:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:NOTCh<Num>:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.FilterPy.Notch.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: