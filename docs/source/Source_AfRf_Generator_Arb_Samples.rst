Samples
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:ARB:SAMPles

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:ARB:SAMPles



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Arb.Samples.SamplesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.arb.samples.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Arb_Samples_Range.rst