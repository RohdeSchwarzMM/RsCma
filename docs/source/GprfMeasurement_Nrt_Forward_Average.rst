Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:NRT:FWARd:AVERage
	single: FETCh:GPRF:MEASurement<Instance>:NRT:FWARd:AVERage
	single: READ:GPRF:MEASurement<Instance>:NRT:FWARd:AVERage

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:NRT:FWARd:AVERage
	FETCh:GPRF:MEASurement<Instance>:NRT:FWARd:AVERage
	READ:GPRF:MEASurement<Instance>:NRT:FWARd:AVERage



.. autoclass:: RsCma.Implementations.GprfMeasurement.Nrt.Forward.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: