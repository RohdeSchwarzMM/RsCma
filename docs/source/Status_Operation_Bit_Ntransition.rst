Ntransition
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: STATus:OPERation:BIT<bitno>:NTRansition

.. code-block:: python

	STATus:OPERation:BIT<bitno>:NTRansition



.. autoclass:: RsCma.Implementations.Status.Operation.Bit.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex: