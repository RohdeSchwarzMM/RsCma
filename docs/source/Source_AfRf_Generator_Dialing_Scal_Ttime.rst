Ttime
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:TTIMe:FIRSt

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:TTIMe:FIRSt



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.Scal.Ttime.TtimeCls
	:members:
	:undoc-members:
	:noindex: