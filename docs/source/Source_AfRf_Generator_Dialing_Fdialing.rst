Fdialing
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:TTYPe
	single: SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:SEQuence
	single: SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:SREPeat
	single: SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:SPAuse
	single: SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:DTIMe
	single: SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:DPAuse
	single: SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:DTMode

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:TTYPe
	SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:SEQuence
	SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:SREPeat
	SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:SPAuse
	SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:DTIMe
	SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:DPAuse
	SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:DTMode



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.Fdialing.FdialingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.dialing.fdialing.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Dialing_Fdialing_Frequency.rst
	Source_AfRf_Generator_Dialing_Fdialing_Individual.rst