Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.SpdifRight.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: