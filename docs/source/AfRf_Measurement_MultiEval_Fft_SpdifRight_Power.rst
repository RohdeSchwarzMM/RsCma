Power
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.SpdifRight.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.fft.spdifRight.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Fft_SpdifRight_Power_Average.rst
	AfRf_Measurement_MultiEval_Fft_SpdifRight_Power_Current.rst
	AfRf_Measurement_MultiEval_Fft_SpdifRight_Power_Maximum.rst
	AfRf_Measurement_MultiEval_Fft_SpdifRight_Power_Minimum.rst