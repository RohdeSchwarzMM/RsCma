Catalog
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:AFRF:GENerator<Instance>:ARB:CATalog:SOURce

.. code-block:: python

	TRIGger:AFRF:GENerator<Instance>:ARB:CATalog:SOURce



.. autoclass:: RsCma.Implementations.Trigger.AfRf.Generator.Arb.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: