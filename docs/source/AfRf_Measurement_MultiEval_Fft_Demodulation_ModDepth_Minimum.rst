Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.ModDepth.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: