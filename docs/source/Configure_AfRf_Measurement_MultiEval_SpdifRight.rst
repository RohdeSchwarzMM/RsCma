SpdifRight
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:SINRight:TMODe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:SINRight:TMODe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.SpdifRight.SpdifRightCls
	:members:
	:undoc-members:
	:noindex: