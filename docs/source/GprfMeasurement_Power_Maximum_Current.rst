Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:MAXimum:CURRent
	single: FETCh:GPRF:MEASurement<Instance>:POWer:MAXimum:CURRent
	single: READ:GPRF:MEASurement<Instance>:POWer:MAXimum:CURRent

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:MAXimum:CURRent
	FETCh:GPRF:MEASurement<Instance>:POWer:MAXimum:CURRent
	READ:GPRF:MEASurement<Instance>:POWer:MAXimum:CURRent



.. autoclass:: RsCma.Implementations.GprfMeasurement.Power.Maximum.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: