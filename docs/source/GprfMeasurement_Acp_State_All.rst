All
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:ACP:STATe:ALL

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:ACP:STATe:ALL



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: