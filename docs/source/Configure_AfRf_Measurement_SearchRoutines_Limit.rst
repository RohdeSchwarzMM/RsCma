Limit
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.searchRoutines.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_SearchRoutines_Limit_RifBandwidth.rst
	Configure_AfRf_Measurement_SearchRoutines_Limit_Rsensitivity.rst
	Configure_AfRf_Measurement_SearchRoutines_Limit_Rsquelch.rst
	Configure_AfRf_Measurement_SearchRoutines_Limit_Ssnr.rst
	Configure_AfRf_Measurement_SearchRoutines_Limit_Tsensitivity.rst