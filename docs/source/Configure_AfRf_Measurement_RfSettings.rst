RfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:RFSettings:CONNector
	single: CONFigure:AFRF:MEASurement<Instance>:RFSettings:FREQuency
	single: CONFigure:AFRF:MEASurement<Instance>:RFSettings:ENPower
	single: CONFigure:AFRF:MEASurement<Instance>:RFSettings:EATTenuation
	single: CONFigure:AFRF:MEASurement<Instance>:RFSettings:RFCoupling
	single: CONFigure:AFRF:MEASurement<Instance>:RFSettings:DSPace
	single: CONFigure:AFRF:MEASurement<Instance>:RFSettings:CHANnel
	single: CONFigure:AFRF:MEASurement<Instance>:RFSettings:COFFset

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:RFSettings:CONNector
	CONFigure:AFRF:MEASurement<Instance>:RFSettings:FREQuency
	CONFigure:AFRF:MEASurement<Instance>:RFSettings:ENPower
	CONFigure:AFRF:MEASurement<Instance>:RFSettings:EATTenuation
	CONFigure:AFRF:MEASurement<Instance>:RFSettings:RFCoupling
	CONFigure:AFRF:MEASurement<Instance>:RFSettings:DSPace
	CONFigure:AFRF:MEASurement<Instance>:RFSettings:CHANnel
	CONFigure:AFRF:MEASurement<Instance>:RFSettings:COFFset



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_RfSettings_FarFrequency.rst
	Configure_AfRf_Measurement_RfSettings_Rf.rst