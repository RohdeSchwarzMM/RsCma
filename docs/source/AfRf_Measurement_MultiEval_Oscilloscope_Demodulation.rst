Demodulation
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.Demodulation.DemodulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.oscilloscope.demodulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Oscilloscope_Demodulation_LsbLevel.rst
	AfRf_Measurement_MultiEval_Oscilloscope_Demodulation_ModDepth.rst
	AfRf_Measurement_MultiEval_Oscilloscope_Demodulation_Pdeviation.rst
	AfRf_Measurement_MultiEval_Oscilloscope_Demodulation_UsbLevel.rst