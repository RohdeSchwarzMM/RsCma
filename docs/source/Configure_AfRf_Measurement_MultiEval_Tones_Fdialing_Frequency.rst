Frequency<FrequencyLobe>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.afRf.measurement.multiEval.tones.fdialing.frequency.repcap_frequencyLobe_get()
	driver.configure.afRf.measurement.multiEval.tones.fdialing.frequency.repcap_frequencyLobe_set(repcap.FrequencyLobe.Nr1)



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:FREQuency:STONe
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:FREQuency:RANGe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:FREQuency:STONe
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:FREQuency:RANGe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Fdialing.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.tones.fdialing.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Tones_Fdialing_Frequency_Dtone.rst