Repetitions
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight:REPetitions
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight:REPetitions

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight:REPetitions
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight:REPetitions



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.SpdifRight.Repetitions.RepetitionsCls
	:members:
	:undoc-members:
	:noindex: