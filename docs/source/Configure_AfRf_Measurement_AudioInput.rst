AudioInput<AudioInput>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.afRf.measurement.audioInput.repcap_audioInput_get()
	driver.configure.afRf.measurement.audioInput.repcap_audioInput_set(repcap.AudioInput.Nr1)





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.AudioInputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.audioInput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_AudioInput_Aranging.rst
	Configure_AfRf_Measurement_AudioInput_Counter.rst
	Configure_AfRf_Measurement_AudioInput_Enable.rst
	Configure_AfRf_Measurement_AudioInput_FilterPy.rst
	Configure_AfRf_Measurement_AudioInput_First.rst
	Configure_AfRf_Measurement_AudioInput_Frequency.rst
	Configure_AfRf_Measurement_AudioInput_Gcoupling.rst
	Configure_AfRf_Measurement_AudioInput_Icoupling.rst
	Configure_AfRf_Measurement_AudioInput_Level.rst
	Configure_AfRf_Measurement_AudioInput_Mlevel.rst
	Configure_AfRf_Measurement_AudioInput_Sdecay.rst
	Configure_AfRf_Measurement_AudioInput_Second.rst
	Configure_AfRf_Measurement_AudioInput_Tmode.rst