Setup
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:DEVice:SETup

.. code-block:: python

	SYSTem:BASE:DEVice:SETup



.. autoclass:: RsCma.Implementations.System.Base.Device.Setup.SetupCls
	:members:
	:undoc-members:
	:noindex: