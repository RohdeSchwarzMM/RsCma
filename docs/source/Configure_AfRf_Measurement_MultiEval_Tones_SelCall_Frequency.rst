Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:FREQuency
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:FREQuency:RESet

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:FREQuency
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SELCall:FREQuency:RESet



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.SelCall.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: