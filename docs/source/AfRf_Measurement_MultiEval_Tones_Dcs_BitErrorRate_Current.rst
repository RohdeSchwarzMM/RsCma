Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.BitErrorRate.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: