InternalGenerator<InternalGen>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.source.afRf.generator.internalGenerator.repcap_internalGen_get()
	driver.source.afRf.generator.internalGenerator.repcap_internalGen_set(repcap.InternalGen.Nr1)





.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.InternalGeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.internalGenerator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_InternalGenerator_Dialing.rst
	Source_AfRf_Generator_InternalGenerator_Dtone.rst
	Source_AfRf_Generator_InternalGenerator_Enable.rst
	Source_AfRf_Generator_InternalGenerator_First.rst
	Source_AfRf_Generator_InternalGenerator_Fourth.rst
	Source_AfRf_Generator_InternalGenerator_Frequency.rst
	Source_AfRf_Generator_InternalGenerator_MultiTone.rst
	Source_AfRf_Generator_InternalGenerator_Second.rst
	Source_AfRf_Generator_InternalGenerator_Third.rst
	Source_AfRf_Generator_InternalGenerator_Tmode.rst