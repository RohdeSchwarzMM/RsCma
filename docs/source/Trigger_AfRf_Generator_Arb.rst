Arb
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:AFRF:GENerator<Instance>:ARB:AUTostart
	single: TRIGger:AFRF:GENerator<Instance>:ARB:DELay
	single: TRIGger:AFRF:GENerator<Instance>:ARB:RETRigger
	single: TRIGger:AFRF:GENerator<Instance>:ARB:SOURce

.. code-block:: python

	TRIGger:AFRF:GENerator<Instance>:ARB:AUTostart
	TRIGger:AFRF:GENerator<Instance>:ARB:DELay
	TRIGger:AFRF:GENerator<Instance>:ARB:RETRigger
	TRIGger:AFRF:GENerator<Instance>:ARB:SOURce



.. autoclass:: RsCma.Implementations.Trigger.AfRf.Generator.Arb.ArbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.afRf.generator.arb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_AfRf_Generator_Arb_Catalog.rst
	Trigger_AfRf_Generator_Arb_Manual.rst