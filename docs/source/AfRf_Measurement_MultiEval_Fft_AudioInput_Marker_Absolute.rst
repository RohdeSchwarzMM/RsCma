Absolute
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<nr>:MARKer<mnr>:ABSolute

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<nr>:MARKer<mnr>:ABSolute



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.AudioInput.Marker.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex: