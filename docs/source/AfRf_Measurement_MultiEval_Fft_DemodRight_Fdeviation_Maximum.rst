Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMRight:FDEViation:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMRight:FDEViation:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMRight:FDEViation:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMRight:FDEViation:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.DemodRight.Fdeviation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: