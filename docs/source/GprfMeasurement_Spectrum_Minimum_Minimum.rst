Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MINimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MINimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MINimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MINimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Minimum.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: