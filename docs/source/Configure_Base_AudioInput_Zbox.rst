Zbox
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.Base.AudioInput.Zbox.ZboxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.base.audioInput.zbox.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Base_AudioInput_Zbox_Attenuator.rst
	Configure_Base_AudioInput_Zbox_Impedance.rst