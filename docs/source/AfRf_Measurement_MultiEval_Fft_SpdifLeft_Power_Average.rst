Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.SpdifLeft.Power.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: