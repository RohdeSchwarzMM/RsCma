Tetra
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Tetra.TetraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.digital.tetra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_Digital_Tetra_BitErrorRate.rst
	AfRf_Measurement_Digital_Tetra_FreqError.rst
	AfRf_Measurement_Digital_Tetra_Power.rst
	AfRf_Measurement_Digital_Tetra_Sinfo.rst