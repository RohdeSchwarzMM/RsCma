Pdeviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:MODulator:FMSTereo:PDEViation

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:MODulator:FMSTereo:PDEViation



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Modulator.FmStereo.Pdeviation.PdeviationCls
	:members:
	:undoc-members:
	:noindex: