FskDeviation
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.FskDeviation.FskDeviationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.tones.dcs.fskDeviation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Tones_Dcs_FskDeviation_Average.rst
	AfRf_Measurement_MultiEval_Tones_Dcs_FskDeviation_Current.rst
	AfRf_Measurement_MultiEval_Tones_Dcs_FskDeviation_Deviation.rst
	AfRf_Measurement_MultiEval_Tones_Dcs_FskDeviation_Maximum.rst