Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:FREQuency:DELTa:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:FREQuency:DELTa:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:FREQuency:DELTa:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:FREQuency:DELTa:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Voip.Frequency.Delta.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: