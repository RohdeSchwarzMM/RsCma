Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:FREQuency:RESet
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:FREQuency

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:FREQuency:RESet
	SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:FREQuency



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.SelCall.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: