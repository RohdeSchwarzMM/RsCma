Capture
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:CAPTure

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:CAPTure



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.IqRecorder.Capture.CaptureCls
	:members:
	:undoc-members:
	:noindex: