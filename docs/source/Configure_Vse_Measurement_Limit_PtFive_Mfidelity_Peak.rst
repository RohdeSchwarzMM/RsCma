Peak
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:PTFive:MFIDelity:PEAK

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:PTFive:MFIDelity:PEAK



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.PtFive.Mfidelity.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: