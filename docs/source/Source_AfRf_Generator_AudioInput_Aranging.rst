Aranging
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:AIN<nr>:ARANging

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:AIN<nr>:ARANging



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.AudioInput.Aranging.ArangingCls
	:members:
	:undoc-members:
	:noindex: