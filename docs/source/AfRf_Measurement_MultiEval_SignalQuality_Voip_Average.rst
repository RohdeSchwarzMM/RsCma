Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:AVERage
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:AVERage

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:AVERage
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.Voip.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: