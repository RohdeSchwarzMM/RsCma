Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:PERRor:CURRent
	single: READ:VSE:MEASurement<Instance>:PERRor:CURRent
	single: CALCulate:VSE:MEASurement<Instance>:PERRor:CURRent

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:PERRor:CURRent
	READ:VSE:MEASurement<Instance>:PERRor:CURRent
	CALCulate:VSE:MEASurement<Instance>:PERRor:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.Perror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: