Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:CURRent
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:CURRent
	READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:CURRent



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Rms.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: