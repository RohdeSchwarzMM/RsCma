SpdifRight
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.SpdifRight.SpdifRightCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.tones.spdifRight.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Tones_SpdifRight_Repetitions.rst
	AfRf_Measurement_MultiEval_Tones_SpdifRight_Sequence.rst