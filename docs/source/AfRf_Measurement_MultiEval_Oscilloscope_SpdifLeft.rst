SpdifLeft
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.SpdifLeft.SpdifLeftCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.oscilloscope.spdifLeft.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Oscilloscope_SpdifLeft_PowerVsTime.rst