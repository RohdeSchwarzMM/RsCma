Base
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:SPEaker
	single: CONFigure:BASE:SCENario

.. code-block:: python

	CONFigure:BASE:SPEaker
	CONFigure:BASE:SCENario



.. autoclass:: RsCma.Implementations.Configure.Base.BaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.base.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Base_Adjustment.rst
	Configure_Base_Attenuation.rst
	Configure_Base_AudioInput.rst
	Configure_Base_AudioOutput.rst
	Configure_Base_CmaSound.rst
	Configure_Base_Cprotection.rst
	Configure_Base_Display.rst
	Configure_Base_Relay.rst
	Configure_Base_SysSound.rst
	Configure_Base_Ttl.rst
	Configure_Base_Zbox.rst