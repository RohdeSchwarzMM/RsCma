SpdifRight
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifRight.SpdifRightCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.spdifRight.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_SpdifRight_AfSignal.rst
	AfRf_Measurement_MultiEval_SpdifRight_Frequency.rst
	AfRf_Measurement_MultiEval_SpdifRight_Level.rst