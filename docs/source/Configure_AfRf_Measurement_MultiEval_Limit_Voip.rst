Voip
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Voip.VoipCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.limit.voip.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Limit_Voip_Sinad.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Voip_SndRatio.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Voip_SnnRatio.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Voip_SnRatio.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Voip_ThDistortion.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Voip_ThdNoise.rst