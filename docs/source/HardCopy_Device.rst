Device
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: HCOPy:DEVice:FORMat

.. code-block:: python

	HCOPy:DEVice:FORMat



.. autoclass:: RsCma.Implementations.HardCopy.Device.DeviceCls
	:members:
	:undoc-members:
	:noindex: