Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.LsbPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: