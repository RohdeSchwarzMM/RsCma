Rms
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:PTFive:MFIDelity:RMS

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:PTFive:MFIDelity:RMS



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.PtFive.Mfidelity.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: