Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: DISPlay:GPRF:MEASurement<Instance>:FFTSanalyzer:TRACe

.. code-block:: python

	DISPlay:GPRF:MEASurement<Instance>:FFTSanalyzer:TRACe



.. autoclass:: RsCma.Implementations.Display.GprfMeasurement.FftSpecAn.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: