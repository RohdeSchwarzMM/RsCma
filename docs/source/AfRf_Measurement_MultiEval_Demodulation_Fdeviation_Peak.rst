Peak
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Fdeviation.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.demodulation.fdeviation.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Demodulation_Fdeviation_Peak_Delta.rst