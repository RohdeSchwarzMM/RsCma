Catalog
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:SPECtrum:CATalog:SOURce

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:SPECtrum:CATalog:SOURce



.. autoclass:: RsCma.Implementations.Trigger.GprfMeasurement.Spectrum.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: