FdError
----------------------------------------





.. autoclass:: RsCma.Implementations.Vse.Measurement.FdError.FdErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.vse.measurement.fdError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Vse_Measurement_FdError_Current.rst
	Vse_Measurement_FdError_Maximum.rst