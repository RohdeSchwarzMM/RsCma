User
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FREQuency:DELTa:USER

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FREQuency:DELTa:USER



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.Frequency.Delta.User.UserCls
	:members:
	:undoc-members:
	:noindex: