Evm
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Tetra.Evm.EvmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.limit.tetra.evm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Limit_Tetra_Evm_Peak.rst
	Configure_Vse_Measurement_Limit_Tetra_Evm_Rms.rst