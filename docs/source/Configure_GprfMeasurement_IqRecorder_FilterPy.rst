FilterPy
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FILTer:TYPE

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FILTer:TYPE



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.IqRecorder.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.iqRecorder.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_IqRecorder_FilterPy_Bandpass.rst
	Configure_GprfMeasurement_IqRecorder_FilterPy_Gauss.rst