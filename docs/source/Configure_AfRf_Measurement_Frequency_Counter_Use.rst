Use
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:USE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:USE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Frequency.Counter.Use.UseCls
	:members:
	:undoc-members:
	:noindex: