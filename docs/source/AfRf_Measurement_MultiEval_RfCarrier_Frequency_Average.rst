Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Frequency.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: