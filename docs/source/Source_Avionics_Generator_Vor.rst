Vor
----------------------------------------





.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Vor.VorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.vor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_Vor_AfSettings.rst
	Source_Avionics_Generator_Vor_IdSignal.rst
	Source_Avionics_Generator_Vor_RfSettings.rst
	Source_Avionics_Generator_Vor_State.rst