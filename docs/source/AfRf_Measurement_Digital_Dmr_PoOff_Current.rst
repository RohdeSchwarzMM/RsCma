Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:DMR:POOFf:CURRent
	single: READ:AFRF:MEASurement<Instance>:DIGital:DMR:POOFf:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:DMR:POOFf:CURRent
	READ:AFRF:MEASurement<Instance>:DIGital:DMR:POOFf:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Dmr.PoOff.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: