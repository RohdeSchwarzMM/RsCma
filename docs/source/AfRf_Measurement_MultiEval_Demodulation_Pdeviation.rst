Pdeviation
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Pdeviation.PdeviationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.demodulation.pdeviation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Demodulation_Pdeviation_Average.rst
	AfRf_Measurement_MultiEval_Demodulation_Pdeviation_Current.rst
	AfRf_Measurement_MultiEval_Demodulation_Pdeviation_Deviation.rst
	AfRf_Measurement_MultiEval_Demodulation_Pdeviation_Maximum.rst