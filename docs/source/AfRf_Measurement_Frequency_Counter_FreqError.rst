FreqError
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:FREQuency:COUNter:FERRor

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:FREQuency:COUNter:FERRor



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Frequency.Counter.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex: