Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:UPDate:DGRoup

.. code-block:: python

	SYSTem:UPDate:DGRoup



.. autoclass:: RsCma.Implementations.System.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: