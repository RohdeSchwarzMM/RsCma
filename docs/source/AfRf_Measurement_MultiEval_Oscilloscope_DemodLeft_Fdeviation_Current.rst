Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEMLeft:FDEViation:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEMLeft:FDEViation:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEMLeft:FDEViation:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEMLeft:FDEViation:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.DemodLeft.Fdeviation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: