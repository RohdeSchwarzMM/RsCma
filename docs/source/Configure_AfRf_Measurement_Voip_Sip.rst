Sip
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:SIP:STATe
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:SIP:CODE
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:SIP:RESPonse
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:SIP:RPRotocol
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:SIP:RCAuse
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:SIP:RTEXt

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:SIP:STATe
	CONFigure:AFRF:MEASurement<Instance>:VOIP:SIP:CODE
	CONFigure:AFRF:MEASurement<Instance>:VOIP:SIP:RESPonse
	CONFigure:AFRF:MEASurement<Instance>:VOIP:SIP:RPRotocol
	CONFigure:AFRF:MEASurement<Instance>:VOIP:SIP:RCAuse
	CONFigure:AFRF:MEASurement<Instance>:VOIP:SIP:RTEXt



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.Sip.SipCls
	:members:
	:undoc-members:
	:noindex: