Xvalues
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:TDOMain:XVALues

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:TDOMain:XVALues



.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.Tdomain.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: