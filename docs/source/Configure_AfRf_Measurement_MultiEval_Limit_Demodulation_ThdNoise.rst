ThdNoise
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:DEModulation:THDNoise

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:DEModulation:THDNoise



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Demodulation.ThdNoise.ThdNoiseCls
	:members:
	:undoc-members:
	:noindex: