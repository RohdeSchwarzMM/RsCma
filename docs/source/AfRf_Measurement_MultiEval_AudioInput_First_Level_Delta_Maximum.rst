Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:LEVel:DELTa:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:LEVel:DELTa:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:LEVel:DELTa:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:LEVel:DELTa:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.First.Level.Delta.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: