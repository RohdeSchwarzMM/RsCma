FfError
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.PtFive.FfError.FfErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.limit.ptFive.ffError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Limit_PtFive_FfError_Peak.rst
	Configure_Vse_Measurement_Limit_PtFive_FfError_Rms.rst