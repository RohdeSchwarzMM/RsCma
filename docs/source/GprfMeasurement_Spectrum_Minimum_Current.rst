Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:CURRent
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:CURRent
	READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:CURRent



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Minimum.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: