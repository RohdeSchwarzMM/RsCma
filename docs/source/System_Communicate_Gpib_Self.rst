Self
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:COMMunicate:GPIB[:SELF]:ENABle
	single: SYSTem:COMMunicate:GPIB[:SELF]:ADDR

.. code-block:: python

	SYSTem:COMMunicate:GPIB[:SELF]:ENABle
	SYSTem:COMMunicate:GPIB[:SELF]:ADDR



.. autoclass:: RsCma.Implementations.System.Communicate.Gpib.Self.SelfCls
	:members:
	:undoc-members:
	:noindex: