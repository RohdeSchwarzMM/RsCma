Dfrequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:DFRequency

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:DFRequency



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.FilterPy.Dfrequency.DfrequencyCls
	:members:
	:undoc-members:
	:noindex: