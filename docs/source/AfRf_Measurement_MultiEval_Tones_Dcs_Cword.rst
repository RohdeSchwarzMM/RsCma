Cword
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:CWORd
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:CWORd

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:CWORd
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:CWORd



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.Cword.CwordCls
	:members:
	:undoc-members:
	:noindex: