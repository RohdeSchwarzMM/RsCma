Battery<Battery>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix2
	rc = driver.sense.base.battery.repcap_battery_get()
	driver.sense.base.battery.repcap_battery_set(repcap.Battery.Ix1)



.. rubric:: SCPI Command:

.. index::
	single: SENSe:BASE:BATTery:AVAilable
	single: SENSe:BASE:BATTery:CAPacity
	single: SENSe:BASE:BATTery:TTD
	single: SENSe:BASE:BATTery:USAGe

.. code-block:: python

	SENSe:BASE:BATTery:AVAilable
	SENSe:BASE:BATTery:CAPacity
	SENSe:BASE:BATTery:TTD
	SENSe:BASE:BATTery:USAGe



.. autoclass:: RsCma.Implementations.Sense.Base.Battery.BatteryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.base.battery.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Base_Battery_Info.rst