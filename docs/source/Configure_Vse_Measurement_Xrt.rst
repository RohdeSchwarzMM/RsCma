Xrt
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:XRT:ENABle

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:XRT:ENABle



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Xrt.XrtCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.xrt.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Xrt_RfSettings.rst