Peak
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:NXDN:MERRor:PEAK

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:NXDN:MERRor:PEAK



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Nxdn.Merror.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: