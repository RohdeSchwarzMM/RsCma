Reflection
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:REVerse:LIMit:REFLection

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRT:REVerse:LIMit:REFLection



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.Reverse.Limit.Reflection.ReflectionCls
	:members:
	:undoc-members:
	:noindex: