Manual
----------------------------------------





.. autoclass:: RsCma.Implementations.Trigger.AfRf.Generator.Arb.Manual.ManualCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.afRf.generator.arb.manual.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_AfRf_Generator_Arb_Manual_Execute.rst