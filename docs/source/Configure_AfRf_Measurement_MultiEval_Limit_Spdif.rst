Spdif
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:SIN:SNRatio
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:SIN:SINad
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:SIN:SNNRatio
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:SIN:SNDRatio

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:SIN:SNRatio
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:SIN:SINad
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:SIN:SNNRatio
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:SIN:SNDRatio



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Spdif.SpdifCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.limit.spdif.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Limit_Spdif_ThDistortion.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Spdif_ThdNoise.rst