Bandwidth
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:IQRecorder:FILTer:GAUSs:BWIDth

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:IQRecorder:FILTer:GAUSs:BWIDth



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.IqRecorder.FilterPy.Gauss.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: