Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:FREQuency:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:FREQuency:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:FREQuency:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:FREQuency:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Voip.Frequency.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: