Uplink
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:UPLink:BCC
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:UPLink:MCC
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:UPLink:MNC

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:UPLink:BCC
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:UPLink:MCC
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:UPLink:MNC



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Tetra.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: