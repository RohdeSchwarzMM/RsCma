Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:AVERage
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:AVERage

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:AVERage
	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Fdeviation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: