Interferer
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IFERer:MODE
	single: SOURce:AFRF:GENerator<Instance>:IFERer:DFRequency
	single: SOURce:AFRF:GENerator<Instance>:IFERer:DLEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IFERer:MODE
	SOURce:AFRF:GENerator<Instance>:IFERer:DFRequency
	SOURce:AFRF:GENerator<Instance>:IFERer:DLEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Interferer.InterfererCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.interferer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Interferer_Af.rst
	Source_AfRf_Generator_Interferer_Modulator.rst
	Source_AfRf_Generator_Interferer_Rf.rst