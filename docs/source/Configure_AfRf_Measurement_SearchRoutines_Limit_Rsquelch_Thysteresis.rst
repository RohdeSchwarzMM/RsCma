Thysteresis
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:RSQuelch:THYSteresis

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:RSQuelch:THYSteresis



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Limit.Rsquelch.Thysteresis.ThysteresisCls
	:members:
	:undoc-members:
	:noindex: