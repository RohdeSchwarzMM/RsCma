Ttl
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TTL:PATTern
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TTL:ENABle
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TTL:INTerface
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TTL:CRATe
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TTL:BPFactor
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TTL:BPBits

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:TTL:PATTern
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TTL:ENABle
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TTL:INTerface
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TTL:CRATe
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TTL:BPFactor
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TTL:BPBits



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Ttl.TtlCls
	:members:
	:undoc-members:
	:noindex: