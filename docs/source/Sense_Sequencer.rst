Sequencer
----------------------------------------





.. autoclass:: RsCma.Implementations.Sense.Sequencer.SequencerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.sequencer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Sequencer_Tplan.rst