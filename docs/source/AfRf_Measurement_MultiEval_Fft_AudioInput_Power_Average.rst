Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.AudioInput.Power.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: