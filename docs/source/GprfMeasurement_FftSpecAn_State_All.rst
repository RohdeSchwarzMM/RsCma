All
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:STATe:ALL

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:STATe:ALL



.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: