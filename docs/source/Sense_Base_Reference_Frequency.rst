Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SENSe:BASE:REFerence:FREQuency:LOCKed
	single: SENSe:BASE:REFerence:FREQuency:OVENcold

.. code-block:: python

	SENSe:BASE:REFerence:FREQuency:LOCKed
	SENSe:BASE:REFerence:FREQuency:OVENcold



.. autoclass:: RsCma.Implementations.Sense.Base.Reference.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: