Rrc
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:TETRa:FILTer:RRC:ROFFfactor

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:TETRa:FILTer:RRC:ROFFfactor



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Tetra.FilterPy.Rrc.RrcCls
	:members:
	:undoc-members:
	:noindex: