Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Voip.AfSignal.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: