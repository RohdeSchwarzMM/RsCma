Spectrum
----------------------------------------





.. autoclass:: RsCma.Implementations.Display.GprfMeasurement.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.gprfMeasurement.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_GprfMeasurement_Spectrum_Application.rst
	Display_GprfMeasurement_Spectrum_Trace.rst