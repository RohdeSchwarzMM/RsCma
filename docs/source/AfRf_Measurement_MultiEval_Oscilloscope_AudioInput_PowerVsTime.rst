PowerVsTime
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.AudioInput.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.oscilloscope.audioInput.powerVsTime.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Oscilloscope_AudioInput_PowerVsTime_Current.rst