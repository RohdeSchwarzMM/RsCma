Nxdn
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:NXDN:TRANsmission

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:NXDN:TRANsmission



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Nxdn.NxdnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.nxdn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Nxdn_FilterPy.rst