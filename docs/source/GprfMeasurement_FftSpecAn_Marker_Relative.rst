Relative
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:MARKer<nr>:RELative

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:MARKer<nr>:RELative



.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.Marker.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: