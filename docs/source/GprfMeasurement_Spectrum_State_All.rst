All
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:STATe:ALL

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:STATe:ALL



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: