Fdeviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:FDEViation
	single: CALCulate:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:FDEViation
	single: READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:FDEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:FDEViation
	CALCulate:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:FDEViation
	READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:FDEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Tsensitivity.Fdeviation.FdeviationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.searchRoutines.tsensitivity.fdeviation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_SearchRoutines_Tsensitivity_Fdeviation_Trace.rst