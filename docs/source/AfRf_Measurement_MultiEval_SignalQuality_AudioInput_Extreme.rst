Extreme
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:EXTReme
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:EXTReme
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:EXTReme

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:EXTReme
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:EXTReme
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:EXTReme



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.AudioInput.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: