SndRatio
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:AIN<Nr>:SNDRatio

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:AIN<Nr>:SNDRatio



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.AudioInput.SndRatio.SndRatioCls
	:members:
	:undoc-members:
	:noindex: