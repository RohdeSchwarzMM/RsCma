Relative
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:MARKer<nr>:RELative

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:MARKer<nr>:RELative



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.ZeroSpan.Marker.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: