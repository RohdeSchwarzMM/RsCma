Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:MAXimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:MAXimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:MAXimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Maximum.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: