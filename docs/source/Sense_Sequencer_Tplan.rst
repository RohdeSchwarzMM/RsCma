Tplan
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SENSe:SEQuencer:TPLan:LIST
	single: SENSe:SEQuencer:TPLan:INFO

.. code-block:: python

	SENSe:SEQuencer:TPLan:LIST
	SENSe:SEQuencer:TPLan:INFO



.. autoclass:: RsCma.Implementations.Sense.Sequencer.Tplan.TplanCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.sequencer.tplan.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Sequencer_Tplan_Estatus.rst
	Sense_Sequencer_Tplan_State.rst