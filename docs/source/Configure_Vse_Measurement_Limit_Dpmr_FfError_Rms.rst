Rms
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:DPMR:FFERor:RMS

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:DPMR:FFERor:RMS



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Dpmr.FfError.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: