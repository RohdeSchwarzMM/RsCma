Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:LTE:MODulation:CURRent
	single: READ:VSE:MEASurement<Instance>:LTE:MODulation:CURRent

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:LTE:MODulation:CURRent
	READ:VSE:MEASurement<Instance>:LTE:MODulation:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.Lte.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: