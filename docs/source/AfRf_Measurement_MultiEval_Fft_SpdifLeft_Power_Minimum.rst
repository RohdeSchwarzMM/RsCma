Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.SpdifLeft.Power.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: