Range
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:ARB:SAMPles:RANGe

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:ARB:SAMPles:RANGe



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Arb.Samples.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: