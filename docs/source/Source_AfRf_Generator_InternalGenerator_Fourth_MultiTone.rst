MultiTone
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator:FOURth:MTONe:TLEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator:FOURth:MTONe:TLEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.Fourth.MultiTone.MultiToneCls
	:members:
	:undoc-members:
	:noindex: