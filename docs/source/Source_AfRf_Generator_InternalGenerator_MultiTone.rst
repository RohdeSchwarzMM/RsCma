MultiTone
----------------------------------------





.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.MultiTone.MultiToneCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.internalGenerator.multiTone.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_InternalGenerator_MultiTone_Crest.rst
	Source_AfRf_Generator_InternalGenerator_MultiTone_Enable.rst
	Source_AfRf_Generator_InternalGenerator_MultiTone_Frequency.rst
	Source_AfRf_Generator_InternalGenerator_MultiTone_Ilevel.rst
	Source_AfRf_Generator_InternalGenerator_MultiTone_Level.rst
	Source_AfRf_Generator_InternalGenerator_MultiTone_Tlevel.rst
	Source_AfRf_Generator_InternalGenerator_MultiTone_Tone.rst