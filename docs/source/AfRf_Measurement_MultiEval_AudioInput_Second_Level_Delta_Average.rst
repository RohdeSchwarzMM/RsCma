Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:LEVel:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:LEVel:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:LEVel:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:LEVel:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.Second.Level.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: