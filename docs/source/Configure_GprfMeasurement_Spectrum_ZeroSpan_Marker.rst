Marker
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.ZeroSpan.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.spectrum.zeroSpan.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_Spectrum_ZeroSpan_Marker_Placement.rst
	Configure_GprfMeasurement_Spectrum_ZeroSpan_Marker_Range.rst