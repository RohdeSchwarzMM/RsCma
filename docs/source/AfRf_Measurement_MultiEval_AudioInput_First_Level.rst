Level
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.First.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.audioInput.first.level.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_AudioInput_First_Level_Delta.rst