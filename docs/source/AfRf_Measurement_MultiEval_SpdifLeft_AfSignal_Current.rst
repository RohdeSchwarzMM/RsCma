Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifLeft.AfSignal.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: