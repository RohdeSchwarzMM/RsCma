Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.ModDepth.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: