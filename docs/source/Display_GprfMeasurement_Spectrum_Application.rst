Application
----------------------------------------





.. autoclass:: RsCma.Implementations.Display.GprfMeasurement.Spectrum.Application.ApplicationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.gprfMeasurement.spectrum.application.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_GprfMeasurement_Spectrum_Application_Select.rst