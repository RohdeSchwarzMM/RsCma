Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:PTFive:MFIDelity:CURRent
	single: READ:VSE:MEASurement<Instance>:PTFive:MFIDelity:CURRent
	single: CALCulate:VSE:MEASurement<Instance>:PTFive:MFIDelity:CURRent

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:PTFive:MFIDelity:CURRent
	READ:VSE:MEASurement<Instance>:PTFive:MFIDelity:CURRent
	CALCulate:VSE:MEASurement<Instance>:PTFive:MFIDelity:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.PtFive.Mfidelity.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: