Spdif<Channel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.afRf.measurement.multiEval.fft.spdif.repcap_channel_get()
	driver.afRf.measurement.multiEval.fft.spdif.repcap_channel_set(repcap.Channel.Nr1)





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Spdif.SpdifCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.fft.spdif.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Fft_Spdif_Marker.rst