Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:AVERage
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:AVERage
	READ:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:AVERage



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Maximum.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: