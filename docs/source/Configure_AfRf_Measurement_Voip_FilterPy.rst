FilterPy
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:WEIGhting
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:DFRequency
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:ROBustauto
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:LPASs
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:HPASs

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:WEIGhting
	CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:DFRequency
	CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:ROBustauto
	CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:LPASs
	CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:HPASs



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.voip.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Voip_FilterPy_Bpass.rst
	Configure_AfRf_Measurement_Voip_FilterPy_Dwidth.rst
	Configure_AfRf_Measurement_Voip_FilterPy_Notch.rst