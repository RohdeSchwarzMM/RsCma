Adjustment
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:ADJustment:TYPE
	single: CONFigure:BASE:ADJustment:VALue
	single: CONFigure:BASE:ADJustment:SAVE

.. code-block:: python

	CONFigure:BASE:ADJustment:TYPE
	CONFigure:BASE:ADJustment:VALue
	CONFigure:BASE:ADJustment:SAVE



.. autoclass:: RsCma.Implementations.Configure.Base.Adjustment.AdjustmentCls
	:members:
	:undoc-members:
	:noindex: