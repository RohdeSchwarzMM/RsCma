Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.ModDepth.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: