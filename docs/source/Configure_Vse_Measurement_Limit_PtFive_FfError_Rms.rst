Rms
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:PTFive:FFERor:RMS

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:PTFive:FFERor:RMS



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.PtFive.FfError.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: