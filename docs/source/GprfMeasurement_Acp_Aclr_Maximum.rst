Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:ACP:ACLR:MAXimum
	single: READ:GPRF:MEASurement<Instance>:ACP:ACLR:MAXimum
	single: CALCulate:GPRF:MEASurement<Instance>:ACP:ACLR:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:ACP:ACLR:MAXimum
	READ:GPRF:MEASurement<Instance>:ACP:ACLR:MAXimum
	CALCulate:GPRF:MEASurement<Instance>:ACP:ACLR:MAXimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Aclr.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: