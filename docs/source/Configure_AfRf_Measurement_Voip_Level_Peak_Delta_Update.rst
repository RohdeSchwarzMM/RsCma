Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:LEVel:PEAK:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:LEVel:PEAK:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.Level.Peak.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: