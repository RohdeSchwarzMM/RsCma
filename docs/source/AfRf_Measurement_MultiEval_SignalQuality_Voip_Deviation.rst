Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:DEViation
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:DEViation

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:DEViation
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.Voip.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: