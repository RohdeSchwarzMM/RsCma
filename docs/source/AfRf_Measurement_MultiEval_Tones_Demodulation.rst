Demodulation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Demodulation.DemodulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.tones.demodulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Tones_Demodulation_Repetitions.rst
	AfRf_Measurement_MultiEval_Tones_Demodulation_Sequence.rst