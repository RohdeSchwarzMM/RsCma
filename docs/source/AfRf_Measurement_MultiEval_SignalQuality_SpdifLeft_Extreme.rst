Extreme
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:EXTReme
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:EXTReme
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:EXTReme

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:EXTReme
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:EXTReme
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:EXTReme



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.SpdifLeft.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: