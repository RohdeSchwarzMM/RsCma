Out
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:BASE:OUT:SOURce

.. code-block:: python

	TRIGger:BASE:OUT:SOURce



.. autoclass:: RsCma.Implementations.Trigger.Base.Out.OutCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.base.out.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Base_Out_Catalog.rst