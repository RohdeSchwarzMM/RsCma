Ccode
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DPMR:CCODe:CALCulation
	single: SOURce:AFRF:GENerator<Instance>:DPMR:CCODe

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DPMR:CCODe:CALCulation
	SOURce:AFRF:GENerator<Instance>:DPMR:CCODe



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dpmr.Ccode.CcodeCls
	:members:
	:undoc-members:
	:noindex: