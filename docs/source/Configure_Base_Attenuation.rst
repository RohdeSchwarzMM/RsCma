Attenuation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:ATTenuation:ENABle
	single: CONFigure:BASE:ATTenuation:AWARning

.. code-block:: python

	CONFigure:BASE:ATTenuation:ENABle
	CONFigure:BASE:ATTenuation:AWARning



.. autoclass:: RsCma.Implementations.Configure.Base.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex: