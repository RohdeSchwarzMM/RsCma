Lte
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:IQRecorder:LTE:CBWidth

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:IQRecorder:LTE:CBWidth



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.IqRecorder.Lte.LteCls
	:members:
	:undoc-members:
	:noindex: