Absolute
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:MARKer<nr>:ABSolute

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:MARKer<nr>:ABSolute



.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.Marker.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex: