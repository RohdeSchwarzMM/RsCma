FreqSweep
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.FreqSweep.FreqSweepCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.spectrum.freqSweep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Spectrum_FreqSweep_Xvalues.rst