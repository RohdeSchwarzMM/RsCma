GprfMeasurement
----------------------------------------





.. autoclass:: RsCma.Implementations.Calibration.GprfMeasurement.GprfMeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calibration.gprfMeasurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_GprfMeasurement_ExtPwrSensor.rst
	Calibration_GprfMeasurement_Nrt.rst
	Calibration_GprfMeasurement_Spectrum.rst