Level
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:AOUT<nr>:LEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:AOUT<nr>:LEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.AudioOutput.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: