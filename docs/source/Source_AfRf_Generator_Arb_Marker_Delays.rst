Delays
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:ARB:MARKer:DELays

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:ARB:MARKer:DELays



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Arb.Marker.Delays.DelaysCls
	:members:
	:undoc-members:
	:noindex: