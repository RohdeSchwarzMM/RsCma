Pdeviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:PDEViation
	single: CALCulate:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:PDEViation
	single: READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:PDEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:PDEViation
	CALCulate:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:PDEViation
	READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:PDEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Tsensitivity.Pdeviation.PdeviationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.searchRoutines.tsensitivity.pdeviation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_SearchRoutines_Tsensitivity_Pdeviation_Trace.rst