LrfLevel
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:LRFLevel

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:LRFLevel



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.LrfLevel.LrfLevelCls
	:members:
	:undoc-members:
	:noindex: