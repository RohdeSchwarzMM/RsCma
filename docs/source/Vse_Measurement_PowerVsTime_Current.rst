Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:PVTime:CURRent
	single: READ:VSE:MEASurement<Instance>:PVTime:CURRent

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:PVTime:CURRent
	READ:VSE:MEASurement<Instance>:PVTime:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.PowerVsTime.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: