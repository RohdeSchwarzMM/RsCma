Forward
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Nrt.Forward.ForwardCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.nrt.forward.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Nrt_Forward_Average.rst
	GprfMeasurement_Nrt_Forward_Current.rst
	GprfMeasurement_Nrt_Forward_Maximum.rst
	GprfMeasurement_Nrt_Forward_Minimum.rst