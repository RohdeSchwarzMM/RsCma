Dpmr
----------------------------------------





.. autoclass:: RsCma.Implementations.Vse.Measurement.Dpmr.DpmrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.vse.measurement.dpmr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Vse_Measurement_Dpmr_Symbols.rst