FmStereo
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.FmStereo.FmStereoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.demodulation.fmStereo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Demodulation_FmStereo_Average.rst
	AfRf_Measurement_MultiEval_Demodulation_FmStereo_Current.rst
	AfRf_Measurement_MultiEval_Demodulation_FmStereo_Deviation.rst
	AfRf_Measurement_MultiEval_Demodulation_FmStereo_Maximum.rst