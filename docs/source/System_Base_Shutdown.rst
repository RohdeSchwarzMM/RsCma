Shutdown
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:SHUTdown

.. code-block:: python

	SYSTem:BASE:SHUTdown



.. autoclass:: RsCma.Implementations.System.Base.Shutdown.ShutdownCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.base.shutdown.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Base_Shutdown_Device.rst