Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.BitErrorRate.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: