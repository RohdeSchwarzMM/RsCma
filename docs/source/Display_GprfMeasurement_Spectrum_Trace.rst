Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: DISPlay:GPRF:MEASurement<Instance>:SPECtrum:TRACe

.. code-block:: python

	DISPlay:GPRF:MEASurement<Instance>:SPECtrum:TRACe



.. autoclass:: RsCma.Implementations.Display.GprfMeasurement.Spectrum.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: