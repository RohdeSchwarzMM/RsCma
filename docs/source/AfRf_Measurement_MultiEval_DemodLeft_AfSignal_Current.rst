Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodLeft.AfSignal.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: