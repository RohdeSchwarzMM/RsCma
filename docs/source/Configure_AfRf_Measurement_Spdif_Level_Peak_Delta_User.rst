User
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:PEAK:DELTa:USER

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:PEAK:DELTa:USER



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.Level.Peak.Delta.User.UserCls
	:members:
	:undoc-members:
	:noindex: