Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:FFERror:MAXimum
	single: READ:VSE:MEASurement<Instance>:FFERror:MAXimum
	single: CALCulate:VSE:MEASurement<Instance>:FFERror:MAXimum

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:FFERror:MAXimum
	READ:VSE:MEASurement<Instance>:FFERror:MAXimum
	CALCulate:VSE:MEASurement<Instance>:FFERror:MAXimum



.. autoclass:: RsCma.Implementations.Vse.Measurement.FfError.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: