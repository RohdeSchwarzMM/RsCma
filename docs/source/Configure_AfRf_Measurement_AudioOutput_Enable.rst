Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AOUT<nr>:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AOUT<nr>:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioOutput.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: