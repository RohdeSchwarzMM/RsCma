Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:PTFive:BERate:MAXimum
	single: CALCulate:AFRF:MEASurement<Instance>:DIGital:PTFive:BERate:MAXimum
	single: READ:AFRF:MEASurement<Instance>:DIGital:PTFive:BERate:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:PTFive:BERate:MAXimum
	CALCulate:AFRF:MEASurement<Instance>:DIGital:PTFive:BERate:MAXimum
	READ:AFRF:MEASurement<Instance>:DIGital:PTFive:BERate:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.PtFive.BitErrorRate.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: