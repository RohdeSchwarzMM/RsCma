Spectrum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:SPECtrum
	single: STOP:GPRF:MEASurement<Instance>:SPECtrum
	single: ABORt:GPRF:MEASurement<Instance>:SPECtrum

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:SPECtrum
	STOP:GPRF:MEASurement<Instance>:SPECtrum
	ABORt:GPRF:MEASurement<Instance>:SPECtrum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Spectrum_Average.rst
	GprfMeasurement_Spectrum_FreqSweep.rst
	GprfMeasurement_Spectrum_Frequency.rst
	GprfMeasurement_Spectrum_Maximum.rst
	GprfMeasurement_Spectrum_Minimum.rst
	GprfMeasurement_Spectrum_ReferenceMarker.rst
	GprfMeasurement_Spectrum_Rms.rst
	GprfMeasurement_Spectrum_Sample.rst
	GprfMeasurement_Spectrum_State.rst
	GprfMeasurement_Spectrum_Tgenerator.rst
	GprfMeasurement_Spectrum_ZeroSpan.rst