SnnRatio
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:AIN<Nr>:SNNRatio

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:AIN<Nr>:SNNRatio



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.AudioInput.SnnRatio.SnnRatioCls
	:members:
	:undoc-members:
	:noindex: