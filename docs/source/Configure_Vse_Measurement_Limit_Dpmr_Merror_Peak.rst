Peak
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:DPMR:MERRor:PEAK

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:DPMR:MERRor:PEAK



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Dpmr.Merror.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: