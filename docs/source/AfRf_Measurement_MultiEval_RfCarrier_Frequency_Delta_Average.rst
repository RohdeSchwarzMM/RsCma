Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Frequency.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: