Sample
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Sample.SampleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.spectrum.sample.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Spectrum_Sample_Average.rst
	GprfMeasurement_Spectrum_Sample_Current.rst
	GprfMeasurement_Spectrum_Sample_Maximum.rst
	GprfMeasurement_Spectrum_Sample_Minimum.rst