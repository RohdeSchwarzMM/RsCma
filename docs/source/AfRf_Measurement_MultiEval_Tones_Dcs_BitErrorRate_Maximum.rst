Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.BitErrorRate.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: