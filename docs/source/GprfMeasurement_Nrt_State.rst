State
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:NRT:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:NRT:STATe



.. autoclass:: RsCma.Implementations.GprfMeasurement.Nrt.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.nrt.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Nrt_State_All.rst