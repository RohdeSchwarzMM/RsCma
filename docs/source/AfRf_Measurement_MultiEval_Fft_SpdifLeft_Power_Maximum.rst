Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.SpdifLeft.Power.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: