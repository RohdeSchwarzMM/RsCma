Dmr
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:DMR:SRATe
	single: CONFigure:VSE:MEASurement<Instance>:DMR:DEModulation

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:DMR:SRATe
	CONFigure:VSE:MEASurement<Instance>:DMR:DEModulation



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Dmr.DmrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.dmr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Dmr_FilterPy.rst