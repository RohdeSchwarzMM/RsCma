Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:AVERage:MAXimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:AVERage:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:AVERage:MAXimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:AVERage:MAXimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Average.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: