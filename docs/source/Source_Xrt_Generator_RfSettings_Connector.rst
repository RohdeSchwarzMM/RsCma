Connector<Connector>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.source.xrt.generator.rfSettings.connector.repcap_connector_get()
	driver.source.xrt.generator.rfSettings.connector.repcap_connector_set(repcap.Connector.Nr1)





.. autoclass:: RsCma.Implementations.Source.Xrt.Generator.RfSettings.Connector.ConnectorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.xrt.generator.rfSettings.connector.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Xrt_Generator_RfSettings_Connector_Enable.rst