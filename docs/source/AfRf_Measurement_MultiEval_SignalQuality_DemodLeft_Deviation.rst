Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:DEViation
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:DEViation

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:DEViation
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.DemodLeft.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: