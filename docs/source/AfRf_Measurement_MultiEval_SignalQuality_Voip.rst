Voip
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.Voip.VoipCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.signalQuality.voip.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_SignalQuality_Voip_Average.rst
	AfRf_Measurement_MultiEval_SignalQuality_Voip_Current.rst
	AfRf_Measurement_MultiEval_SignalQuality_Voip_Deviation.rst
	AfRf_Measurement_MultiEval_SignalQuality_Voip_Extreme.rst