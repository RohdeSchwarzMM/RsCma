Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:AVERage
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:AVERage

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:AVERage
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.SpdifRight.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: