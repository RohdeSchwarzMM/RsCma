Uri
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:VOIP:URI:PORT
	single: SOURce:AFRF:GENerator<Instance>:VOIP:URI:CMA
	single: SOURce:AFRF:GENerator<Instance>:VOIP:URI:IP
	single: SOURce:AFRF:GENerator<Instance>:VOIP:URI:USER

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:VOIP:URI:PORT
	SOURce:AFRF:GENerator<Instance>:VOIP:URI:CMA
	SOURce:AFRF:GENerator<Instance>:VOIP:URI:IP
	SOURce:AFRF:GENerator<Instance>:VOIP:URI:USER



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Voip.Uri.UriCls
	:members:
	:undoc-members:
	:noindex: