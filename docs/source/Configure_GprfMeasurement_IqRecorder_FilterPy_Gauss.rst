Gauss
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FILTer:GAUSs:BWIDth

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FILTer:GAUSs:BWIDth



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.IqRecorder.FilterPy.Gauss.GaussCls
	:members:
	:undoc-members:
	:noindex: