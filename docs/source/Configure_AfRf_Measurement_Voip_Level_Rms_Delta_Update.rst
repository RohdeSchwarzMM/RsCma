Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:LEVel:RMS:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:LEVel:RMS:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.Level.Rms.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: