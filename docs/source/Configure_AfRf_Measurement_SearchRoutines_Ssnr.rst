Ssnr
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:REPetition
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:SCONdition
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:MOEXception
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:SCOunt
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:AFSource
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:CREPetition
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:RCOupling

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:REPetition
	CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:SCONdition
	CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:MOEXception
	CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:SCOunt
	CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:AFSource
	CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:CREPetition
	CONFigure:AFRF:MEASurement<Instance>:SROutines:SSNR:RCOupling



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Ssnr.SsnrCls
	:members:
	:undoc-members:
	:noindex: