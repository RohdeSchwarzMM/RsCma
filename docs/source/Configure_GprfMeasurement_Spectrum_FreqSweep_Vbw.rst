Vbw
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:VBW:AUTO
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:VBW

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:VBW:AUTO
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:VBW



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.FreqSweep.Vbw.VbwCls
	:members:
	:undoc-members:
	:noindex: