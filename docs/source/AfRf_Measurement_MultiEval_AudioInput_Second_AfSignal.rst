AfSignal
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.Second.AfSignal.AfSignalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.audioInput.second.afSignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_AudioInput_Second_AfSignal_Average.rst
	AfRf_Measurement_MultiEval_AudioInput_Second_AfSignal_Current.rst
	AfRf_Measurement_MultiEval_AudioInput_Second_AfSignal_Deviation.rst
	AfRf_Measurement_MultiEval_AudioInput_Second_AfSignal_Maximum.rst