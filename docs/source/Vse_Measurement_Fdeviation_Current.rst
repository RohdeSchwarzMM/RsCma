Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:FDEViation:CURRent
	single: READ:VSE:MEASurement<Instance>:FDEViation:CURRent

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:FDEViation:CURRent
	READ:VSE:MEASurement<Instance>:FDEViation:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.Fdeviation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: