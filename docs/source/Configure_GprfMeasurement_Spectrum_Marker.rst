Marker
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:MARKer:DETector

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:MARKer:DETector



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.spectrum.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_Spectrum_Marker_Enable.rst