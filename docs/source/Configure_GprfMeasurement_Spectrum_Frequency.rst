Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:CENTer
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:STARt
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:STOP

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:CENTer
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:STARt
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:STOP



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.spectrum.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_Spectrum_Frequency_Marker.rst
	Configure_GprfMeasurement_Spectrum_Frequency_Span.rst