Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:ACP:POWer:MINimum
	single: READ:GPRF:MEASurement<Instance>:ACP:POWer:MINimum
	single: CALCulate:GPRF:MEASurement<Instance>:ACP:POWer:MINimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:ACP:POWer:MINimum
	READ:GPRF:MEASurement<Instance>:ACP:POWer:MINimum
	CALCulate:GPRF:MEASurement<Instance>:ACP:POWer:MINimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Power.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: