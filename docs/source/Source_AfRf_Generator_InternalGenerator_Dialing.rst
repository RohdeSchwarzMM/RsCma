Dialing
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:DIALing:STARt
	single: SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:DIALing

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:DIALing:STARt
	SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:DIALing



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.Dialing.DialingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.internalGenerator.dialing.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_InternalGenerator_Dialing_Mode.rst