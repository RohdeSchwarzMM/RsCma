Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.Pdeviation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: