Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:DEViation
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:DEViation

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:DEViation
	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Pdeviation.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: