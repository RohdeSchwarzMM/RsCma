Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMRight:FDEViation:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMRight:FDEViation:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMRight:FDEViation:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMRight:FDEViation:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.DemodRight.Fdeviation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: