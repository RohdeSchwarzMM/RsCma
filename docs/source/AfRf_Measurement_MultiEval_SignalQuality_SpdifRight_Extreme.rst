Extreme
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:EXTReme
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:EXTReme
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:EXTReme

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:EXTReme
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:EXTReme
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:EXTReme



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.SpdifRight.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: