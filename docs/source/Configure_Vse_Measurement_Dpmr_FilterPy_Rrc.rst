Rrc
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:DPMR:FILTer:RRC:ROFFfactor

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:DPMR:FILTer:RRC:ROFFfactor



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Dpmr.FilterPy.Rrc.RrcCls
	:members:
	:undoc-members:
	:noindex: