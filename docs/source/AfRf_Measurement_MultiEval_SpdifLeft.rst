SpdifLeft
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifLeft.SpdifLeftCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.spdifLeft.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_SpdifLeft_AfSignal.rst
	AfRf_Measurement_MultiEval_SpdifLeft_Frequency.rst
	AfRf_Measurement_MultiEval_SpdifLeft_Level.rst