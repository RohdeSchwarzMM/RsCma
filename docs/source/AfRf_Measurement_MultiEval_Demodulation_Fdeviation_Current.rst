Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:CURRent
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:CURRent

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:CURRent
	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Fdeviation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: