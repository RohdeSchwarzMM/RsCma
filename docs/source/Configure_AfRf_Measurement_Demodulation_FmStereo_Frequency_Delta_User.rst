User
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FMSTereo:FREQuency:DELTa:USER

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FMSTereo:FREQuency:DELTa:USER



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.FmStereo.Frequency.Delta.User.UserCls
	:members:
	:undoc-members:
	:noindex: