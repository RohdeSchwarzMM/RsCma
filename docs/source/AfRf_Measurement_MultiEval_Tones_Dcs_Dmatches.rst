Dmatches
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:DMATches
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:DMATches

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:DMATches
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:DMATches



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.Dmatches.DmatchesCls
	:members:
	:undoc-members:
	:noindex: