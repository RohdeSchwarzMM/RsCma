Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: READ:VSE:MEASurement<Instance>:LTE:POWer:CURRent
	single: FETCh:VSE:MEASurement<Instance>:LTE:POWer:CURRent

.. code-block:: python

	READ:VSE:MEASurement<Instance>:LTE:POWer:CURRent
	FETCh:VSE:MEASurement<Instance>:LTE:POWer:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.Lte.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: