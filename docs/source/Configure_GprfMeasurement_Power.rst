Power
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:TOUT
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:SLENgth
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:MLENgth
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:REPetition
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:RCOupling
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:SCOunt

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:TOUT
	CONFigure:GPRF:MEASurement<Instance>:POWer:SLENgth
	CONFigure:GPRF:MEASurement<Instance>:POWer:MLENgth
	CONFigure:GPRF:MEASurement<Instance>:POWer:REPetition
	CONFigure:GPRF:MEASurement<Instance>:POWer:RCOupling
	CONFigure:GPRF:MEASurement<Instance>:POWer:SCOunt



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_Power_FilterPy.rst