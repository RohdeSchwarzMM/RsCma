Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:LEVel:DELTa:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:LEVel:DELTa:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:LEVel:DELTa:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:LEVel:DELTa:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifLeft.Level.Delta.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: