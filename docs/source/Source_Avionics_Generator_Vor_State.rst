State
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:VOR:STATe:ALL
	single: SOURce:AVIonics:GENerator<Instance>:VOR:STATe

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:VOR:STATe:ALL
	SOURce:AVIonics:GENerator<Instance>:VOR:STATe



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Vor.State.StateCls
	:members:
	:undoc-members:
	:noindex: