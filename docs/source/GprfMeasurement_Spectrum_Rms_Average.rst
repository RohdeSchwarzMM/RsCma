Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:AVERage
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:AVERage
	READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:AVERage



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Rms.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: