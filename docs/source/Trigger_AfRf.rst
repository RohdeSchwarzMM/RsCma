AfRf
----------------------------------------





.. autoclass:: RsCma.Implementations.Trigger.AfRf.AfRfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.afRf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_AfRf_Generator.rst
	Trigger_AfRf_Measurement.rst