Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.Pdeviation.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: