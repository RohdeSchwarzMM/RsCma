Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:RFCarrier:PEPower:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:RFCarrier:PEPower:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.RfCarrier.PePower.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: