Sfactor
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:DWIDth:SFACtor

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:DWIDth:SFACtor



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.FilterPy.Dwidth.Sfactor.SfactorCls
	:members:
	:undoc-members:
	:noindex: