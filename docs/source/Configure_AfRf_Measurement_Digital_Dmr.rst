Dmr
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:LDIRection
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:PTYPe
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:BERPeriod
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:CMODe
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:FILTer
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:ROFactor
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:SRATe
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:SDEViation
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:MODE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:LDIRection
	CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:PTYPe
	CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:BERPeriod
	CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:CMODe
	CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:FILTer
	CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:ROFactor
	CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:SRATe
	CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:SDEViation
	CONFigure:AFRF:MEASurement<Instance>:DIGital:DMR:MODE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Dmr.DmrCls
	:members:
	:undoc-members:
	:noindex: