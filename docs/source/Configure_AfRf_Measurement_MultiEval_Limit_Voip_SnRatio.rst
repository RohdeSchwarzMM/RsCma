SnRatio
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:VOIP:SNRatio

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:VOIP:SNRatio



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Voip.SnRatio.SnRatioCls
	:members:
	:undoc-members:
	:noindex: