Range
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:MARKer<nr>:RANGe

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:MARKer<nr>:RANGe



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.ZeroSpan.Marker.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: