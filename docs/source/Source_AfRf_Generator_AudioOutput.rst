AudioOutput<AudioOutput>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.source.afRf.generator.audioOutput.repcap_audioOutput_get()
	driver.source.afRf.generator.audioOutput.repcap_audioOutput_set(repcap.AudioOutput.Nr1)



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:AOUT<nr>

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:AOUT<nr>



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.AudioOutput.AudioOutputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.audioOutput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_AudioOutput_Enable.rst
	Source_AfRf_Generator_AudioOutput_First.rst
	Source_AfRf_Generator_AudioOutput_Level.rst
	Source_AfRf_Generator_AudioOutput_Second.rst