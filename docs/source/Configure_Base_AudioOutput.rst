AudioOutput<AudioOutput>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.base.audioOutput.repcap_audioOutput_get()
	driver.configure.base.audioOutput.repcap_audioOutput_set(repcap.AudioOutput.Nr1)





.. autoclass:: RsCma.Implementations.Configure.Base.AudioOutput.AudioOutputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.base.audioOutput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Base_AudioOutput_Dimpedance.rst
	Configure_Base_AudioOutput_Ecircuitry.rst
	Configure_Base_AudioOutput_Eimpedance.rst
	Configure_Base_AudioOutput_Limpedance.rst
	Configure_Base_AudioOutput_Zbox.rst