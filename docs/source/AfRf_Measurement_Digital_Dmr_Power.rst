Power
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Dmr.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.digital.dmr.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_Digital_Dmr_Power_Current.rst