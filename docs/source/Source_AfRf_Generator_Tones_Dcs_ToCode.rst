ToCode
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:TONes:DCS:TOCode:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:TONes:DCS:TOCode:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Tones.Dcs.ToCode.ToCodeCls
	:members:
	:undoc-members:
	:noindex: