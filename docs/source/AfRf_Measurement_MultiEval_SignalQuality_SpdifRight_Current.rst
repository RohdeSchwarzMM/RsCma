Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:CURRent
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:CURRent

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:CURRent
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.SpdifRight.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: