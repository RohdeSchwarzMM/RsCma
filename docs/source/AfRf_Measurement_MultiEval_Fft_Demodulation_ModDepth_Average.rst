Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.ModDepth.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: