Rms
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:NXDN:MERRor:RMS

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:NXDN:MERRor:RMS



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Nxdn.Merror.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: