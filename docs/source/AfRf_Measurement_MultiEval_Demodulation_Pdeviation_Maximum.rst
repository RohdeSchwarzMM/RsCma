Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:MAXimum
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:MAXimum

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:MAXimum
	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Pdeviation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: