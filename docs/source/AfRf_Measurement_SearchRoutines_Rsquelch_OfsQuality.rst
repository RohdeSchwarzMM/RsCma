OfsQuality
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:OFSQuality
	single: READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:OFSQuality

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:OFSQuality
	READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:OFSQuality



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsquelch.OfsQuality.OfsQualityCls
	:members:
	:undoc-members:
	:noindex: