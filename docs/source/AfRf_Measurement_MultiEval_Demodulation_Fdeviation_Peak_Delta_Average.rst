Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:PEAK:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:PEAK:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:PEAK:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:PEAK:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Fdeviation.Peak.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: