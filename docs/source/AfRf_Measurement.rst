Measurement
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_Digital.rst
	AfRf_Measurement_Frequency.rst
	AfRf_Measurement_MultiEval.rst
	AfRf_Measurement_SearchRoutines.rst