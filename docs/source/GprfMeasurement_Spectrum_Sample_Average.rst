Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:AVERage
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:AVERage
	READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:AVERage



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Sample.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: