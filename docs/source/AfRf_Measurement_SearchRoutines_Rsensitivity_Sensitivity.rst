Sensitivity
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:SENSitivity
	single: CALCulate:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:SENSitivity
	single: READ:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:SENSitivity

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:SENSitivity
	CALCulate:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:SENSitivity
	READ:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:SENSitivity



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsensitivity.Sensitivity.SensitivityCls
	:members:
	:undoc-members:
	:noindex: