All
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:STATe:ALL

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:STATe:ALL



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: