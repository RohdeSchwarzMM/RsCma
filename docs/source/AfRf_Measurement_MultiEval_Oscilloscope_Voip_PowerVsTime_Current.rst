Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:PVTime:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:PVTime:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:PVTime:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:PVTime:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.Voip.PowerVsTime.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: