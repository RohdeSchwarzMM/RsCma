Catalog
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:POWer:CATalog:SOURce

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:POWer:CATalog:SOURce



.. autoclass:: RsCma.Implementations.Trigger.GprfMeasurement.Power.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: