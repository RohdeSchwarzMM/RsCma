Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:BPASs:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:BPASs:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.FilterPy.Bpass.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: