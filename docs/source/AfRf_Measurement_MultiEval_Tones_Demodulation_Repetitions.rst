Repetitions
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation:REPetitions
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation:REPetitions

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation:REPetitions
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation:REPetitions



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Demodulation.Repetitions.RepetitionsCls
	:members:
	:undoc-members:
	:noindex: