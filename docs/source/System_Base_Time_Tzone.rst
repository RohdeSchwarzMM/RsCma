Tzone
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:TIME:TZONe

.. code-block:: python

	SYSTem:BASE:TIME:TZONe



.. autoclass:: RsCma.Implementations.System.Base.Time.Tzone.TzoneCls
	:members:
	:undoc-members:
	:noindex: