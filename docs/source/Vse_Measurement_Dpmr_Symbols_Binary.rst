Binary
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:DPMR:SYMBols:BINary
	single: READ:VSE:MEASurement<Instance>:DPMR:SYMBols:BINary

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:DPMR:SYMBols:BINary
	READ:VSE:MEASurement<Instance>:DPMR:SYMBols:BINary



.. autoclass:: RsCma.Implementations.Vse.Measurement.Dpmr.Symbols.Binary.BinaryCls
	:members:
	:undoc-members:
	:noindex: