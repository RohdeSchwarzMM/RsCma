Select
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: DISPlay:GPRF:MEASurement<Instance>:SPECtrum:APPLication:SELect

.. code-block:: python

	DISPlay:GPRF:MEASurement<Instance>:SPECtrum:APPLication:SELect



.. autoclass:: RsCma.Implementations.Display.GprfMeasurement.Spectrum.Application.Select.SelectCls
	:members:
	:undoc-members:
	:noindex: