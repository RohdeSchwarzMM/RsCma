Swr
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:REVerse:LIMit:SWR

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRT:REVerse:LIMit:SWR



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.Reverse.Limit.Swr.SwrCls
	:members:
	:undoc-members:
	:noindex: