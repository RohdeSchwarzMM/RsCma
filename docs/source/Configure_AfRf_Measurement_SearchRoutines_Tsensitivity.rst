Tsensitivity
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:TRELative
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:TFDeviation
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:TPDeviation
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:TMDepth
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:TTOLerance
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:TPARameter

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:TRELative
	CONFigure:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:TFDeviation
	CONFigure:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:TPDeviation
	CONFigure:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:TMDepth
	CONFigure:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:TTOLerance
	CONFigure:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:TPARameter



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Tsensitivity.TsensitivityCls
	:members:
	:undoc-members:
	:noindex: