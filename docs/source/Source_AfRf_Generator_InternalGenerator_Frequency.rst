Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:FREQuency

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:FREQuency



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: