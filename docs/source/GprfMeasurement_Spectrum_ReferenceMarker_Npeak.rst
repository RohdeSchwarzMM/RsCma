Npeak
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:REFMarker:NPEak

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:REFMarker:NPEak



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.ReferenceMarker.Npeak.NpeakCls
	:members:
	:undoc-members:
	:noindex: