AudioOutput
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:AOUT:ENABle
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:AOUT:LEVel

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:AOUT:ENABle
	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:AOUT:LEVel



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Gslope.AfSettings.AudioOutput.AudioOutputCls
	:members:
	:undoc-members:
	:noindex: