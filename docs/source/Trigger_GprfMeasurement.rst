GprfMeasurement
----------------------------------------





.. autoclass:: RsCma.Implementations.Trigger.GprfMeasurement.GprfMeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprfMeasurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_GprfMeasurement_FftSpecAn.rst
	Trigger_GprfMeasurement_IqRecorder.rst
	Trigger_GprfMeasurement_Power.rst
	Trigger_GprfMeasurement_Spectrum.rst