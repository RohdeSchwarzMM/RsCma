Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:RFCarrier:MAXimum
	single: READ:VSE:MEASurement<Instance>:RFCarrier:MAXimum
	single: CALCulate:VSE:MEASurement<Instance>:RFCarrier:MAXimum

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:RFCarrier:MAXimum
	READ:VSE:MEASurement<Instance>:RFCarrier:MAXimum
	CALCulate:VSE:MEASurement<Instance>:RFCarrier:MAXimum



.. autoclass:: RsCma.Implementations.Vse.Measurement.RfCarrier.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: