Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: READ:VSE:MEASurement<Instance>:CONS:FREQuency:CURRent
	single: FETCh:VSE:MEASurement<Instance>:CONS:FREQuency:CURRent

.. code-block:: python

	READ:VSE:MEASurement<Instance>:CONS:FREQuency:CURRent
	FETCh:VSE:MEASurement<Instance>:CONS:FREQuency:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.Cons.Frequency.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: