Squelch
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:SQUelch:STATe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:SQUelch:STATe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.Squelch.SquelchCls
	:members:
	:undoc-members:
	:noindex: