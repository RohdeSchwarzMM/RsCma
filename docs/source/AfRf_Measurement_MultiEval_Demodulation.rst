Demodulation
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.DemodulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.demodulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Demodulation_Fdeviation.rst
	AfRf_Measurement_MultiEval_Demodulation_FmStereo.rst
	AfRf_Measurement_MultiEval_Demodulation_Frequency.rst
	AfRf_Measurement_MultiEval_Demodulation_ModDepth.rst
	AfRf_Measurement_MultiEval_Demodulation_Pdeviation.rst