Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:LSBLevel:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:LSBLevel:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:LSBLevel:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:LSBLevel:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.Demodulation.LsbLevel.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: