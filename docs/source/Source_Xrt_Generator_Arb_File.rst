File
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:XRT:GENerator<Instance>:ARB:FILE:DATE
	single: SOURce:XRT:GENerator<Instance>:ARB:FILE:OPTion
	single: SOURce:XRT:GENerator<Instance>:ARB:FILE:VERSion
	single: SOURce:XRT:GENerator<Instance>:ARB:FILE

.. code-block:: python

	SOURce:XRT:GENerator<Instance>:ARB:FILE:DATE
	SOURce:XRT:GENerator<Instance>:ARB:FILE:OPTion
	SOURce:XRT:GENerator<Instance>:ARB:FILE:VERSion
	SOURce:XRT:GENerator<Instance>:ARB:FILE



.. autoclass:: RsCma.Implementations.Source.Xrt.Generator.Arb.File.FileCls
	:members:
	:undoc-members:
	:noindex: