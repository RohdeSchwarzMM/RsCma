Hexadecimal
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:DPMR:SYMBols:HEXadecimal
	single: READ:VSE:MEASurement<Instance>:DPMR:SYMBols:HEXadecimal

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:DPMR:SYMBols:HEXadecimal
	READ:VSE:MEASurement<Instance>:DPMR:SYMBols:HEXadecimal



.. autoclass:: RsCma.Implementations.Vse.Measurement.Dpmr.Symbols.Hexadecimal.HexadecimalCls
	:members:
	:undoc-members:
	:noindex: