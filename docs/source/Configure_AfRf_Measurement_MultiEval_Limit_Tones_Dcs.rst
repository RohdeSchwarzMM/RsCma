Dcs
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Tones.Dcs.DcsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.limit.tones.dcs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Limit_Tones_Dcs_FskDeviation.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Tones_Dcs_TocLength.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Tones_Dcs_TofDeviation.rst