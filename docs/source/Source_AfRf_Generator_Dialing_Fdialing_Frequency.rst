Frequency<FrequencyLobe>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.source.afRf.generator.dialing.fdialing.frequency.repcap_frequencyLobe_get()
	driver.source.afRf.generator.dialing.fdialing.frequency.repcap_frequencyLobe_set(repcap.FrequencyLobe.Nr1)



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:FREQuency:STONe

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:FREQuency:STONe



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.Fdialing.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.dialing.fdialing.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Dialing_Fdialing_Frequency_Dtone.rst