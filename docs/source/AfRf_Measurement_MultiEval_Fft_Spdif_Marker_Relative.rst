Relative
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SIN<nr>:MARKer<mnr>:RELative

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SIN<nr>:MARKer<mnr>:RELative



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Spdif.Marker.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: