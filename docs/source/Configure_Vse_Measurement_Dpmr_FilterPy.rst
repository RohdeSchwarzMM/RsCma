FilterPy
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:DPMR:FILTer

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:DPMR:FILTer



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Dpmr.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.dpmr.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Dpmr_FilterPy_Rrc.rst