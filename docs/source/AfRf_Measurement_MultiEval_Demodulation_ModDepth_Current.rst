Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.ModDepth.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: