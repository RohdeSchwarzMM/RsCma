Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:RMS:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:RMS:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.Level.Rms.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: