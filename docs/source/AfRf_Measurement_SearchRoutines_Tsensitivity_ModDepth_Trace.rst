Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:MDEPth:TRACe
	single: READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:MDEPth:TRACe

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:MDEPth:TRACe
	READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:MDEPth:TRACe



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Tsensitivity.ModDepth.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: