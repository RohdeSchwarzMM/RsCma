Routines
----------------------------------------





.. autoclass:: RsCma.Implementations.Display.AfRf.Measurement.Routines.RoutinesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.afRf.measurement.routines.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_AfRf_Measurement_Routines_Application.rst