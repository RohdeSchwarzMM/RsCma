Application
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: DISPlay:AFRF:MEASurement<Instance>:AUDio:APPLication:SELect

.. code-block:: python

	DISPlay:AFRF:MEASurement<Instance>:AUDio:APPLication:SELect



.. autoclass:: RsCma.Implementations.Display.AfRf.Measurement.Audio.Application.ApplicationCls
	:members:
	:undoc-members:
	:noindex: