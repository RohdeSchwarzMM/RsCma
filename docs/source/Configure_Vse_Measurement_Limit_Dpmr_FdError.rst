FdError
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:DPMR:FDERor

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:DPMR:FDERor



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Dpmr.FdError.FdErrorCls
	:members:
	:undoc-members:
	:noindex: