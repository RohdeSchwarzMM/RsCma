Dtone
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:FREQuency<Nr>:DTONe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:FREQuency<Nr>:DTONe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Fdialing.Frequency.Dtone.DtoneCls
	:members:
	:undoc-members:
	:noindex: