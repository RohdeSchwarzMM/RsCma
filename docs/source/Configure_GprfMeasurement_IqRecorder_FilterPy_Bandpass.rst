Bandpass
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FILTer:BANDpass:BWIDth

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FILTer:BANDpass:BWIDth



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.IqRecorder.FilterPy.Bandpass.BandpassCls
	:members:
	:undoc-members:
	:noindex: