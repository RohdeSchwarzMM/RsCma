Second
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AOUT:SECond:LEVel

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AOUT:SECond:LEVel



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioOutput.Second.SecondCls
	:members:
	:undoc-members:
	:noindex: