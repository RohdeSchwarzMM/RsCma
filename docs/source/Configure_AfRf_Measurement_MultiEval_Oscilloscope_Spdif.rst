Spdif
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:XDIVision
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:MTIMe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:XDIVision
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:MTIMe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Oscilloscope.Spdif.SpdifCls
	:members:
	:undoc-members:
	:noindex: