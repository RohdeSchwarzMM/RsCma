Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodRight.AfSignal.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: