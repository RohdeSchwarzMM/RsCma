Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:REFerence:FREQuency:SOURce

.. code-block:: python

	SYSTem:BASE:REFerence:FREQuency:SOURce



.. autoclass:: RsCma.Implementations.System.Base.Reference.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: