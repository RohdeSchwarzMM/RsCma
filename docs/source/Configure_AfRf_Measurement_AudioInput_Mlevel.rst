Mlevel
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:MLEVel

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:MLEVel



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Mlevel.MlevelCls
	:members:
	:undoc-members:
	:noindex: