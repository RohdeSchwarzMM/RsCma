AfSignal
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.AfSignal.AfSignalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.audioInput.afSignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_AudioInput_AfSignal_Average.rst
	AfRf_Measurement_MultiEval_AudioInput_AfSignal_Current.rst
	AfRf_Measurement_MultiEval_AudioInput_AfSignal_Deviation.rst
	AfRf_Measurement_MultiEval_AudioInput_AfSignal_Maximum.rst