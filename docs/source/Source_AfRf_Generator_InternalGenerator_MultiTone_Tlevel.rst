Tlevel
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:MTONe:TLEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:MTONe:TLEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.MultiTone.Tlevel.TlevelCls
	:members:
	:undoc-members:
	:noindex: