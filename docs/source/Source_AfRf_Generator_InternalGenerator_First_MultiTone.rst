MultiTone
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator:FIRSt:MTONe:TLEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator:FIRSt:MTONe:TLEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.First.MultiTone.MultiToneCls
	:members:
	:undoc-members:
	:noindex: