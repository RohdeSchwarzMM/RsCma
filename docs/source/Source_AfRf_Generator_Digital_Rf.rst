Rf
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIGital:RF:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIGital:RF:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Digital.Rf.RfCls
	:members:
	:undoc-members:
	:noindex: