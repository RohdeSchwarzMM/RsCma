Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodLeft.AfSignal.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: