Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifLeft.AfSignal.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: