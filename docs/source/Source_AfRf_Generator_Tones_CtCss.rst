CtCss
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:TONes:CTCSs:ENABle
	single: SOURce:AFRF:GENerator<Instance>:TONes:CTCSs:TNUMber

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:TONes:CTCSs:ENABle
	SOURce:AFRF:GENerator<Instance>:TONes:CTCSs:TNUMber



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Tones.CtCss.CtCssCls
	:members:
	:undoc-members:
	:noindex: