Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:DMR:BERate:CURRent
	single: CALCulate:AFRF:MEASurement<Instance>:DIGital:DMR:BERate:CURRent
	single: READ:AFRF:MEASurement<Instance>:DIGital:DMR:BERate:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:DMR:BERate:CURRent
	CALCulate:AFRF:MEASurement<Instance>:DIGital:DMR:BERate:CURRent
	READ:AFRF:MEASurement<Instance>:DIGital:DMR:BERate:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Dmr.BitErrorRate.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: