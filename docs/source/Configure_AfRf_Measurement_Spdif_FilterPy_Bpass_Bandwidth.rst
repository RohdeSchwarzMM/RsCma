Bandwidth
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:BPASs:BWIDth

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:BPASs:BWIDth



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.FilterPy.Bpass.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: