SignalQuality
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.SignalQualityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.signalQuality.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_SignalQuality_AudioInput.rst
	AfRf_Measurement_MultiEval_SignalQuality_DemodLeft.rst
	AfRf_Measurement_MultiEval_SignalQuality_DemodRight.rst
	AfRf_Measurement_MultiEval_SignalQuality_SpdifLeft.rst
	AfRf_Measurement_MultiEval_SignalQuality_SpdifRight.rst
	AfRf_Measurement_MultiEval_SignalQuality_Voip.rst