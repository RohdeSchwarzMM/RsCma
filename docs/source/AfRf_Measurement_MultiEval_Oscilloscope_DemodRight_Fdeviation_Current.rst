Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEMRight:FDEViation:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEMRight:FDEViation:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEMRight:FDEViation:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEMRight:FDEViation:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.DemodRight.Fdeviation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: