All
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:STATe:ALL

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:STATe:ALL



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: