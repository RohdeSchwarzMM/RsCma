Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:PTFive:MFIDelity:MAXimum
	single: READ:VSE:MEASurement<Instance>:PTFive:MFIDelity:MAXimum
	single: CALCulate:VSE:MEASurement<Instance>:PTFive:MFIDelity:MAXimum

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:PTFive:MFIDelity:MAXimum
	READ:VSE:MEASurement<Instance>:PTFive:MFIDelity:MAXimum
	CALCulate:VSE:MEASurement<Instance>:PTFive:MFIDelity:MAXimum



.. autoclass:: RsCma.Implementations.Vse.Measurement.PtFive.Mfidelity.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: