AudioInput<AudioInput>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.afRf.measurement.multiEval.limit.audioInput.repcap_audioInput_get()
	driver.configure.afRf.measurement.multiEval.limit.audioInput.repcap_audioInput_set(repcap.AudioInput.Nr1)





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.AudioInput.AudioInputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.limit.audioInput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Limit_AudioInput_Sinad.rst
	Configure_AfRf_Measurement_MultiEval_Limit_AudioInput_SndRatio.rst
	Configure_AfRf_Measurement_MultiEval_Limit_AudioInput_SnnRatio.rst
	Configure_AfRf_Measurement_MultiEval_Limit_AudioInput_SnRatio.rst
	Configure_AfRf_Measurement_MultiEval_Limit_AudioInput_ThDistortion.rst
	Configure_AfRf_Measurement_MultiEval_Limit_AudioInput_ThdNoise.rst