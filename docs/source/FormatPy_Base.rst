Base
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FORMat:BASE:BORDer
	single: FORMat:BASE:DINTerchange
	single: FORMat:BASE:SREGister

.. code-block:: python

	FORMat:BASE:BORDer
	FORMat:BASE:DINTerchange
	FORMat:BASE:SREGister



.. autoclass:: RsCma.Implementations.FormatPy.Base.BaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.formatPy.base.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	FormatPy_Base_Data.rst