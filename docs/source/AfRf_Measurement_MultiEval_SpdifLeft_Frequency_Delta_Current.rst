Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:FREQuency:DELTa:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:FREQuency:DELTa:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:FREQuency:DELTa:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:FREQuency:DELTa:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifLeft.Frequency.Delta.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: