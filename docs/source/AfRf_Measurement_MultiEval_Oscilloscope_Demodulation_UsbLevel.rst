UsbLevel
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.Demodulation.UsbLevel.UsbLevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.oscilloscope.demodulation.usbLevel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Oscilloscope_Demodulation_UsbLevel_Current.rst