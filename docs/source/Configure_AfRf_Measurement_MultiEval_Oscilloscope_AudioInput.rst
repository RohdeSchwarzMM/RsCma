AudioInput
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:XDIVision
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:MTIMe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:XDIVision
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:MTIMe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Oscilloscope.AudioInput.AudioInputCls
	:members:
	:undoc-members:
	:noindex: