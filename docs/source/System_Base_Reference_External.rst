External
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:REFerence:EXTernal:LIRange

.. code-block:: python

	SYSTem:BASE:REFerence:EXTernal:LIRange



.. autoclass:: RsCma.Implementations.System.Base.Reference.External.ExternalCls
	:members:
	:undoc-members:
	:noindex: