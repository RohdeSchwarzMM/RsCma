Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.FreqError.Delta.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: