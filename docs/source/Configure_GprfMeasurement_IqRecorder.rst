IqRecorder
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:TOFFset
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:SAMPles
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:TOUT
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:RATio
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FORMat
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:MUNit
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:IQFile
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:WTFile

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:TOFFset
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:SAMPles
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:TOUT
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:RATio
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FORMat
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:MUNit
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:IQFile
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:WTFile



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.IqRecorder.IqRecorderCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.iqRecorder.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_IqRecorder_Capture.rst
	Configure_GprfMeasurement_IqRecorder_FilterPy.rst