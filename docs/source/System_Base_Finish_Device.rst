Device
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:FINish:DEVice

.. code-block:: python

	SYSTem:BASE:FINish:DEVice



.. autoclass:: RsCma.Implementations.System.Base.Finish.Device.DeviceCls
	:members:
	:undoc-members:
	:noindex: