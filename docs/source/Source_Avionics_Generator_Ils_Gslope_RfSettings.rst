RfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:RFSettings:FREQuency
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:RFSettings:LEVel

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:RFSettings:FREQuency
	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:RFSettings:LEVel



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Gslope.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.ils.gslope.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_Ils_Gslope_RfSettings_Channel.rst
	Source_Avionics_Generator_Ils_Gslope_RfSettings_RfOut.rst