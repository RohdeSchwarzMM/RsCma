Dwidth
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:DWIDth

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:DWIDth



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.FilterPy.Dwidth.DwidthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.demodulation.filterPy.dwidth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Demodulation_FilterPy_Dwidth_Sfactor.rst