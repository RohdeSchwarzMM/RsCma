Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: READ:VSE:MEASurement<Instance>:LTE:EVM:CURRent
	single: FETCh:VSE:MEASurement<Instance>:LTE:EVM:CURRent

.. code-block:: python

	READ:VSE:MEASurement<Instance>:LTE:EVM:CURRent
	FETCh:VSE:MEASurement<Instance>:LTE:EVM:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.Lte.Evm.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: