Fmoddepth
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:FMODdepth<nr>

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:FMODdepth<nr>



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Gslope.AfSettings.Fmoddepth.FmoddepthCls
	:members:
	:undoc-members:
	:noindex: