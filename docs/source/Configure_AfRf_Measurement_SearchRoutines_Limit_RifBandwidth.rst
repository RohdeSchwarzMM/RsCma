RifBandwidth
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Limit.RifBandwidth.RifBandwidthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.searchRoutines.limit.rifBandwidth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_SearchRoutines_Limit_RifBandwidth_BwDisplace.rst
	Configure_AfRf_Measurement_SearchRoutines_Limit_RifBandwidth_Foffset.rst