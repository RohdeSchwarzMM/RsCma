Obw
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:OBW:PERCentage

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:ACP:OBW:PERCentage



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Acp.Obw.ObwCls
	:members:
	:undoc-members:
	:noindex: