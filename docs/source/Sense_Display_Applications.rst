Applications
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SENSe:DISPlay:APPLications:CATalog

.. code-block:: python

	SENSe:DISPlay:APPLications:CATalog



.. autoclass:: RsCma.Implementations.Sense.Display.Applications.ApplicationsCls
	:members:
	:undoc-members:
	:noindex: