Demodulation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.DemodulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.demodulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Demodulation_Enable.rst
	Configure_AfRf_Measurement_Demodulation_Fdeviation.rst
	Configure_AfRf_Measurement_Demodulation_FilterPy.rst
	Configure_AfRf_Measurement_Demodulation_FmStereo.rst
	Configure_AfRf_Measurement_Demodulation_Frequency.rst
	Configure_AfRf_Measurement_Demodulation_Gcoupling.rst
	Configure_AfRf_Measurement_Demodulation_ModDepth.rst
	Configure_AfRf_Measurement_Demodulation_Tmode.rst