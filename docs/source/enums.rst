Enums
=========

AcpOffset
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AcpOffset.LSB
	# All values (3x):
	LSB | NONE | USB

Activity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Activity.ACTive
	# All values (2x):
	ACTive | INACtive

Allow
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Allow.ALLowed
	# All values (2x):
	ALLowed | NA

AnalogDigital
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AnalogDigital.ANALog
	# All values (2x):
	ANALog | DIGital

ArbSamplesRange
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbSamplesRange.FULL
	# All values (2x):
	FULL | SUB

ArmedState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArmedState.ARMed
	# All values (3x):
	ARMed | OFF | TRIGgered

AttenuationPort
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AttenuationPort.LOAD
	# All values (2x):
	LOAD | SOURce

AudioConnector
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AudioConnector.AF1O
	# All values (2x):
	AF1O | AF2O

AudioSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AudioSource.DEM
	# All values (6x):
	DEM | DEML | DEMR | NONE | UGEN | VOIP

AveragingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AveragingMode.LINear
	# All values (2x):
	LINear | LOGarithmic

BandpassFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BandpassFilter.F01M
	# All values (5x):
	F01M | F05M | F25K | F50K | F8330

Bandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Bandwidth.FR1K
	# All values (5x):
	FR1K | FR1K1 | FR1K2 | FR1K3 | FR1K4

BaseScenario
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BaseScenario.AUDio
	# Last value:
	value = enums.BaseScenario.TXTest
	# All values (12x):
	AUDio | AVIonics | DEXPert | DRXTest | DSPectrum | DTXTest | DXTest | EXPert
	RXTest | SEQuencer | SPECtrum | TXTest

BatteryUsage
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BatteryUsage.NAV
	# All values (3x):
	NAV | REMovable | USED

BerPeriod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BerPeriod.F36
	# All values (2x):
	F36 | F48

ByteOrder
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ByteOrder.NORMal
	# All values (2x):
	NORMal | SWAPped

CalibType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalibType.CALibration
	# All values (4x):
	CALibration | FSCorrection | OGCal | UCORrection

CatalogFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CatalogFormat.ALL
	# All values (2x):
	ALL | WTIMe

ChannelModeDmr
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelModeDmr.DATA
	# All values (2x):
	DATA | VOICe

ChannelModeTetra
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelModeTetra.TCH72
	# All values (1x):
	TCH72

ChannelTypeTetra
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelTypeTetra.CT0
	# All values (8x):
	CT0 | CT1 | CT2 | CT21 | CT22 | CT24 | CT3 | CT4

CircuitryState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CircuitryState.ACTive
	# All values (2x):
	ACTive | PASSive

ClockRate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ClockRate.BPS115200
	# Last value:
	value = enums.ClockRate.BPS9600
	# All values (10x):
	BPS115200 | BPS1200 | BPS14400 | BPS19200 | BPS2400 | BPS28800 | BPS38400 | BPS4800
	BPS57600 | BPS9600

CrestFactor
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CrestFactor.LOW
	# All values (2x):
	LOW | MAXimum

DataFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DataFormat.ASCii
	# All values (8x):
	ASCii | BINary | HEXadecimal | INTeger | OCTal | PACKed | REAL | UINTeger

DefaultUnitAngle
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DefaultUnitAngle.DEG
	# All values (3x):
	DEG | GRAD | RAD

DefaultUnitCapacity
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DefaultUnitCapacity.AF
	# Last value:
	value = enums.DefaultUnitCapacity.UF
	# All values (13x):
	AF | EXF | F | FF | GF | KF | MF | MIF
	NF | PEF | PF | TF | UF

DefaultUnitCharge
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DefaultUnitCharge.AC
	# Last value:
	value = enums.DefaultUnitCharge.UC
	# All values (13x):
	AC | C | EXC | FC | GC | KC | MC | MIC
	NC | PC | PEC | TC | UC

DefaultUnitConductance
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DefaultUnitConductance.ASIE
	# Last value:
	value = enums.DefaultUnitConductance.USIE
	# All values (13x):
	ASIE | EXSie | FSIE | GSIE | KSIE | MISie | MSIE | NSIE
	PESie | PSIE | SIE | TSIE | USIE

DefaultUnitCurrent
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DefaultUnitCurrent.A
	# Last value:
	value = enums.DefaultUnitCurrent.UA
	# All values (18x):
	A | AA | DBA | DBMA | DBNA | DBPA | DBUA | EXA
	FA | GA | KA | MA | MAA | NA | PA | PEA
	TA | UA

DefaultUnitEnergy
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DefaultUnitEnergy.AJ
	# Last value:
	value = enums.DefaultUnitEnergy.UJ
	# All values (13x):
	AJ | EXJ | FJ | GJ | J | KJ | MIJ | MJ
	NJ | PEJ | PJ | TJ | UJ

DefaultUnitFrequency
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DefaultUnitFrequency.AHZ
	# Last value:
	value = enums.DefaultUnitFrequency.UHZ
	# All values (13x):
	AHZ | EXHZ | FHZ | GHZ | HZ | KHZ | MHZ | MIHZ
	NHZ | PEHZ | PHZ | THZ | UHZ

DefaultUnitFullScale
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DefaultUnitFullScale.DBFS
	# All values (4x):
	DBFS | FS | PCT | PPM

DefaultUnitLenght
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DefaultUnitLenght.AM
	# Last value:
	value = enums.DefaultUnitLenght.UM
	# All values (13x):
	AM | EXM | FM | GM | KM | M | MAM | MM
	NM | PEM | PM | TM | UM

DefaultUnitPower
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DefaultUnitPower.AW
	# Last value:
	value = enums.DefaultUnitPower.W
	# All values (19x):
	AW | DBM | DBMW | DBNW | DBPW | DBUW | DBW | EXW
	FW | GW | KW | MIW | MW | NW | PEW | PW
	TW | UW | W

DefaultUnitResistor
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DefaultUnitResistor.AOHM
	# Last value:
	value = enums.DefaultUnitResistor.UOHM
	# All values (13x):
	AOHM | EXOHm | FOHM | GOHM | KOHM | MIOHm | MOHM | NOHM
	OHM | PEOHm | POHM | TOHM | UOHM

DefaultUnitTemperature
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DefaultUnitTemperature.C
	# All values (6x):
	C | CEL | F | FAR | K | KEL

DefaultUnitTime
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DefaultUnitTime.AS
	# Last value:
	value = enums.DefaultUnitTime.US
	# All values (18x):
	AS | EXS | FS | GS | H | HOUR | KS | M
	MAS | MIN | MS | NS | PES | PS | S | SEC
	TS | US

DefaultUnitVoltage
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DefaultUnitVoltage.AV
	# Last value:
	value = enums.DefaultUnitVoltage.V
	# All values (18x):
	AV | DBMV | DBNV | DBPV | DBUV | DBV | EXV | FV
	GV | KV | MAV | MV | NV | PEV | PV | TV
	UV | V

DeltaMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DeltaMode.MEAS
	# All values (3x):
	MEAS | NONE | USER

Demodulation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Demodulation.AM
	# All values (6x):
	AM | FM | FMSTereo | LSB | PM | USB

DemodulationType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DemodulationType.FSK4
	# All values (1x):
	FSK4

Detector
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Detector.AUTopeak
	# All values (6x):
	AUTopeak | AVERage | MAXPeak | MINPeak | RMS | SAMPle

DetectorSimple
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DetectorSimple.PEAK
	# All values (2x):
	PEAK | RMS

DialingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DialingMode.DTMF
	# All values (4x):
	DTMF | FDIaling | SCAL | SELCall

DigitalSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DigitalSource.ARB
	# All values (8x):
	ARB | DMR | DPMR | NXDN | P25 | POCSag | UDEFined | ZIGBee

DigitalToneMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DigitalToneMode.DCS
	# All values (6x):
	DCS | DTMF | FDIA | NONE | SCAL | SELCall

DigitTimeMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DigitTimeMode.EQUal
	# All values (2x):
	EQUal | INDividual

DirectionIo
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DirectionIo.IN
	# All values (2x):
	IN | OUT

DirPwrSensorFwdValue
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DirPwrSensorFwdValue.CCDF
	# All values (4x):
	CCDF | CFAC | FPWR | PEP

DirPwrSensorRevValue
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DirPwrSensorRevValue.REFL
	# All values (4x):
	REFL | RLOS | RPWR | SWR

DisplayLanguage
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DisplayLanguage.AR
	# Last value:
	value = enums.DisplayLanguage.ZH
	# All values (14x):
	AR | CS | DA | DE | EN | ES | FR | IT
	JA | KO | RU | SV | TR | ZH

DmrPattern
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DmrPattern.BPRB15
	# Last value:
	value = enums.DmrPattern.SILence
	# All values (16x):
	BPRB15 | BPRB9 | C153 | O153 | P1031 | PRBS9 | R10A | RA0
	RA1 | RFBS | RFMS | RLD | RPRB15 | RPRB9 | RSYR | SILence

DmrPatternB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DmrPatternB.P1031
	# All values (3x):
	P1031 | SILence | SYNC

Dstrategy
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dstrategy.BYLayout
	# All values (2x):
	BYLayout | OFF

Eformat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Eformat.ASCii
	# All values (2x):
	ASCii | XML

EstartMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EstartMode.AUTO
	# All values (2x):
	AUTO | EXPLicit

EstopMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EstopMode.AUTO
	# All values (4x):
	AUTO | BUFFerfull | ERRor | EXPLicit

ExpFrequency
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ExpFrequency.CONF
	# All values (2x):
	CONF | FGEN

ExpressionMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ExpressionMode.REGex
	# All values (2x):
	REGex | STRing

ExtPwrSensorApp
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ExtPwrSensorApp.EPS
	# All values (2x):
	EPS | NRTZ

ExtSensorResolution
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ExtSensorResolution.PD0
	# All values (4x):
	PD0 | PD1 | PD2 | PD3

FftLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FftLength.F16K
	# All values (3x):
	F16K | F4K | F8K

FftOffsetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FftOffsetMode.FIXed
	# All values (2x):
	FIXed | VARiable

FftSpan
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FftSpan.SP1
	# All values (4x):
	SP1 | SP10 | SP21 | SP5

FftWindowType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FftWindowType.BLHA
	# All values (5x):
	BLHA | FLTP | HAMMing | HANN | RECTangle

FileSave
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FileSave.OFF
	# All values (3x):
	OFF | ON | ONLY

FilterDigital
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterDigital.COSine
	# All values (4x):
	COSine | GAUSs | RRC | SINC

FilterNxDn
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterNxDn.NXTX
	# All values (1x):
	NXTX

FilterType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterType.BANDpass
	# All values (5x):
	BANDpass | CDMA | GAUSs | TDSCdma | WCDMa

FreqCounterMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FreqCounterMode.HW
	# All values (2x):
	HW | SW

FskMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FskMode.FSK2
	# All values (2x):
	FSK2 | FSK4

GeneratorCoupling
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeneratorCoupling.GEN1
	# All values (5x):
	GEN1 | GEN2 | GEN3 | GEN4 | OFF

GeneratorCouplingVoIp
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeneratorCouplingVoIp.GEN3
	# All values (3x):
	GEN3 | GEN4 | OFF

GeneratorFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeneratorFilter.COS
	# All values (5x):
	COS | GAUSs | RC | RRC | SINC

GeneratorState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeneratorState.ADJusted
	# All values (7x):
	ADJusted | AUTonomous | COUPled | INValid | OFF | ON | PENDing

HighpassFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HighpassFilter.F300
	# All values (2x):
	F300 | OFF

HighpassFilterExtended
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HighpassFilterExtended.F300
	# All values (4x):
	F300 | F50 | F6 | OFF

IlsLetter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IlsLetter.X
	# All values (2x):
	X | Y

IlsTab
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IlsTab.GSLope
	# All values (2x):
	GSLope | LOCalizer

Impedance
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Impedance.IHOL
	# All values (5x):
	IHOL | R150 | R300 | R50 | R600

ImpulseLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ImpulseLength.T
	# All values (5x):
	T | T2 | T4 | T6 | T8

InputConnector
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InputConnector.RFCom
	# All values (2x):
	RFCom | RFIN

InterfererMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InterfererMode.AM
	# All values (5x):
	AM | CW | FM | NONE | PM

IqFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqFormat.IQ
	# All values (2x):
	IQ | RPHI

LeftRightDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LeftRightDirection.LEFT
	# All values (2x):
	LEFT | RIGHt

LevelEditMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LevelEditMode.INDividual
	# All values (2x):
	INDividual | TOTal

LinkDirectionDmr
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LinkDirectionDmr.MSSourced
	# All values (1x):
	MSSourced

LinkDirectionTetra
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LinkDirectionTetra.DLNK
	# All values (2x):
	DLNK | ULNK

LockRangeExternal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LockRangeExternal.INV
	# All values (4x):
	INV | MEDium | NARRow | WIDE

LockRangeInternal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LockRangeInternal.INV
	# All values (3x):
	INV | MEDium | NARRow

LowHigh
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowHigh.HIGH
	# All values (2x):
	HIGH | LOW

LowpassFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowpassFilter.F15K
	# All values (4x):
	F15K | F3K | F4K | OFF

LowpassFilterExtended
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowpassFilterExtended.F15K
	# All values (6x):
	F15K | F255 | F3K | F3K4 | F4K | OFF

LteChannelBandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LteChannelBandwidth.F10M
	# All values (4x):
	F10M | F20M | F3M | F5M

MagnitudeUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MagnitudeUnit.RAW
	# All values (2x):
	RAW | VOLT

MarkerFunction
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MarkerFunction.MAX
	# All values (6x):
	MAX | MAXL | MAXN | MAXR | MAXV | MIN

MarkerPlacement
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MarkerPlacement.ABSolute
	# All values (2x):
	ABSolute | RELative

MeasAccuracy
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasAccuracy.HIGH
	# All values (2x):
	HIGH | NORMal

MeasState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MeasState.ACTive
	# Last value:
	value = enums.MeasState.RUN
	# All values (10x):
	ACTive | ADJusted | ALIVe | FROZen | INValid | OFF | PENDing | QUEued
	RDY | RUN

ModeTetra
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModeTetra.DQPS45
	# All values (1x):
	DQPS45

ModulationScheme
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModulationScheme.AM
	# All values (8x):
	AM | ARB | CW | FM | FMSTereo | LSB | PM | USB

NotchPath
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NotchPath.AF
	# All values (3x):
	AF | SPDif | VOIP

NrtDevice
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NrtDevice.N14
	# All values (3x):
	N14 | N43 | N44

NxdnPattern
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NxdnPattern.P1011
	# Last value:
	value = enums.NxdnPattern.SILence
	# All values (14x):
	P1011 | P1031 | PRBS15 | PRBS9 | R10A | RA0 | RA1 | RAW1
	RAW2 | RLD | RPRB15 | RPRB9 | RSYR | SILence

NxdnPatternB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NxdnPatternB.CUST
	# Last value:
	value = enums.NxdnPatternB.STD1
	# All values (13x):
	CUST | P1031 | PRBS9 | R1031 | R10A | RA0 | RA1 | RLD
	RPRB15 | RPRB9 | RSYR | SILence | STD1

OperationMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OperationMode.LOCal
	# All values (2x):
	LOCal | REMote

OptionsProductType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OptionsProductType.ALL
	# All values (5x):
	ALL | FWA | HWOPtion | SWOPtion | SWPackage

OptionsScope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OptionsScope.INSTrument
	# All values (2x):
	INSTrument | SYSTem

OptionValidity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OptionValidity.ALL
	# All values (4x):
	ALL | CLICense | FUNCtional | VALid

OscillatorType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OscillatorType.OCXO
	# All values (2x):
	OCXO | TCXO

OutputConnector
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OutputConnector.RFCom
	# All values (2x):
	RFCom | RFOut

OverviewType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OverviewType.FFT
	# All values (3x):
	FFT | NONE | OSCilloscope

P25Mode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.P25Mode.C4FM
	# All values (2x):
	C4FM | CQPSk

P25Pattern
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.P25Pattern.BUSY
	# Last value:
	value = enums.P25Pattern.SILence
	# All values (15x):
	BUSY | C4FM | CALibration | IDLE | INTerference | P1011 | R10A | RA0
	RA1 | RAW1 | RLD | RPRB15 | RPRB9 | RSYR | SILence

PagerType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PagerType.ALPHanumeric
	# All values (3x):
	ALPHanumeric | NUMeric | TONLy

PathCoupling
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PathCoupling.AC
	# All values (2x):
	AC | DC

PatternTetra
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PatternTetra.S1
	# All values (3x):
	S1 | S2 | S3

PayloadType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PayloadType.P1011
	# All values (2x):
	P1011 | SILence

PayloadTypeTetra
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PayloadTypeTetra.ALLO
	# All values (5x):
	ALLO | ALLZero | ALTE | PRBS9 | USER

PowerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerMode.ALL
	# All values (4x):
	ALL | ONCE | PRESelect | SWEep

PowerSignalDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerSignalDirection.AUTO
	# All values (3x):
	AUTO | FWD | REV

PreDeEmphasis
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PreDeEmphasis.OFF
	# All values (4x):
	OFF | T50 | T75 | T750

ProtocolMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ProtocolMode.AGILent
	# All values (3x):
	AGILent | IEEE1174 | RAW

PtFiveFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PtFiveFilter.C4FM
	# All values (2x):
	C4FM | RC

PulseShapingFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PulseShapingFilter.RRC
	# All values (1x):
	RRC

PulseShapingUserFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PulseShapingUserFilter.COS
	# All values (5x):
	COS | GAUSs | NXRX | RRC | SINC

PwrFilterType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PwrFilterType.NARRow
	# All values (3x):
	NARRow | UDEF | WIDE

RbwFilterType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RbwFilterType.BANDpass
	# All values (2x):
	BANDpass | GAUSs

RefFreqSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RefFreqSource.EXTernal
	# All values (3x):
	EXTernal | INTernal | INV

Relative
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Relative.CONStant
	# All values (2x):
	CONStant | RELative

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

RepeatMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RepeatMode.CONTinuous
	# All values (2x):
	CONTinuous | SINGle

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultStatus
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus.DC
	# Last value:
	value = enums.ResultStatus.ULEU
	# All values (11x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | ON
	UFL | ULEL | ULEU

ScreenshotFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ScreenshotFormat.BMP
	# All values (3x):
	BMP | JPG | PNG

SearchExtent
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SearchExtent.FULL
	# All values (3x):
	FULL | OFFLevel | ONLevel

SearchRoutine
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SearchRoutine.ADELay
	# All values (6x):
	ADELay | RIFBandwidth | RSENsitivity | RSQuelch | SSNR | TSENsitivity

SearchRoutinePath
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SearchRoutinePath.AFI1
	# All values (3x):
	AFI1 | AFI2 | VOIP

SelCallStandard
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelCallStandard.CCIR
	# All values (8x):
	CCIR | DZVei | EEA | EIA | PZVei | ZVEI1 | ZVEI2 | ZVEI3

SelCalStandard
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelCalStandard.SCAL16
	# All values (3x):
	SCAL16 | SCAL32 | UDEFind

SharingModeTetra
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SharingModeTetra.CARRier
	# All values (4x):
	CARRier | CONTinuous | MMCH | TRAFfic

SignalSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlope.FEDGe
	# All values (2x):
	FEDGe | REDGe

SignalSlopeExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlopeExt.FALLing
	# All values (4x):
	FALLing | FEDGe | REDGe | RISing

SignalSource
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SignalSource.AFI1
	# Last value:
	value = enums.SignalSource.SPIR
	# All values (14x):
	AFI1 | AFI2 | AFIB | FCHL | FCHR | FILE | GEN1 | GEN2
	GEN3 | GEN4 | GENB | SPIL | SPIN | SPIR

SingDualTonesType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SingDualTonesType.DTONes
	# All values (2x):
	DTONes | STONes

SingDualToneType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SingDualToneType.DTONe
	# All values (2x):
	DTONe | STONe

SipState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SipState.ERRor
	# All values (3x):
	ERRor | ESTablished | TERMinated

SlowDecay
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SlowDecay.OFF
	# All values (5x):
	OFF | X10 | X2 | X3 | X4

SoundSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SoundSource.AFONe
	# All values (7x):
	AFONe | AVIO | DEModulator | GENone | GENThree | LAN | SPDif

SpanMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SpanMode.FSWeep
	# All values (2x):
	FSWeep | ZSPan

SpecAnApp
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SpecAnApp.FREQ
	# All values (2x):
	FREQ | ZERO

Standard
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Standard.CUSTom
	# All values (8x):
	CUSTom | DMR | DPMR | LTE | NXDN | P25 | SPECtrum | TETRa

StandardB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StandardB.CUSTom
	# All values (7x):
	CUSTom | DMR | DPMR | LTE | NXDN | P25 | TETRa

StandardDigital
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StandardDigital.DMR
	# All values (3x):
	DMR | PTFive | TETRa

Statistic
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Statistic.AVERage
	# All values (4x):
	AVERage | CURRent | MAXimum | MINimum

StatRegFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StatRegFormat.ASCii
	# All values (4x):
	ASCii | BINary | HEXadecimal | OCTal

Status
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Status.FAILed
	# All values (2x):
	FAILed | PASSed

StopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopCondition.NONE
	# All values (2x):
	NONE | SLFail

SubTab
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SubTab.AFResults
	# Last value:
	value = enums.SubTab.TRIM
	# All values (9x):
	AFResults | FFT | FMSTereo | MULTitone | OSC | OVERview | RFResults | TONes
	TRIM

SubTabAudioMeas
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SubTabAudioMeas.AFResults
	# All values (5x):
	AFResults | FFT | OSC | OVERview | TRIM

SubTabDigitalMeas
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SubTabDigitalMeas.BER
	# All values (3x):
	BER | OVERview | SINFo

SubTabRoutines
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SubTabRoutines.CHARt
	# All values (2x):
	CHARt | TABLe

SubTabVseMeas
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SubTabVseMeas.CONStellation
	# Last value:
	value = enums.SubTabVseMeas.SYMResults
	# All values (11x):
	CONStellation | DEMod | EYEDiagram | LTE | NXDNresults | OVERview | PVTResults | RFResults
	SPECtrum | SYMDistr | SYMResults

SupplyMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SupplyMode.BATTery
	# All values (2x):
	BATTery | MAINs

TabSplit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TabSplit.SPLit
	# All values (2x):
	SPLit | TAB

TargetParameter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetParameter.NPEK
	# All values (5x):
	NPEK | PNPA | PPEK | RMS | RMSQ

TargetParType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetParType.SINad
	# All values (4x):
	SINad | SNDNratio | SNNRatio | SNRatio

TestModeTetra
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestModeTetra.SIDecoding
	# All values (3x):
	SIDecoding | T1 | VSE

TestPlanState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TestPlanState.EDITmode
	# Last value:
	value = enums.TestPlanState.SERRor
	# All values (10x):
	EDITmode | FINished | IDLE | LOADing | NOAVailable | NOLoaded | OPTMissing | PAUSed
	RUNNing | SERRor

TimeoutMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeoutMode.AUTO
	# All values (2x):
	AUTO | MANU

ToneMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ToneMode.NOISe
	# All values (4x):
	NOISe | NONE | SQUare | STONe

ToneTypeA
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ToneTypeA.DTMF
	# Last value:
	value = enums.ToneTypeA.STONe
	# All values (9x):
	DTMF | DTONe | FDIaling | MTONe | NOISe | SCAL | SELCall | SQUare
	STONe

ToneTypeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ToneTypeB.CTCSs
	# All values (4x):
	CTCSs | DCS | NONE | SUBTone

TraceB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceB.AVERage
	# All values (5x):
	AVERage | CURRent | MAXimum | MINimum | TDOMmain

TraceC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceC.AVERage
	# All values (3x):
	AVERage | CURRent | MAXimum

Transmission
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Transmission.EFR9600
	# All values (3x):
	EFR9600 | EHR4800 | EHR9600

TriggerCouplingAin
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerCouplingAin.DEMod
	# All values (4x):
	DEMod | NONE | SIN | VOIP

TriggerCouplingDemod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerCouplingDemod.AIN
	# All values (4x):
	AIN | NONE | SIN | VOIP

TriggerCouplingDigital
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerCouplingDigital.AIN
	# All values (3x):
	AIN | DEMod | NONE

TriggerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerMode.AUTO
	# All values (4x):
	AUTO | FRUN | NORMal | SINGle

TriggerSourceAf
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSourceAf.AF1
	# All values (2x):
	AF1 | AF2

TriggerSourceDemod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSourceDemod.DEMod
	# All values (3x):
	DEMod | LEFT | RIGHt

TtlInterface
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TtlInterface.WIRE1
	# All values (2x):
	WIRE1 | WIRE2

TxAfSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TxAfSource.AF1O
	# All values (3x):
	AF1O | AF2O | VOIP

UpDownDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UpDownDirection.DOWN
	# All values (2x):
	DOWN | UP

UserDefPattern
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UserDefPattern.PRBS6
	# All values (2x):
	PRBS6 | PRBS9

UserRole
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UserRole.ADMin
	# All values (5x):
	ADMin | DEVeloper | SERVice | UEXTended | USER

VoIpCodec
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VoIpCodec.ALAW
	# All values (2x):
	ALAW | ULAW

VoIpSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VoIpSource.AFI1
	# All values (4x):
	AFI1 | AFI2 | GEN3 | GEN4

VphaseDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VphaseDirection.FROM
	# All values (2x):
	FROM | TO

WeightingFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.WeightingFilter.AWEighting
	# All values (4x):
	AWEighting | CCITt | CMESsage | OFF

Xdivision
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Xdivision.M1
	# Last value:
	value = enums.Xdivision.U500
	# All values (19x):
	M1 | M10 | M100 | M2 | M20 | M200 | M5 | M50
	M500 | S1 | U1 | U10 | U100 | U2 | U20 | U200
	U5 | U50 | U500

XrtInputConnector
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.XrtInputConnector.RF1
	# All values (8x):
	RF1 | RF2 | RF3 | RF4 | RF5 | RF6 | RF7 | RF8

YesNoStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.YesNoStatus.NO
	# All values (2x):
	NO | YES

ZigBeeMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ZigBeeMode.OQPSk
	# All values (1x):
	OQPSk

