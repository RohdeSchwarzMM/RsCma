Tsensitivity
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:RSQuelch:TSENsitivity

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:RSQuelch:TSENsitivity



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Limit.Rsquelch.Tsensitivity.TsensitivityCls
	:members:
	:undoc-members:
	:noindex: