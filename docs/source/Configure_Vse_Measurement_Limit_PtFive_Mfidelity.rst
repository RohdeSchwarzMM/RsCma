Mfidelity
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.PtFive.Mfidelity.MfidelityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.limit.ptFive.mfidelity.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Limit_PtFive_Mfidelity_Peak.rst
	Configure_Vse_Measurement_Limit_PtFive_Mfidelity_Rms.rst