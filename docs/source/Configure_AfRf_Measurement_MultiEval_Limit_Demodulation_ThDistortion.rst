ThDistortion
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:DEModulation:THDistortion

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:DEModulation:THDistortion



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Demodulation.ThDistortion.ThDistortionCls
	:members:
	:undoc-members:
	:noindex: