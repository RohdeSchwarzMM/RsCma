Dmr
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Limit.Dmr.DmrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.digital.limit.dmr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Digital_Limit_Dmr_BitErrorRate.rst