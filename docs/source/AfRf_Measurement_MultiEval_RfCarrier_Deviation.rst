Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:DEViation
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:DEViation

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:DEViation
	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: