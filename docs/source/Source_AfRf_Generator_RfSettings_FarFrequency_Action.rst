Action
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:RFSettings:FARFrequency:ACTion

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:RFSettings:FARFrequency:ACTion



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.RfSettings.FarFrequency.Action.ActionCls
	:members:
	:undoc-members:
	:noindex: