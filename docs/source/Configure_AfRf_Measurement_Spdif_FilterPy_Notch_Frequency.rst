Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:NOTCh<Num>:FREQuency

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:NOTCh<Num>:FREQuency



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.FilterPy.Notch.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: