SpdifLeft
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:SINLeft:TMODe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:SINLeft:TMODe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.SpdifLeft.SpdifLeftCls
	:members:
	:undoc-members:
	:noindex: