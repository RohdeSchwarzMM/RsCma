Marker
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:MARKer<nr>

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:MARKer<nr>



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Frequency.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.spectrum.frequency.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Spectrum_Frequency_Marker_Absolute.rst
	GprfMeasurement_Spectrum_Frequency_Marker_Relative.rst