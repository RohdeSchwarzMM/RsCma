Generator
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:XRT:GENerator<Instance>:DSOurce

.. code-block:: python

	SOURce:XRT:GENerator<Instance>:DSOurce



.. autoclass:: RsCma.Implementations.Source.Xrt.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.xrt.generator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Xrt_Generator_Arb.rst
	Source_Xrt_Generator_Digital.rst
	Source_Xrt_Generator_Reliability.rst
	Source_Xrt_Generator_RfSettings.rst
	Source_Xrt_Generator_State.rst