Peak
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:TETRa:EVM:PEAK

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:TETRa:EVM:PEAK



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Tetra.Evm.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: