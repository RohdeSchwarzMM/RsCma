Tones
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:TONes:FDEViation
	single: SOURce:AFRF:GENerator<Instance>:TONes:PDEViation
	single: SOURce:AFRF:GENerator<Instance>:TONes:MDEPth
	single: SOURce:AFRF:GENerator<Instance>:TONes

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:TONes:FDEViation
	SOURce:AFRF:GENerator<Instance>:TONes:PDEViation
	SOURce:AFRF:GENerator<Instance>:TONes:MDEPth
	SOURce:AFRF:GENerator<Instance>:TONes



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Tones.TonesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.tones.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Tones_CtCss.rst
	Source_AfRf_Generator_Tones_Dcs.rst
	Source_AfRf_Generator_Tones_Subtone.rst