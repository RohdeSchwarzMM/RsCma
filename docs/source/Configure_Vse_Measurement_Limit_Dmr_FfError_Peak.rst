Peak
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:DMR:FFERor:PEAK

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:DMR:FFERor:PEAK



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Dmr.FfError.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: