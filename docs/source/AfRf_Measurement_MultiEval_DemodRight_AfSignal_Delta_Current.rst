Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodRight.AfSignal.Delta.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: