Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:AVERage:AVERage
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:AVERage:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:AVERage:AVERage
	READ:GPRF:MEASurement<Instance>:SPECtrum:AVERage:AVERage



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Average.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: