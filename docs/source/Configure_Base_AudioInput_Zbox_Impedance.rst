Impedance
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:AIN<nr>:ZBOX:IMPedance

.. code-block:: python

	CONFigure:BASE:AIN<nr>:ZBOX:IMPedance



.. autoclass:: RsCma.Implementations.Configure.Base.AudioInput.Zbox.Impedance.ImpedanceCls
	:members:
	:undoc-members:
	:noindex: