Xvalues
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:XVALues

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:XVALues



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.ZeroSpan.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: