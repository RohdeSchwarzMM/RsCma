Base
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALibration:BASE:ALL
	single: CALibration:BASE:ACFile

.. code-block:: python

	CALibration:BASE:ALL
	CALibration:BASE:ACFile



.. autoclass:: RsCma.Implementations.Calibration.Base.BaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calibration.base.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_Base_Latest.rst