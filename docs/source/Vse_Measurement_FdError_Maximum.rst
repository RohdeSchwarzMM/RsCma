Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:FDERror:MAXimum
	single: READ:VSE:MEASurement<Instance>:FDERror:MAXimum
	single: CALCulate:VSE:MEASurement<Instance>:FDERror:MAXimum

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:FDERror:MAXimum
	READ:VSE:MEASurement<Instance>:FDERror:MAXimum
	CALCulate:VSE:MEASurement<Instance>:FDERror:MAXimum



.. autoclass:: RsCma.Implementations.Vse.Measurement.FdError.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: