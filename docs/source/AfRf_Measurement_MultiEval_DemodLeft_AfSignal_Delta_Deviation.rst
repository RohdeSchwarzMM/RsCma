Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DELTa:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DELTa:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DELTa:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DELTa:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodLeft.AfSignal.Delta.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: