Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Voip.Power.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: