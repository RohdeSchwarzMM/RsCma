Attenuation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:ATTenuation:PORT
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:ATTenuation:STATe
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:ATTenuation

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRT:ATTenuation:PORT
	CONFigure:GPRF:MEASurement<Instance>:NRT:ATTenuation:STATe
	CONFigure:GPRF:MEASurement<Instance>:NRT:ATTenuation



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex: