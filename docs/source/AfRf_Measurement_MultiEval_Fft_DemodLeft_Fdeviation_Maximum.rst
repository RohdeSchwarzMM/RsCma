Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.DemodLeft.Fdeviation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: