Fdeviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:TONes:SCAL:FDEViation

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:TONes:SCAL:FDEViation



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Tones.Scal.Fdeviation.FdeviationCls
	:members:
	:undoc-members:
	:noindex: