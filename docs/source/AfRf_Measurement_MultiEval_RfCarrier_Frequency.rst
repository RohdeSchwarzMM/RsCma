Frequency
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.rfCarrier.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_RfCarrier_Frequency_Average.rst
	AfRf_Measurement_MultiEval_RfCarrier_Frequency_Current.rst
	AfRf_Measurement_MultiEval_RfCarrier_Frequency_Delta.rst
	AfRf_Measurement_MultiEval_RfCarrier_Frequency_Maximum.rst
	AfRf_Measurement_MultiEval_RfCarrier_Frequency_Minimum.rst