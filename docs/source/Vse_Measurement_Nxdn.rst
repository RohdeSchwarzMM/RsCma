Nxdn
----------------------------------------





.. autoclass:: RsCma.Implementations.Vse.Measurement.Nxdn.NxdnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.vse.measurement.nxdn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Vse_Measurement_Nxdn_Symbols.rst