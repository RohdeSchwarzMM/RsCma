Scal
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Tones.Scal.ScalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.limit.tones.scal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Limit_Tones_Scal_Fdeviation.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Tones_Scal_Tpause.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Tones_Scal_Ttime.rst