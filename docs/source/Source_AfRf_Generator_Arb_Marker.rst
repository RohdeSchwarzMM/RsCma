Marker
----------------------------------------





.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Arb.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.arb.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Arb_Marker_Delays.rst