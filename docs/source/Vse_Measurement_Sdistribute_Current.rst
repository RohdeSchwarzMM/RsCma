Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: READ:VSE:MEASurement<Instance>:SDIStribute:CURRent
	single: FETCh:VSE:MEASurement<Instance>:SDIStribute:CURRent

.. code-block:: python

	READ:VSE:MEASurement<Instance>:SDIStribute:CURRent
	FETCh:VSE:MEASurement<Instance>:SDIStribute:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.Sdistribute.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: