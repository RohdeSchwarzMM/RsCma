Tetra
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:TETRa:SRATe
	single: CONFigure:VSE:MEASurement<Instance>:TETRa:DEModulation

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:TETRa:SRATe
	CONFigure:VSE:MEASurement<Instance>:TETRa:DEModulation



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Tetra.TetraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.tetra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Tetra_FilterPy.rst