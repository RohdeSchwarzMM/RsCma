Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:ACP:POWer:CURRent
	single: READ:GPRF:MEASurement<Instance>:ACP:POWer:CURRent
	single: CALCulate:GPRF:MEASurement<Instance>:ACP:POWer:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:ACP:POWer:CURRent
	READ:GPRF:MEASurement<Instance>:ACP:POWer:CURRent
	CALCulate:GPRF:MEASurement<Instance>:ACP:POWer:CURRent



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: