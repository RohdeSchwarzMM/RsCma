Ecircuitry
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:AIN<nr>:ECIRcuitry

.. code-block:: python

	CONFigure:BASE:AIN<nr>:ECIRcuitry



.. autoclass:: RsCma.Implementations.Configure.Base.AudioInput.Ecircuitry.EcircuitryCls
	:members:
	:undoc-members:
	:noindex: