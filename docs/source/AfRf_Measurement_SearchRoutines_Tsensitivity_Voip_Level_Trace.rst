Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:VOIP:LEVel:TRACe
	single: READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:VOIP:LEVel:TRACe

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:VOIP:LEVel:TRACe
	READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:VOIP:LEVel:TRACe



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Tsensitivity.Voip.Level.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: