Dwidth
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FILTer:DWIDth

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FILTer:DWIDth



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.FilterPy.Dwidth.DwidthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.audioInput.filterPy.dwidth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_AudioInput_FilterPy_Dwidth_Sfactor.rst