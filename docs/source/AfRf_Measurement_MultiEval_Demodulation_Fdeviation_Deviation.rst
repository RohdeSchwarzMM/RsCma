Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:DEViation
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:DEViation

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:DEViation
	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Fdeviation.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: