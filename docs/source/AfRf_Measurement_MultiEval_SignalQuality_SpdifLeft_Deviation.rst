Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:DEViation
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:DEViation

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:DEViation
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.SpdifLeft.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: