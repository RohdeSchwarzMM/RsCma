Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodRight.AfSignal.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: