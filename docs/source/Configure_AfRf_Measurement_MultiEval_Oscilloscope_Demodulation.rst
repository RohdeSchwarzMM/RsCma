Demodulation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:XDIVision
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:MTIMe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:XDIVision
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:MTIMe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Oscilloscope.Demodulation.DemodulationCls
	:members:
	:undoc-members:
	:noindex: