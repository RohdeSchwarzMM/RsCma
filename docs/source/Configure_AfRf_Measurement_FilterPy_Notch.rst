Notch
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:FILTer:NOTCh:PATH

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:FILTer:NOTCh:PATH



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.FilterPy.Notch.NotchCls
	:members:
	:undoc-members:
	:noindex: