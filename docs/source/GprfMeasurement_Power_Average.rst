Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:AVERage
	single: FETCh:GPRF:MEASurement<Instance>:POWer:AVERage
	single: READ:GPRF:MEASurement<Instance>:POWer:AVERage

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:AVERage
	FETCh:GPRF:MEASurement<Instance>:POWer:AVERage
	READ:GPRF:MEASurement<Instance>:POWer:AVERage



.. autoclass:: RsCma.Implementations.GprfMeasurement.Power.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: