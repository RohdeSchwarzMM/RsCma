FilterPy
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:FILTer:BWIDth

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:FILTer:BWIDth



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: