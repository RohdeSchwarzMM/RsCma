Source
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AOUT<nr>:SOURce

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AOUT<nr>:SOURce



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioOutput.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: