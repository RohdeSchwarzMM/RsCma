Ifsk
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:TONes:DCS:IFSK:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:TONes:DCS:IFSK:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Tones.Dcs.Ifsk.IfskCls
	:members:
	:undoc-members:
	:noindex: