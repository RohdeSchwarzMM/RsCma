PePower
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:RFCarrier:PEPower

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:RFCarrier:PEPower



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.RfCarrier.PePower.PePowerCls
	:members:
	:undoc-members:
	:noindex: