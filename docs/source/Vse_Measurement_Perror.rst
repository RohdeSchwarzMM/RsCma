Perror
----------------------------------------





.. autoclass:: RsCma.Implementations.Vse.Measurement.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.vse.measurement.perror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Vse_Measurement_Perror_Current.rst
	Vse_Measurement_Perror_Maximum.rst