Ttl<TTL>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix2
	rc = driver.configure.base.ttl.repcap_tTL_get()
	driver.configure.base.ttl.repcap_tTL_set(repcap.TTL.Ix1)



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:TTL<Index>

.. code-block:: python

	CONFigure:BASE:TTL<Index>



.. autoclass:: RsCma.Implementations.Configure.Base.Ttl.TtlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.base.ttl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Base_Ttl_Direction.rst
	Configure_Base_Ttl_Update.rst