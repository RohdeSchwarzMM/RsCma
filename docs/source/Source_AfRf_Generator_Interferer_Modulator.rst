Modulator
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IFERer:MODulator:FDEViation
	single: SOURce:AFRF:GENerator<Instance>:IFERer:MODulator:PDEViation
	single: SOURce:AFRF:GENerator<Instance>:IFERer:MODulator:MDEPth

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IFERer:MODulator:FDEViation
	SOURce:AFRF:GENerator<Instance>:IFERer:MODulator:PDEViation
	SOURce:AFRF:GENerator<Instance>:IFERer:MODulator:MDEPth



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Interferer.Modulator.ModulatorCls
	:members:
	:undoc-members:
	:noindex: