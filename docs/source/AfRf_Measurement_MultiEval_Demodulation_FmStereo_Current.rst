Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:CURRent
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:CURRent

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:CURRent
	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.FmStereo.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: