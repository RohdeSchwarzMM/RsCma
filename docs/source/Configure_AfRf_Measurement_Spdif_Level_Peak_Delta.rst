Delta
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:PEAK:DELTa:MEASured
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:PEAK:DELTa:MODE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:PEAK:DELTa:MEASured
	CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:PEAK:DELTa:MODE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.Level.Peak.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.spdif.level.peak.delta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Spdif_Level_Peak_Delta_Update.rst
	Configure_AfRf_Measurement_Spdif_Level_Peak_Delta_User.rst