Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:MODulator:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:MODulator:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Modulator.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: