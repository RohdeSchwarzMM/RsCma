Localizer
----------------------------------------





.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Localizer.LocalizerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.ils.localizer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_Ils_Localizer_AfSettings.rst
	Source_Avionics_Generator_Ils_Localizer_IdSignal.rst
	Source_Avionics_Generator_Ils_Localizer_RfSettings.rst