ThDistortion
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:SIN:THDistortion

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:SIN:THDistortion



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Spdif.ThDistortion.ThDistortionCls
	:members:
	:undoc-members:
	:noindex: