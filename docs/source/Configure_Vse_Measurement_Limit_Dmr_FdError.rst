FdError
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:DMR:FDERor

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:DMR:FDERor



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Dmr.FdError.FdErrorCls
	:members:
	:undoc-members:
	:noindex: