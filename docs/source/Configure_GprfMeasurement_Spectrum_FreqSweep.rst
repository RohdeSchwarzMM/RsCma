FreqSweep
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.FreqSweep.FreqSweepCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.spectrum.freqSweep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_Spectrum_FreqSweep_Rbw.rst
	Configure_GprfMeasurement_Spectrum_FreqSweep_Swt.rst
	Configure_GprfMeasurement_Spectrum_FreqSweep_Vbw.rst