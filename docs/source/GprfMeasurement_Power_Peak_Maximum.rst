Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:PEAK:MAXimum
	single: FETCh:GPRF:MEASurement<Instance>:POWer:PEAK:MAXimum
	single: READ:GPRF:MEASurement<Instance>:POWer:PEAK:MAXimum

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:PEAK:MAXimum
	FETCh:GPRF:MEASurement<Instance>:POWer:PEAK:MAXimum
	READ:GPRF:MEASurement<Instance>:POWer:PEAK:MAXimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Power.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: