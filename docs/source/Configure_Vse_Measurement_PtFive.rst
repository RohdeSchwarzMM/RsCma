PtFive
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:PTFive:SRATe
	single: CONFigure:VSE:MEASurement<Instance>:PTFive:MODE
	single: CONFigure:VSE:MEASurement<Instance>:PTFive:FILTer

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:PTFive:SRATe
	CONFigure:VSE:MEASurement<Instance>:PTFive:MODE
	CONFigure:VSE:MEASurement<Instance>:PTFive:FILTer



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.PtFive.PtFiveCls
	:members:
	:undoc-members:
	:noindex: