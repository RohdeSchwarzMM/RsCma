All
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:EPSensor:STATe:ALL

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:EPSensor:STATe:ALL



.. autoclass:: RsCma.Implementations.GprfMeasurement.ExtPwrSensor.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: