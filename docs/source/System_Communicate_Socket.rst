Socket
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:COMMunicate:SOCKet:VRESource
	single: SYSTem:COMMunicate:SOCKet:MODE
	single: SYSTem:COMMunicate:SOCKet:PORT

.. code-block:: python

	SYSTem:COMMunicate:SOCKet:VRESource
	SYSTem:COMMunicate:SOCKet:MODE
	SYSTem:COMMunicate:SOCKet:PORT



.. autoclass:: RsCma.Implementations.System.Communicate.Socket.SocketCls
	:members:
	:undoc-members:
	:noindex: