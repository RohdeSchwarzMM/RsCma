Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:MTONe:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:MTONe:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.MultiTone.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: