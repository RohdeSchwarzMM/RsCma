Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Power.Delta.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: