Hexadecimal
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:TETRa:SYMBols:HEXadecimal
	single: READ:VSE:MEASurement<Instance>:TETRa:SYMBols:HEXadecimal

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:TETRa:SYMBols:HEXadecimal
	READ:VSE:MEASurement<Instance>:TETRa:SYMBols:HEXadecimal



.. autoclass:: RsCma.Implementations.Vse.Measurement.Tetra.Symbols.Hexadecimal.HexadecimalCls
	:members:
	:undoc-members:
	:noindex: