Rdy
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: STATus:MEASurement:CONDition:RDY

.. code-block:: python

	STATus:MEASurement:CONDition:RDY



.. autoclass:: RsCma.Implementations.Status.Measurement.Condition.Rdy.RdyCls
	:members:
	:undoc-members:
	:noindex: