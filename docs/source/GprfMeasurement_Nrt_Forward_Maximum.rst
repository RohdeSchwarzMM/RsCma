Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:NRT:FWARd:MAXimum
	single: FETCh:GPRF:MEASurement<Instance>:NRT:FWARd:MAXimum
	single: READ:GPRF:MEASurement<Instance>:NRT:FWARd:MAXimum

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:NRT:FWARd:MAXimum
	FETCh:GPRF:MEASurement<Instance>:NRT:FWARd:MAXimum
	READ:GPRF:MEASurement<Instance>:NRT:FWARd:MAXimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Nrt.Forward.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: