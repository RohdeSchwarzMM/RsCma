FarFrequency
----------------------------------------





.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.RfSettings.FarFrequency.FarFrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.rfSettings.farFrequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_RfSettings_FarFrequency_Action.rst