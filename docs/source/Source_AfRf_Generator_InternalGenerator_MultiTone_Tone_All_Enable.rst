Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:MTONe:TONE:ALL:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:MTONe:TONE:ALL:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.MultiTone.Tone.All.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: