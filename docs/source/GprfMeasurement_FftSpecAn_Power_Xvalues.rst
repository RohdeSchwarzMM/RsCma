Xvalues
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:XVALues

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:XVALues



.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.Power.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: