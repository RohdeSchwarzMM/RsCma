Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:NRT:FWARd:MINimum
	single: FETCh:GPRF:MEASurement<Instance>:NRT:FWARd:MINimum
	single: READ:GPRF:MEASurement<Instance>:NRT:FWARd:MINimum

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:NRT:FWARd:MINimum
	FETCh:GPRF:MEASurement<Instance>:NRT:FWARd:MINimum
	READ:GPRF:MEASurement<Instance>:NRT:FWARd:MINimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Nrt.Forward.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: