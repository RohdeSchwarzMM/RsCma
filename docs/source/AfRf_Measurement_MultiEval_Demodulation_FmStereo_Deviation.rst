Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:DEViation
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:DEViation

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:DEViation
	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.FmStereo.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: