RsCma API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCma('TCPIP::192.168.2.101::hislip0')
	# Instance range: Inst1 .. Inst32
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCma.RsCma
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	AfRf.rst
	Calibration.rst
	Configure.rst
	Display.rst
	FirmwareUpdate.rst
	FormatPy.rst
	GprfMeasurement.rst
	HardCopy.rst
	Init.rst
	Instrument.rst
	MassMemory.rst
	RecallState.rst
	SaveState.rst
	Sense.rst
	Source.rst
	Status.rst
	System.rst
	Trace.rst
	Trigger.rst
	Unit.rst
	Vse.rst