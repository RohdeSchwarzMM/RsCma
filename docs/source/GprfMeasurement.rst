GprfMeasurement
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.GprfMeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Acp.rst
	GprfMeasurement_ExtPwrSensor.rst
	GprfMeasurement_FftSpecAn.rst
	GprfMeasurement_IqRecorder.rst
	GprfMeasurement_Nrt.rst
	GprfMeasurement_Power.rst
	GprfMeasurement_Spectrum.rst