Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.ModDepth.Delta.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: