Cfactor
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:FWARd:LIMit:CFACtor

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRT:FWARd:LIMit:CFACtor



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.Forward.Limit.Cfactor.CfactorCls
	:members:
	:undoc-members:
	:noindex: