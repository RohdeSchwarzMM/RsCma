Tetra
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:SRATe
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:SDEViation
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:MODE
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:LDIRection
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:PATTern
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:BERPeriod
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:PTYPe
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:CTYPe
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:TMODe
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:CMODe
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:RPRBs
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:FILTer
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:ROFactor

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:SRATe
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:SDEViation
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:MODE
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:LDIRection
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:PATTern
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:BERPeriod
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:PTYPe
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:CTYPe
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:TMODe
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:CMODe
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:RPRBs
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:FILTer
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TETRa:ROFactor



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Tetra.TetraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.digital.tetra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Digital_Tetra_Uplink.rst