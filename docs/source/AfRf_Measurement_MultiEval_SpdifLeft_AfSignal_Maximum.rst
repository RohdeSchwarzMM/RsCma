Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifLeft.AfSignal.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: