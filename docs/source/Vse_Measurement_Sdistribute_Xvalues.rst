Xvalues
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:SDIStribute:XVALues

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:SDIStribute:XVALues



.. autoclass:: RsCma.Implementations.Vse.Measurement.Sdistribute.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: