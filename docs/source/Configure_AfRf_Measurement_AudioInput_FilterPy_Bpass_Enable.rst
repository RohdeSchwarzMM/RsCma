Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FILTer:BPASs:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FILTer:BPASs:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.FilterPy.Bpass.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: