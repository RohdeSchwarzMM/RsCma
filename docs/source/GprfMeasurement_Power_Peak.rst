Peak
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Power.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.power.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Power_Peak_Maximum.rst
	GprfMeasurement_Power_Peak_Minimum.rst