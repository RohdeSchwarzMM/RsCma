Sequence
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight:SEQuence
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight:SEQuence

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight:SEQuence
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:SINRight:SEQuence



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.SpdifRight.Sequence.SequenceCls
	:members:
	:undoc-members:
	:noindex: