ThdNoise
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:VOIP:THDNoise

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:VOIP:THDNoise



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Voip.ThdNoise.ThdNoiseCls
	:members:
	:undoc-members:
	:noindex: