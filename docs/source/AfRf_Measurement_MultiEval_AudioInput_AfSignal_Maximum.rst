Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.AfSignal.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: