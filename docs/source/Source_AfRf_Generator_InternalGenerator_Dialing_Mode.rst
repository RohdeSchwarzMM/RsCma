Mode
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:DIALing:MODE

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:DIALing:MODE



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.Dialing.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: