Binary
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:NXDN:SYMBols:BINary
	single: READ:VSE:MEASurement<Instance>:NXDN:SYMBols:BINary

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:NXDN:SYMBols:BINary
	READ:VSE:MEASurement<Instance>:NXDN:SYMBols:BINary



.. autoclass:: RsCma.Implementations.Vse.Measurement.Nxdn.Symbols.Binary.BinaryCls
	:members:
	:undoc-members:
	:noindex: