Tone<ToneNumber>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr20
	rc = driver.source.afRf.generator.internalGenerator.multiTone.tone.repcap_toneNumber_get()
	driver.source.afRf.generator.internalGenerator.multiTone.tone.repcap_toneNumber_set(repcap.ToneNumber.Nr1)





.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.MultiTone.Tone.ToneCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.internalGenerator.multiTone.tone.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_InternalGenerator_MultiTone_Tone_All.rst
	Source_AfRf_Generator_InternalGenerator_MultiTone_Tone_Enable.rst