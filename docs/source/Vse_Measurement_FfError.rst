FfError
----------------------------------------





.. autoclass:: RsCma.Implementations.Vse.Measurement.FfError.FfErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.vse.measurement.ffError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Vse_Measurement_FfError_Current.rst
	Vse_Measurement_FfError_Maximum.rst