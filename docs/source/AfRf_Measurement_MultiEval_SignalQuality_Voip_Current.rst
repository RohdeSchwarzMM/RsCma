Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:CURRent
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:CURRent

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:CURRent
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:VOIP:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.Voip.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: