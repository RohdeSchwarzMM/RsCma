Scal
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:STANdard
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:SEQuence
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:SREPeat
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:SPAuse
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:TPAuse

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:STANdard
	SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:SEQuence
	SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:SREPeat
	SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:SPAuse
	SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:TPAuse



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.Scal.ScalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.dialing.scal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Dialing_Scal_Frequency.rst
	Source_AfRf_Generator_Dialing_Scal_Ttime.rst
	Source_AfRf_Generator_Dialing_Scal_UserDefined.rst