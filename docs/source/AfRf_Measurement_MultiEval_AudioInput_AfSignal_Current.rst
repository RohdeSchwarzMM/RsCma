Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.AfSignal.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: