Voip
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Voip.VoipCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.voip.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Voip_AfSignal.rst
	AfRf_Measurement_MultiEval_Voip_Frequency.rst
	AfRf_Measurement_MultiEval_Voip_Level.rst