MultiTone
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator:THIRd:MTONe:TLEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator:THIRd:MTONe:TLEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.Third.MultiTone.MultiToneCls
	:members:
	:undoc-members:
	:noindex: