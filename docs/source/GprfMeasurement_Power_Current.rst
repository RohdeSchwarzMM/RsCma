Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:CURRent
	single: FETCh:GPRF:MEASurement<Instance>:POWer:CURRent
	single: READ:GPRF:MEASurement<Instance>:POWer:CURRent

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:CURRent
	FETCh:GPRF:MEASurement<Instance>:POWer:CURRent
	READ:GPRF:MEASurement<Instance>:POWer:CURRent



.. autoclass:: RsCma.Implementations.GprfMeasurement.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: