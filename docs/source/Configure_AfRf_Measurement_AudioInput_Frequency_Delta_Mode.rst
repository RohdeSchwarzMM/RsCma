Mode
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FREQuency:DELTa:MODE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FREQuency:DELTa:MODE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Frequency.Delta.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: