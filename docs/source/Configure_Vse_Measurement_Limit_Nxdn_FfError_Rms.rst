Rms
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:NXDN:FFERor:RMS

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:NXDN:FFERor:RMS



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Nxdn.FfError.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: