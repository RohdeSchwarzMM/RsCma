Bpass
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.FilterPy.Bpass.BpassCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.audioInput.filterPy.bpass.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_AudioInput_FilterPy_Bpass_Bandwidth.rst
	Configure_AfRf_Measurement_AudioInput_FilterPy_Bpass_Cfrequency.rst
	Configure_AfRf_Measurement_AudioInput_FilterPy_Bpass_Enable.rst