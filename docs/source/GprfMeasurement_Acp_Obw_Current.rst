Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:ACP:OBW:CURRent
	single: READ:GPRF:MEASurement<Instance>:ACP:OBW:CURRent
	single: CALCulate:GPRF:MEASurement<Instance>:ACP:OBW:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:ACP:OBW:CURRent
	READ:GPRF:MEASurement<Instance>:ACP:OBW:CURRent
	CALCulate:GPRF:MEASurement<Instance>:ACP:OBW:CURRent



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Obw.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: