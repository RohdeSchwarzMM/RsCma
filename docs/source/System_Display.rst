Display
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:DISPlay:UPDate

.. code-block:: python

	SYSTem:DISPlay:UPDate



.. autoclass:: RsCma.Implementations.System.Display.DisplayCls
	:members:
	:undoc-members:
	:noindex: