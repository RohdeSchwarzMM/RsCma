Dpmr
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Dpmr.DpmrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.dpmr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Dpmr_FilterPy.rst