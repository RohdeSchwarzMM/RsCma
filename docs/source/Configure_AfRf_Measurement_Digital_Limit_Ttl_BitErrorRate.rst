BitErrorRate
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:LIMit:TTL:BERate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:LIMit:TTL:BERate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Limit.Ttl.BitErrorRate.BitErrorRateCls
	:members:
	:undoc-members:
	:noindex: