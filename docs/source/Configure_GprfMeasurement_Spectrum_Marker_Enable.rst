Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:MARKer:ENABle:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:MARKer:ENABle:ALL



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.Marker.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: