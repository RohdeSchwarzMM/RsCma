Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.Pdeviation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: