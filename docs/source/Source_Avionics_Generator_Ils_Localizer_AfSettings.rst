AfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:CONNector
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:ENABle
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:SDM
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:DDM
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:POFFset

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:CONNector
	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:ENABle
	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:SDM
	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:DDM
	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:POFFset



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Localizer.AfSettings.AfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.ils.localizer.afSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_Ils_Localizer_AfSettings_AudioOutput.rst
	Source_Avionics_Generator_Ils_Localizer_AfSettings_Fly.rst
	Source_Avionics_Generator_Ils_Localizer_AfSettings_Fmoddepth.rst
	Source_Avionics_Generator_Ils_Localizer_AfSettings_Frequency.rst