Delta
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:RFCarrier:POWer:DELTa:MODE
	single: CONFigure:AFRF:MEASurement<Instance>:RFCarrier:POWer:DELTa:USER
	single: CONFigure:AFRF:MEASurement<Instance>:RFCarrier:POWer:DELTa:MEASured

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:RFCarrier:POWer:DELTa:MODE
	CONFigure:AFRF:MEASurement<Instance>:RFCarrier:POWer:DELTa:USER
	CONFigure:AFRF:MEASurement<Instance>:RFCarrier:POWer:DELTa:MEASured



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.RfCarrier.Power.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.rfCarrier.power.delta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_RfCarrier_Power_Delta_Update.rst