Span
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:SPAN:MODE
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:SPAN

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:SPAN:MODE
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:SPAN



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.Frequency.Span.SpanCls
	:members:
	:undoc-members:
	:noindex: