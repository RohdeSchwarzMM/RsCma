Mode
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>:MODE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>:MODE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.AudioInput.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: