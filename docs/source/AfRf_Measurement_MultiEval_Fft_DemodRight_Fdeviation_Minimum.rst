Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMRight:FDEViation:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMRight:FDEViation:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMRight:FDEViation:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMRight:FDEViation:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.DemodRight.Fdeviation.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: