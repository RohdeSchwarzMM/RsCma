Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINRight:FREQuency:DELTa:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINRight:FREQuency:DELTa:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINRight:FREQuency:DELTa:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:SINRight:FREQuency:DELTa:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifRight.Frequency.Delta.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: