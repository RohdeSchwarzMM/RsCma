State
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:STATe
	single: SOURce:AFRF:GENerator<Instance>:STATe:ALL

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:STATe
	SOURce:AFRF:GENerator<Instance>:STATe:ALL



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.State.StateCls
	:members:
	:undoc-members:
	:noindex: