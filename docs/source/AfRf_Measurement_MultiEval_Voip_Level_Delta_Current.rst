Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:LEVel:DELTa:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:LEVel:DELTa:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:LEVel:DELTa:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:LEVel:DELTa:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Voip.Level.Delta.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: