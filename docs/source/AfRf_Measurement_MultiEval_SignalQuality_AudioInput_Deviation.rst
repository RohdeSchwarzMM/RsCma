Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:DEViation
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:DEViation

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:DEViation
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.AudioInput.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: