Application
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: DISPlay:AFRF:MEASurement<Instance>:DIGital:APPLication:SELect

.. code-block:: python

	DISPlay:AFRF:MEASurement<Instance>:DIGital:APPLication:SELect



.. autoclass:: RsCma.Implementations.Display.AfRf.Measurement.Digital.Application.ApplicationCls
	:members:
	:undoc-members:
	:noindex: