Execute
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:AFRF:GENerator<Instance>:ARB:MANual:EXECute

.. code-block:: python

	TRIGger:AFRF:GENerator<Instance>:ARB:MANual:EXECute



.. autoclass:: RsCma.Implementations.Trigger.AfRf.Generator.Arb.Manual.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: