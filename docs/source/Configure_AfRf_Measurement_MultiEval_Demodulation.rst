Demodulation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:DEModulation:TMODe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:DEModulation:TMODe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Demodulation.DemodulationCls
	:members:
	:undoc-members:
	:noindex: