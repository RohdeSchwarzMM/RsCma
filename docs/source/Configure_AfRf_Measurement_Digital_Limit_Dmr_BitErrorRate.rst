BitErrorRate
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:LIMit:DMR:BERate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:LIMit:DMR:BERate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Limit.Dmr.BitErrorRate.BitErrorRateCls
	:members:
	:undoc-members:
	:noindex: