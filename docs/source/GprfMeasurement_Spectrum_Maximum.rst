Maximum
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.spectrum.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Spectrum_Maximum_Average.rst
	GprfMeasurement_Spectrum_Maximum_Current.rst
	GprfMeasurement_Spectrum_Maximum_Maximum.rst
	GprfMeasurement_Spectrum_Maximum_Minimum.rst