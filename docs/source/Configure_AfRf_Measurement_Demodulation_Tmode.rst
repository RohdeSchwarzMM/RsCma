Tmode
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:TMODe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:TMODe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.Tmode.TmodeCls
	:members:
	:undoc-members:
	:noindex: