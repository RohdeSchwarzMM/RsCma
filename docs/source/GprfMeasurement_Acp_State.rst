State
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:ACP:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:ACP:STATe



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.acp.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Acp_State_All.rst