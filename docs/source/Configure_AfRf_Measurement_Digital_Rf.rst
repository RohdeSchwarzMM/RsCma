Rf
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:RF:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:RF:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Rf.RfCls
	:members:
	:undoc-members:
	:noindex: