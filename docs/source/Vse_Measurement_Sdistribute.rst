Sdistribute
----------------------------------------





.. autoclass:: RsCma.Implementations.Vse.Measurement.Sdistribute.SdistributeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.vse.measurement.sdistribute.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Vse_Measurement_Sdistribute_Current.rst
	Vse_Measurement_Sdistribute_Xvalues.rst