Eimpedance
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:AOUT<nr>:EIMPedance

.. code-block:: python

	CONFigure:BASE:AOUT<nr>:EIMPedance



.. autoclass:: RsCma.Implementations.Configure.Base.AudioOutput.Eimpedance.EimpedanceCls
	:members:
	:undoc-members:
	:noindex: