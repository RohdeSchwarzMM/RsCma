Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SOUT:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SOUT:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Sout.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: