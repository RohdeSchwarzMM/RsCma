Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:MAXimum
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:MAXimum

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:MAXimum
	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Fdeviation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: