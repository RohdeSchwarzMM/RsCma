Nrt
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:NRT
	single: STOP:GPRF:MEASurement<Instance>:NRT
	single: ABORt:GPRF:MEASurement<Instance>:NRT
	single: FETCh:GPRF:MEASurement<Instance>:NRT:IDN

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:NRT
	STOP:GPRF:MEASurement<Instance>:NRT
	ABORt:GPRF:MEASurement<Instance>:NRT
	FETCh:GPRF:MEASurement<Instance>:NRT:IDN



.. autoclass:: RsCma.Implementations.GprfMeasurement.Nrt.NrtCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.nrt.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Nrt_Forward.rst
	GprfMeasurement_Nrt_Reverse.rst
	GprfMeasurement_Nrt_State.rst