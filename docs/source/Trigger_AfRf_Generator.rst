Generator
----------------------------------------





.. autoclass:: RsCma.Implementations.Trigger.AfRf.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.afRf.generator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_AfRf_Generator_Arb.rst