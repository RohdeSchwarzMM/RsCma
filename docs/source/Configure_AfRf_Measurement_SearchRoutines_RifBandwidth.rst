RifBandwidth
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:SDMethod
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:RSResults

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:SDMethod
	CONFigure:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:RSResults



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.RifBandwidth.RifBandwidthCls
	:members:
	:undoc-members:
	:noindex: