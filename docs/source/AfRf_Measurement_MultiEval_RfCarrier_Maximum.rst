Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:MAXimum
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:MAXimum

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:MAXimum
	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: