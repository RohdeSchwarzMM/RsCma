AudioOutput
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:AOUT:ENABle
	single: SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:AOUT:LEVel

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:AOUT:ENABle
	SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:AOUT:LEVel



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Vor.AfSettings.AudioOutput.AudioOutputCls
	:members:
	:undoc-members:
	:noindex: