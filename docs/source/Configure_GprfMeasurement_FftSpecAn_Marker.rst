Marker<MarkerOther>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr2 .. Nr5
	rc = driver.configure.gprfMeasurement.fftSpecAn.marker.repcap_markerOther_get()
	driver.configure.gprfMeasurement.fftSpecAn.marker.repcap_markerOther_set(repcap.MarkerOther.Nr2)





.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.FftSpecAn.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.fftSpecAn.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_FftSpecAn_Marker_Enable.rst
	Configure_GprfMeasurement_FftSpecAn_Marker_Placement.rst