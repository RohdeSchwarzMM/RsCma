Sdecay
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:SDECay

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:SDECay



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Sdecay.SdecayCls
	:members:
	:undoc-members:
	:noindex: