Frequency<FrequencyLobe>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.source.avionics.generator.ils.localizer.afSettings.frequency.repcap_frequencyLobe_get()
	driver.source.avionics.generator.ils.localizer.afSettings.frequency.repcap_frequencyLobe_set(repcap.FrequencyLobe.Nr1)



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:FREQuency<nr>

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:FREQuency<nr>



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Localizer.AfSettings.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.ils.localizer.afSettings.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_Ils_Localizer_AfSettings_Frequency_Enable.rst