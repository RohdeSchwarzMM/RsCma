Spectrum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:AMODe
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:REPetition
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:RCOupling
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:TOUT
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:SCOunt

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:AMODe
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:REPetition
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:RCOupling
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:TOUT
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:SCOunt



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_Spectrum_FreqSweep.rst
	Configure_GprfMeasurement_Spectrum_Frequency.rst
	Configure_GprfMeasurement_Spectrum_Marker.rst
	Configure_GprfMeasurement_Spectrum_Tgenerator.rst
	Configure_GprfMeasurement_Spectrum_Vswr.rst
	Configure_GprfMeasurement_Spectrum_ZeroSpan.rst