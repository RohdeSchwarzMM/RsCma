Reverse
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Nrt.Reverse.ReverseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.nrt.reverse.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Nrt_Reverse_Average.rst
	GprfMeasurement_Nrt_Reverse_Current.rst
	GprfMeasurement_Nrt_Reverse_Maximum.rst
	GprfMeasurement_Nrt_Reverse_Minimum.rst