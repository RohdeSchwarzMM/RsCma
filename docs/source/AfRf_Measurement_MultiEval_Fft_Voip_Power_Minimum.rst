Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Voip.Power.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: