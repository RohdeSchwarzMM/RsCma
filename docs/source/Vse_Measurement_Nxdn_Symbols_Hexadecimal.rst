Hexadecimal
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:NXDN:SYMBols:HEXadecimal
	single: READ:VSE:MEASurement<Instance>:NXDN:SYMBols:HEXadecimal

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:NXDN:SYMBols:HEXadecimal
	READ:VSE:MEASurement<Instance>:NXDN:SYMBols:HEXadecimal



.. autoclass:: RsCma.Implementations.Vse.Measurement.Nxdn.Symbols.Hexadecimal.HexadecimalCls
	:members:
	:undoc-members:
	:noindex: