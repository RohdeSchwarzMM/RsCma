Bin
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: READ:GPRF:MEASurement<Instance>:IQRecorder:BIN
	single: FETCh:GPRF:MEASurement<Instance>:IQRecorder:BIN

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:IQRecorder:BIN
	FETCh:GPRF:MEASurement<Instance>:IQRecorder:BIN



.. autoclass:: RsCma.Implementations.GprfMeasurement.IqRecorder.Bin.BinCls
	:members:
	:undoc-members:
	:noindex: