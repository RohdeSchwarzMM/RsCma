Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:AVERage
	single: CALCulate:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:AVERage
	single: READ:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:AVERage
	CALCulate:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:AVERage
	READ:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Ttl.BitErrorRate.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: