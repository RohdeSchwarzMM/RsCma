Channel
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:RFSettings:CHANnel

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:RFSettings:CHANnel



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Localizer.RfSettings.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: