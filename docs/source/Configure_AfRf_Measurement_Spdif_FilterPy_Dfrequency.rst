Dfrequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:DFRequency

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:DFRequency



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.FilterPy.Dfrequency.DfrequencyCls
	:members:
	:undoc-members:
	:noindex: