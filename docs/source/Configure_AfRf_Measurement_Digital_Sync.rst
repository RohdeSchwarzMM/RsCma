Sync
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Sync.SyncCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.digital.sync.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Digital_Sync_Timeout.rst