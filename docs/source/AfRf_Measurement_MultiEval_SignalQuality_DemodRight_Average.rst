Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:AVERage
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:AVERage

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:AVERage
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.DemodRight.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: