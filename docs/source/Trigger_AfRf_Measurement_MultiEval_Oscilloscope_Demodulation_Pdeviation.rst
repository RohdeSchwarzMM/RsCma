Pdeviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:PDEViation:THReshold

.. code-block:: python

	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:PDEViation:THReshold



.. autoclass:: RsCma.Implementations.Trigger.AfRf.Measurement.MultiEval.Oscilloscope.Demodulation.Pdeviation.PdeviationCls
	:members:
	:undoc-members:
	:noindex: