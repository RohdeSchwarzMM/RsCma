Repetitions
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP:REPetitions
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP:REPetitions

.. code-block:: python

	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP:REPetitions
	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP:REPetitions



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Voip.Repetitions.RepetitionsCls
	:members:
	:undoc-members:
	:noindex: