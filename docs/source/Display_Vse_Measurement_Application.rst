Application
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: DISPlay:VSE:MEASurement<Instance>:APPLication:SELect

.. code-block:: python

	DISPlay:VSE:MEASurement<Instance>:APPLication:SELect



.. autoclass:: RsCma.Implementations.Display.Vse.Measurement.Application.ApplicationCls
	:members:
	:undoc-members:
	:noindex: