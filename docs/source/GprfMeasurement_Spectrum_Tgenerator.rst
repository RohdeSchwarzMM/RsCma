Tgenerator
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Tgenerator.TgeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.spectrum.tgenerator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Spectrum_Tgenerator_RefDataAvailable.rst