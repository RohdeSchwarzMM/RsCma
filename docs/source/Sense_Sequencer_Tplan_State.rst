State
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SENSe:SEQuencer:TPLan:STATe

.. code-block:: python

	SENSe:SEQuencer:TPLan:STATe



.. autoclass:: RsCma.Implementations.Sense.Sequencer.Tplan.State.StateCls
	:members:
	:undoc-members:
	:noindex: