Counter
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:DETection
	single: CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:AFRequency
	single: CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:GCOupling
	single: CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:REPetition
	single: CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:TOUT
	single: CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:SPOWer
	single: CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:AUTomatic
	single: CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:MODE
	single: CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:FEPower
	single: CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:BURSt

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:DETection
	CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:AFRequency
	CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:GCOupling
	CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:REPetition
	CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:TOUT
	CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:SPOWer
	CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:AUTomatic
	CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:MODE
	CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:FEPower
	CONFigure:AFRF:MEASurement<Instance>:FREQuency:COUNter:BURSt



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Frequency.Counter.CounterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.frequency.counter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Frequency_Counter_Frange.rst
	Configure_AfRf_Measurement_Frequency_Counter_Use.rst