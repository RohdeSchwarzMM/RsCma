Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:CURRent
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:CURRent

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:CURRent
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.DemodRight.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: