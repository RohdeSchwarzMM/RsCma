Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:AFSignal:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:AFSignal:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:AFSignal:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:AFSignal:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.Second.AfSignal.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: