SearchRoutines
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: INITiate:AFRF:MEASurement<Instance>:SROutines
	single: STOP:AFRF:MEASurement<Instance>:SROutines
	single: ABORt:AFRF:MEASurement<Instance>:SROutines

.. code-block:: python

	INITiate:AFRF:MEASurement<Instance>:SROutines
	STOP:AFRF:MEASurement<Instance>:SROutines
	ABORt:AFRF:MEASurement<Instance>:SROutines



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.SearchRoutinesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.searchRoutines.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_SearchRoutines_RifBandwidth.rst
	AfRf_Measurement_SearchRoutines_Rsensitivity.rst
	AfRf_Measurement_SearchRoutines_Rsquelch.rst
	AfRf_Measurement_SearchRoutines_Ssnr.rst
	AfRf_Measurement_SearchRoutines_State.rst
	AfRf_Measurement_SearchRoutines_Tsensitivity.rst