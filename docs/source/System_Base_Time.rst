Time
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:TIME

.. code-block:: python

	SYSTem:BASE:TIME



.. autoclass:: RsCma.Implementations.System.Base.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.base.time.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Base_Time_Tzone.rst