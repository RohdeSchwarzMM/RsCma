PtFive
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:PTFive:PTYPe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:PTFive:PTYPe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.PtFive.PtFiveCls
	:members:
	:undoc-members:
	:noindex: