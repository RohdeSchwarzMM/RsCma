Relative
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<nr>:MARKer<mnr>:RELative

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<nr>:MARKer<mnr>:RELative



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.AudioInput.Marker.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: