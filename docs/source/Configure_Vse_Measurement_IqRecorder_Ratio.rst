Ratio
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:IQRecorder:RATio

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:IQRecorder:RATio



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.IqRecorder.Ratio.RatioCls
	:members:
	:undoc-members:
	:noindex: