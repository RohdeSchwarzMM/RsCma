AudioInput<AudioInput>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.source.afRf.generator.audioInput.repcap_audioInput_get()
	driver.source.afRf.generator.audioInput.repcap_audioInput_set(repcap.AudioInput.Nr1)





.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.AudioInput.AudioInputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.audioInput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_AudioInput_Aranging.rst
	Source_AfRf_Generator_AudioInput_First.rst
	Source_AfRf_Generator_AudioInput_Icoupling.rst
	Source_AfRf_Generator_AudioInput_Mlevel.rst
	Source_AfRf_Generator_AudioInput_Second.rst