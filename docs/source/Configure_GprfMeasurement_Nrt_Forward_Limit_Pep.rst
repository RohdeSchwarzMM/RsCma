Pep
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:FWARd:LIMit:PEP

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRT:FWARd:LIMit:PEP



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.Forward.Limit.Pep.PepCls
	:members:
	:undoc-members:
	:noindex: