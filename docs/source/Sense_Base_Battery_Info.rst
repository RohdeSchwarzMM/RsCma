Info
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SENSe:BASE:BATTery<BattIdx>:INFO

.. code-block:: python

	SENSe:BASE:BATTery<BattIdx>:INFO



.. autoclass:: RsCma.Implementations.Sense.Base.Battery.Info.InfoCls
	:members:
	:undoc-members:
	:noindex: