Coffset
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:COFFset
	single: READ:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:COFFset

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:COFFset
	READ:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:COFFset



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.RifBandwidth.Coffset.CoffsetCls
	:members:
	:undoc-members:
	:noindex: