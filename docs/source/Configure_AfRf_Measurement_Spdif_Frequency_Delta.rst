Delta
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FREQuency:DELTa:MODE
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FREQuency:DELTa:MEASured

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FREQuency:DELTa:MODE
	CONFigure:AFRF:MEASurement<Instance>:SIN:FREQuency:DELTa:MEASured



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.Frequency.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.spdif.frequency.delta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Spdif_Frequency_Delta_Update.rst
	Configure_AfRf_Measurement_Spdif_Frequency_Delta_User.rst