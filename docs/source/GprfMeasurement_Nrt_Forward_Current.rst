Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:NRT:FWARd:CURRent
	single: FETCh:GPRF:MEASurement<Instance>:NRT:FWARd:CURRent
	single: READ:GPRF:MEASurement<Instance>:NRT:FWARd:CURRent

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:NRT:FWARd:CURRent
	FETCh:GPRF:MEASurement<Instance>:NRT:FWARd:CURRent
	READ:GPRF:MEASurement<Instance>:NRT:FWARd:CURRent



.. autoclass:: RsCma.Implementations.GprfMeasurement.Nrt.Forward.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: