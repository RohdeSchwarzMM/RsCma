SpdifLeft
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.SpdifLeft.SpdifLeftCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.tones.spdifLeft.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Tones_SpdifLeft_Repetitions.rst
	AfRf_Measurement_MultiEval_Tones_SpdifLeft_Sequence.rst