ModDepth
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.ModDepth.ModDepthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.fft.demodulation.modDepth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Fft_Demodulation_ModDepth_Average.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation_ModDepth_Current.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation_ModDepth_Maximum.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation_ModDepth_Minimum.rst