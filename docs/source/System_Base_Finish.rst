Finish
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:FINish

.. code-block:: python

	SYSTem:BASE:FINish



.. autoclass:: RsCma.Implementations.System.Base.Finish.FinishCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.base.finish.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Base_Finish_Device.rst