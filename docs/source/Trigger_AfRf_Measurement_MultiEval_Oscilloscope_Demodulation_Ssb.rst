Ssb
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:SSB:THReshold

.. code-block:: python

	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:SSB:THReshold



.. autoclass:: RsCma.Implementations.Trigger.AfRf.Measurement.MultiEval.Oscilloscope.Demodulation.Ssb.SsbCls
	:members:
	:undoc-members:
	:noindex: