Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:LEVel:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:LEVel:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Level.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: