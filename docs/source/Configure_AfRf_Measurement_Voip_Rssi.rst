Rssi
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:RSSI:CODE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:RSSI:CODE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.Rssi.RssiCls
	:members:
	:undoc-members:
	:noindex: