AfSignal
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodLeft.AfSignal.AfSignalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.demodLeft.afSignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_DemodLeft_AfSignal_Average.rst
	AfRf_Measurement_MultiEval_DemodLeft_AfSignal_Current.rst
	AfRf_Measurement_MultiEval_DemodLeft_AfSignal_Delta.rst
	AfRf_Measurement_MultiEval_DemodLeft_AfSignal_Deviation.rst
	AfRf_Measurement_MultiEval_DemodLeft_AfSignal_Maximum.rst