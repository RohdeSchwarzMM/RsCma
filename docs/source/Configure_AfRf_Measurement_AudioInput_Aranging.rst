Aranging
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:ARANging

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:ARANging



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Aranging.ArangingCls
	:members:
	:undoc-members:
	:noindex: