ModDepth
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:MDEPth
	single: CALCulate:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:MDEPth
	single: READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:MDEPth

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:MDEPth
	CALCulate:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:MDEPth
	READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:MDEPth



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Tsensitivity.ModDepth.ModDepthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.searchRoutines.tsensitivity.modDepth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_SearchRoutines_Tsensitivity_ModDepth_Trace.rst