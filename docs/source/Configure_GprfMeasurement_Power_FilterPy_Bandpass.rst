Bandpass
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:FILTer:BANDpass:BWIDth

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:FILTer:BANDpass:BWIDth



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Power.FilterPy.Bandpass.BandpassCls
	:members:
	:undoc-members:
	:noindex: