Timeout
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:TOUT

.. code-block:: python

	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:TOUT



.. autoclass:: RsCma.Implementations.Trigger.AfRf.Measurement.MultiEval.Oscilloscope.Timeout.TimeoutCls
	:members:
	:undoc-members:
	:noindex: