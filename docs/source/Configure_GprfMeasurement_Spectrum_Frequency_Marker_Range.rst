Range
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:MARKer<nr>:RANGe

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:MARKer<nr>:RANGe



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.Frequency.Marker.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: