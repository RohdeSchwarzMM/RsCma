Pdeviation
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Demodulation.Pdeviation.PdeviationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.limit.demodulation.pdeviation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_Pdeviation_Peak.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_Pdeviation_Rms.rst