DemodRight
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.DemodRight.DemodRightCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.signalQuality.demodRight.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_SignalQuality_DemodRight_Average.rst
	AfRf_Measurement_MultiEval_SignalQuality_DemodRight_Current.rst
	AfRf_Measurement_MultiEval_SignalQuality_DemodRight_Deviation.rst
	AfRf_Measurement_MultiEval_SignalQuality_DemodRight_Extreme.rst