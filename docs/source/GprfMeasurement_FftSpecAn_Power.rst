Power
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.fftSpecAn.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_FftSpecAn_Power_Average.rst
	GprfMeasurement_FftSpecAn_Power_Current.rst
	GprfMeasurement_FftSpecAn_Power_Maximum.rst
	GprfMeasurement_FftSpecAn_Power_Minimum.rst
	GprfMeasurement_FftSpecAn_Power_Xvalues.rst