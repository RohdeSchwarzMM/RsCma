Foffset
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:RIFBandwidth:FOFFset

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:RIFBandwidth:FOFFset



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Limit.RifBandwidth.Foffset.FoffsetCls
	:members:
	:undoc-members:
	:noindex: