Catalog
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:BASE:OUT:CATalog:SOURce

.. code-block:: python

	TRIGger:BASE:OUT:CATalog:SOURce



.. autoclass:: RsCma.Implementations.Trigger.Base.Out.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: