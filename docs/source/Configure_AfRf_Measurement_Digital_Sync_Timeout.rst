Timeout
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:SYNC:TOUT

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:SYNC:TOUT



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Sync.Timeout.TimeoutCls
	:members:
	:undoc-members:
	:noindex: