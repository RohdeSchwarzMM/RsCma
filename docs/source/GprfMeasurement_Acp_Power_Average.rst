Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:ACP:POWer:AVERage
	single: READ:GPRF:MEASurement<Instance>:ACP:POWer:AVERage
	single: CALCulate:GPRF:MEASurement<Instance>:ACP:POWer:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:ACP:POWer:AVERage
	READ:GPRF:MEASurement<Instance>:ACP:POWer:AVERage
	CALCulate:GPRF:MEASurement<Instance>:ACP:POWer:AVERage



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Power.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: