Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FREQuency:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FREQuency:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.Frequency.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: