Data
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FORMat:BASE[:DATA]

.. code-block:: python

	FORMat:BASE[:DATA]



.. autoclass:: RsCma.Implementations.FormatPy.Base.Data.DataCls
	:members:
	:undoc-members:
	:noindex: