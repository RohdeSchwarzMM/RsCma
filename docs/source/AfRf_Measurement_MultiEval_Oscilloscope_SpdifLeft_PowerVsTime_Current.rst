Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SINLeft:PVTime:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SINLeft:PVTime:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SINLeft:PVTime:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SINLeft:PVTime:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.SpdifLeft.PowerVsTime.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: