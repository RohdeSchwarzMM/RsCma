LcWord
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:LCWord
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:LCWord

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:LCWord
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:LCWord



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.LcWord.LcWordCls
	:members:
	:undoc-members:
	:noindex: