Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:AVERage
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:AVERage

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:AVERage
	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Pdeviation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: