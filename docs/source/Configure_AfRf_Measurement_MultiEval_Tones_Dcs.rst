Dcs
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:ECWord
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:IMODulation

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:ECWord
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:IMODulation



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Dcs.DcsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.tones.dcs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Tones_Dcs_Timeout.rst