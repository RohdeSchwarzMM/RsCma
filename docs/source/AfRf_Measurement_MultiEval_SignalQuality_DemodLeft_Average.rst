Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:AVERage
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:AVERage

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:AVERage
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.DemodLeft.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: