Fft
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.FftCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.fft.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Fft_AudioInput.rst
	AfRf_Measurement_MultiEval_Fft_DemodLeft.rst
	AfRf_Measurement_MultiEval_Fft_DemodRight.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation.rst
	AfRf_Measurement_MultiEval_Fft_Spdif.rst
	AfRf_Measurement_MultiEval_Fft_SpdifLeft.rst
	AfRf_Measurement_MultiEval_Fft_SpdifRight.rst
	AfRf_Measurement_MultiEval_Fft_Voip.rst