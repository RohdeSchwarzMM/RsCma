Tlevel
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:TLEVel
	single: READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:TLEVel

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:TLEVel
	READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:TLEVel



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsquelch.Tlevel.TlevelCls
	:members:
	:undoc-members:
	:noindex: