AfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:CONNector

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:CONNector



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Vor.AfSettings.AfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.vor.afSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_Vor_AfSettings_AudioOutput.rst
	Source_Avionics_Generator_Vor_AfSettings_Reference.rst
	Source_Avionics_Generator_Vor_AfSettings_Vphase.rst