Rf
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IFERer:RF:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IFERer:RF:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Interferer.Rf.RfCls
	:members:
	:undoc-members:
	:noindex: