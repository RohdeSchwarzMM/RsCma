RfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:RFSettings:FREQuency
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:RFSettings:LEVel

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:RFSettings:FREQuency
	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:RFSettings:LEVel



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Localizer.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.ils.localizer.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_Ils_Localizer_RfSettings_Channel.rst
	Source_Avionics_Generator_Ils_Localizer_RfSettings_RfOut.rst