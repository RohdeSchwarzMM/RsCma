Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:FFT:MARKer:ENABle:ALL

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:FFT:MARKer:ENABle:ALL



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Fft.Marker.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: