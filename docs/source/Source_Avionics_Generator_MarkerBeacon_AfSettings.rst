AfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:MBEacon:AFSettings:CONNector
	single: SOURce:AVIonics:GENerator<Instance>:MBEacon:AFSettings:ENABle
	single: SOURce:AVIonics:GENerator<Instance>:MBEacon:AFSettings:MDEPth
	single: SOURce:AVIonics:GENerator<Instance>:MBEacon:AFSettings:FREQuency

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:MBEacon:AFSettings:CONNector
	SOURce:AVIonics:GENerator<Instance>:MBEacon:AFSettings:ENABle
	SOURce:AVIonics:GENerator<Instance>:MBEacon:AFSettings:MDEPth
	SOURce:AVIonics:GENerator<Instance>:MBEacon:AFSettings:FREQuency



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.MarkerBeacon.AfSettings.AfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.markerBeacon.afSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_MarkerBeacon_AfSettings_AudioOutput.rst