Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.PePower.Delta.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: