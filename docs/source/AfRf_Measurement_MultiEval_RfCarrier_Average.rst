Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:AVERage
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:AVERage

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:AVERage
	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: