Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:RFCarrier:FERRor:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:RFCarrier:FERRor:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.RfCarrier.FreqError.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: