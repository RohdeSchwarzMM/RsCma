Delta
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Level.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.audioInput.level.delta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_AudioInput_Level_Delta_Mode.rst
	Configure_AfRf_Measurement_AudioInput_Level_Delta_Update.rst