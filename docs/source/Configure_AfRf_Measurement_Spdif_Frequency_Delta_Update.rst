Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FREQuency:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FREQuency:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.Frequency.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: