RfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:RFSettings:DGAin
	single: SOURce:AFRF:GENerator<Instance>:RFSettings:EATTenuation
	single: SOURce:AFRF:GENerator<Instance>:RFSettings:FREQuency
	single: SOURce:AFRF:GENerator<Instance>:RFSettings:LEVel
	single: SOURce:AFRF:GENerator<Instance>:RFSettings:PEPower
	single: SOURce:AFRF:GENerator<Instance>:RFSettings:RFCoupling
	single: SOURce:AFRF:GENerator<Instance>:RFSettings:CONNector
	single: SOURce:AFRF:GENerator<Instance>:RFSettings:CHANnel
	single: SOURce:AFRF:GENerator<Instance>:RFSettings:COFFset

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:RFSettings:DGAin
	SOURce:AFRF:GENerator<Instance>:RFSettings:EATTenuation
	SOURce:AFRF:GENerator<Instance>:RFSettings:FREQuency
	SOURce:AFRF:GENerator<Instance>:RFSettings:LEVel
	SOURce:AFRF:GENerator<Instance>:RFSettings:PEPower
	SOURce:AFRF:GENerator<Instance>:RFSettings:RFCoupling
	SOURce:AFRF:GENerator<Instance>:RFSettings:CONNector
	SOURce:AFRF:GENerator<Instance>:RFSettings:CHANnel
	SOURce:AFRF:GENerator<Instance>:RFSettings:COFFset



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_RfSettings_FarFrequency.rst
	Source_AfRf_Generator_RfSettings_Rf.rst