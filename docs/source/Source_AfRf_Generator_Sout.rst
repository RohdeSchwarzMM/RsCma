Sout
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:SOUT

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:SOUT



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Sout.SoutCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.sout.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Sout_Enable.rst
	Source_AfRf_Generator_Sout_Level.rst