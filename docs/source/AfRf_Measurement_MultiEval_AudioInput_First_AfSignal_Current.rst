Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.First.AfSignal.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: