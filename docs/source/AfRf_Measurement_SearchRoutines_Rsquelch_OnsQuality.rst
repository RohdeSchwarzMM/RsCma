OnsQuality
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:ONSQuality
	single: READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:ONSQuality

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:ONSQuality
	READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:ONSQuality



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsquelch.OnsQuality.OnsQualityCls
	:members:
	:undoc-members:
	:noindex: