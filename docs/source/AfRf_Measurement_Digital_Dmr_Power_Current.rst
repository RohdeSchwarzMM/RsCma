Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:DMR:POWer:CURRent
	single: READ:AFRF:MEASurement<Instance>:DIGital:DMR:POWer:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:DMR:POWer:CURRent
	READ:AFRF:MEASurement<Instance>:DIGital:DMR:POWer:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Dmr.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: