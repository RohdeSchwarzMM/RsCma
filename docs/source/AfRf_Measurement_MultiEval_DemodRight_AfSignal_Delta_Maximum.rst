Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodRight.AfSignal.Delta.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: