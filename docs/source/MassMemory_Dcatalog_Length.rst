Length
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: MMEMory:DCATalog:LENGth

.. code-block:: python

	MMEMory:DCATalog:LENGth



.. autoclass:: RsCma.Implementations.MassMemory.Dcatalog.Length.LengthCls
	:members:
	:undoc-members:
	:noindex: