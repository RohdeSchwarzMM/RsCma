Average
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Average.AverageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.spectrum.average.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Spectrum_Average_Average.rst
	GprfMeasurement_Spectrum_Average_Current.rst
	GprfMeasurement_Spectrum_Average_Maximum.rst
	GprfMeasurement_Spectrum_Average_Minimum.rst