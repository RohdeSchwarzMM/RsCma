AudioOutput
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:MBEacon:AFSettings:AOUT:ENABle
	single: SOURce:AVIonics:GENerator<Instance>:MBEacon:AFSettings:AOUT:LEVel

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:MBEacon:AFSettings:AOUT:ENABle
	SOURce:AVIonics:GENerator<Instance>:MBEacon:AFSettings:AOUT:LEVel



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.MarkerBeacon.AfSettings.AudioOutput.AudioOutputCls
	:members:
	:undoc-members:
	:noindex: