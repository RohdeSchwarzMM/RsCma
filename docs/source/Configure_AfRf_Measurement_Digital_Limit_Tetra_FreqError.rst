FreqError
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:LIMit:TETRa:FERRor

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:LIMit:TETRa:FERRor



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Limit.Tetra.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex: