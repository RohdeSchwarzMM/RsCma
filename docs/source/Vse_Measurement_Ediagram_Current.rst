Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:EDIagram:CURRent
	single: READ:VSE:MEASurement<Instance>:EDIagram:CURRent

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:EDIagram:CURRent
	READ:VSE:MEASurement<Instance>:EDIagram:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.Ediagram.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: