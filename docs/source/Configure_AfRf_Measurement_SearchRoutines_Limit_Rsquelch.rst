Rsquelch
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Limit.Rsquelch.RsquelchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.searchRoutines.limit.rsquelch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_SearchRoutines_Limit_Rsquelch_Thysteresis.rst
	Configure_AfRf_Measurement_SearchRoutines_Limit_Rsquelch_Tsensitivity.rst