Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.First.AfSignal.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: