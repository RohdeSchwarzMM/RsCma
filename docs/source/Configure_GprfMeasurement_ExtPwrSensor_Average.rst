Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:AVERage:APERture

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:EPSensor:AVERage:APERture



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.ExtPwrSensor.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: