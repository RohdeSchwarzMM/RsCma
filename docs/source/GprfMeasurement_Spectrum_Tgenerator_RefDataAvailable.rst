RefDataAvailable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:TGENerator:RDAVailable

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:TGENerator:RDAVailable



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Tgenerator.RefDataAvailable.RefDataAvailableCls
	:members:
	:undoc-members:
	:noindex: