Bpass
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:BPASs:ENABle
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:BPASs:CFRequency
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:BPASs:BWIDth

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:BPASs:ENABle
	CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:BPASs:CFRequency
	CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:BPASs:BWIDth



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.FilterPy.Bpass.BpassCls
	:members:
	:undoc-members:
	:noindex: