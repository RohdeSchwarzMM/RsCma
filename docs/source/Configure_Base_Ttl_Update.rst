Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:TTL<Index>:UPDate

.. code-block:: python

	CONFigure:BASE:TTL<Index>:UPDate



.. autoclass:: RsCma.Implementations.Configure.Base.Ttl.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: