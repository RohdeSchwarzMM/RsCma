Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:CURRent
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:CURRent

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:CURRent
	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: