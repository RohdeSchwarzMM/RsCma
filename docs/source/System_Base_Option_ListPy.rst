ListPy
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:OPTion:LIST

.. code-block:: python

	SYSTem:BASE:OPTion:LIST



.. autoclass:: RsCma.Implementations.System.Base.Option.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex: