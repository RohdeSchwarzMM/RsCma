Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:CONS:IQ:CURRent
	single: READ:VSE:MEASurement<Instance>:CONS:IQ:CURRent

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:CONS:IQ:CURRent
	READ:VSE:MEASurement<Instance>:CONS:IQ:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.Cons.Iq.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: