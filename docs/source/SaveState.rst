SaveState
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: *SAV

.. code-block:: python

	*SAV



.. autoclass:: RsCma.Implementations.SaveState.SaveStateCls
	:members:
	:undoc-members:
	:noindex: