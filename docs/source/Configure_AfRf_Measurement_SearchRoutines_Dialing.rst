Dialing
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:DIALing:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:DIALing:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Dialing.DialingCls
	:members:
	:undoc-members:
	:noindex: