First
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:AOUT:FIRSt:LEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:AOUT:FIRSt:LEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.AudioOutput.First.FirstCls
	:members:
	:undoc-members:
	:noindex: