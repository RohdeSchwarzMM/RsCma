SpdifLeft
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.SpdifLeft.SpdifLeftCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.signalQuality.spdifLeft.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_SignalQuality_SpdifLeft_Average.rst
	AfRf_Measurement_MultiEval_SignalQuality_SpdifLeft_Current.rst
	AfRf_Measurement_MultiEval_SignalQuality_SpdifLeft_Deviation.rst
	AfRf_Measurement_MultiEval_SignalQuality_SpdifLeft_Extreme.rst