Demodulation
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Demodulation.DemodulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.limit.demodulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_Fdeviation.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_FmStereo.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_Pdeviation.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_Sinad.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_SndRatio.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_SnnRatio.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_SnRatio.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_ThDistortion.rst
	Configure_AfRf_Measurement_MultiEval_Limit_Demodulation_ThdNoise.rst