FilterPy
----------------------------------------





.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_FilterPy_Rf.rst