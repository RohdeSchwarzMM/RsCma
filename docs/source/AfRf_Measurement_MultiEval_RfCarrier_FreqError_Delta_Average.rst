Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.FreqError.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: