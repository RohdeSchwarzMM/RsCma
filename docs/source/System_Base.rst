Base
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:RELiability
	single: SYSTem:BASE:DID
	single: SYSTem:BASE:KLOCk
	single: SYSTem:BASE:VERSion

.. code-block:: python

	SYSTem:BASE:RELiability
	SYSTem:BASE:DID
	SYSTem:BASE:KLOCk
	SYSTem:BASE:VERSion



.. autoclass:: RsCma.Implementations.System.Base.BaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.base.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Base_Date.rst
	System_Base_Device.rst
	System_Base_Display.rst
	System_Base_Finish.rst
	System_Base_Gotsystem.rst
	System_Base_Option.rst
	System_Base_Password.rst
	System_Base_Reference.rst
	System_Base_Restart.rst
	System_Base_Shutdown.rst
	System_Base_Time.rst