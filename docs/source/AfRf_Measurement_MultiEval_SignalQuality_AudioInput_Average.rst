Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:AVERage
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:AVERage

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:AVERage
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:AIN<Nr>:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.AudioInput.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: