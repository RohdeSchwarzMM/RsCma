SpdifRight
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.SpdifRight.SpdifRightCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.signalQuality.spdifRight.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_SignalQuality_SpdifRight_Average.rst
	AfRf_Measurement_MultiEval_SignalQuality_SpdifRight_Current.rst
	AfRf_Measurement_MultiEval_SignalQuality_SpdifRight_Deviation.rst
	AfRf_Measurement_MultiEval_SignalQuality_SpdifRight_Extreme.rst