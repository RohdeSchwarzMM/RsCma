Peak
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:NXDN:FFERor:PEAK

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:NXDN:FFERor:PEAK



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Nxdn.FfError.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: