Talignment
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:IQRecorder:TALignment
	single: READ:GPRF:MEASurement<Instance>:IQRecorder:TALignment

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:IQRecorder:TALignment
	READ:GPRF:MEASurement<Instance>:IQRecorder:TALignment



.. autoclass:: RsCma.Implementations.GprfMeasurement.IqRecorder.Talignment.TalignmentCls
	:members:
	:undoc-members:
	:noindex: