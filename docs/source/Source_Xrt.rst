Xrt
----------------------------------------





.. autoclass:: RsCma.Implementations.Source.Xrt.XrtCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.xrt.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Xrt_Generator.rst