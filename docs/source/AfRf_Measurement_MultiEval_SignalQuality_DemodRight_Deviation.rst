Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:DEViation
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:DEViation

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:DEViation
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.DemodRight.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: