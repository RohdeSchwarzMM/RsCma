Power
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:LIMit:POWer

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:ACP:LIMit:POWer



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Acp.Limit.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: