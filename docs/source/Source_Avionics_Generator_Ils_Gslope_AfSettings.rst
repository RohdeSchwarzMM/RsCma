AfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:CONNector
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:ENABle
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:SDM
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:DDM
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:POFFset

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:CONNector
	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:ENABle
	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:SDM
	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:DDM
	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:POFFset



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Gslope.AfSettings.AfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.ils.gslope.afSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_Ils_Gslope_AfSettings_AudioOutput.rst
	Source_Avionics_Generator_Ils_Gslope_AfSettings_Fly.rst
	Source_Avionics_Generator_Ils_Gslope_AfSettings_Fmoddepth.rst
	Source_Avionics_Generator_Ils_Gslope_AfSettings_Frequency.rst