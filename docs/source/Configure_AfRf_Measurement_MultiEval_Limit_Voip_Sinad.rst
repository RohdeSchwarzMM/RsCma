Sinad
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:VOIP:SINad

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:VOIP:SINad



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Voip.Sinad.SinadCls
	:members:
	:undoc-members:
	:noindex: