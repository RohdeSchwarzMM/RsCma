Value
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:FWARd:VALue:ENABle

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRT:FWARd:VALue:ENABle



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.Forward.Value.ValueCls
	:members:
	:undoc-members:
	:noindex: