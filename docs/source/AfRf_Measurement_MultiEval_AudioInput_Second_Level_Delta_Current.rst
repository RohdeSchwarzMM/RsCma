Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:LEVel:DELTa:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:LEVel:DELTa:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:LEVel:DELTa:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:LEVel:DELTa:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.Second.Level.Delta.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: