Date
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:DATE

.. code-block:: python

	SYSTem:BASE:DATE



.. autoclass:: RsCma.Implementations.System.Base.Date.DateCls
	:members:
	:undoc-members:
	:noindex: