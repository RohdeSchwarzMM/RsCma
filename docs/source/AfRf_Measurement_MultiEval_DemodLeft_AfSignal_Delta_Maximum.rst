Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DELTa:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DELTa:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DELTa:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DELTa:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodLeft.AfSignal.Delta.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: