Display
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:DISPlay:TABSplit

.. code-block:: python

	CONFigure:DISPlay:TABSplit



.. autoclass:: RsCma.Implementations.Configure.Display.DisplayCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.display.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Display_Application.rst