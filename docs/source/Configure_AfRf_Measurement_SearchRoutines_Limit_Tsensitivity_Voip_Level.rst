Level
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:TSENsitivity:VOIP:LEVel

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:TSENsitivity:VOIP:LEVel



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Limit.Tsensitivity.Voip.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: