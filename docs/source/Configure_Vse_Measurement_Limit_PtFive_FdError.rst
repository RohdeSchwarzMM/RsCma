FdError
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:PTFive:FDERor

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:PTFive:FDERor



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.PtFive.FdError.FdErrorCls
	:members:
	:undoc-members:
	:noindex: