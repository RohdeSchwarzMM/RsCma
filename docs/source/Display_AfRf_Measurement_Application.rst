Application
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: DISPlay:AFRF:MEASurement<Instance>:APPLication:SELect

.. code-block:: python

	DISPlay:AFRF:MEASurement<Instance>:APPLication:SELect



.. autoclass:: RsCma.Implementations.Display.AfRf.Measurement.Application.ApplicationCls
	:members:
	:undoc-members:
	:noindex: