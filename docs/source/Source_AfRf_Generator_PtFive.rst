PtFive
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:PTFive:EMERgency
	single: SOURce:AFRF:GENerator<Instance>:PTFive:SID
	single: SOURce:AFRF:GENerator<Instance>:PTFive:TGID
	single: SOURce:AFRF:GENerator<Instance>:PTFive:NAC
	single: SOURce:AFRF:GENerator<Instance>:PTFive:PATTern
	single: SOURce:AFRF:GENerator<Instance>:PTFive:MODE

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:PTFive:EMERgency
	SOURce:AFRF:GENerator<Instance>:PTFive:SID
	SOURce:AFRF:GENerator<Instance>:PTFive:TGID
	SOURce:AFRF:GENerator<Instance>:PTFive:NAC
	SOURce:AFRF:GENerator<Instance>:PTFive:PATTern
	SOURce:AFRF:GENerator<Instance>:PTFive:MODE



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.PtFive.PtFiveCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.ptFive.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_PtFive_CfFm.rst