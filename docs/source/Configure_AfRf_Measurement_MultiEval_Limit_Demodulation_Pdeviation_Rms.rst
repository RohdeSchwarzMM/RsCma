Rms
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:DEModulation:PDEViation:RMS

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:DEModulation:PDEViation:RMS



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Demodulation.Pdeviation.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: