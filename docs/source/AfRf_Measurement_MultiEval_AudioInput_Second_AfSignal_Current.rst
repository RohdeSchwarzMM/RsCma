Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:AFSignal:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:AFSignal:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:AFSignal:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:AFSignal:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.Second.AfSignal.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: