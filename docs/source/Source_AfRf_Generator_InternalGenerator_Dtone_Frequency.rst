Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:DTONe:FREQuency

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:DTONe:FREQuency



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.Dtone.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: