RobustAuto
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:ROBustauto

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:ROBustauto



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.FilterPy.RobustAuto.RobustAutoCls
	:members:
	:undoc-members:
	:noindex: