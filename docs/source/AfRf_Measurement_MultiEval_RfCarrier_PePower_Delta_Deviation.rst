Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.PePower.Delta.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: