Voip
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:ENABle
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:THReshold
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:MODE
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:SLOPe
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:OFFSet
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:STATe
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:COUPling

.. code-block:: python

	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:ENABle
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:THReshold
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:MODE
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:SLOPe
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:OFFSet
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:STATe
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:VOIP:COUPling



.. autoclass:: RsCma.Implementations.Trigger.AfRf.Measurement.MultiEval.Oscilloscope.Voip.VoipCls
	:members:
	:undoc-members:
	:noindex: