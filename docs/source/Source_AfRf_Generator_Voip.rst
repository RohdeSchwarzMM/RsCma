Voip
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:VOIP:ENABle
	single: SOURce:AFRF:GENerator<Instance>:VOIP:PCODec
	single: SOURce:AFRF:GENerator<Instance>:VOIP:LEVel
	single: SOURce:AFRF:GENerator<Instance>:VOIP:AUDio
	single: SOURce:AFRF:GENerator<Instance>:VOIP:FID
	single: SOURce:AFRF:GENerator<Instance>:VOIP:FREQuency
	single: SOURce:AFRF:GENerator<Instance>:VOIP

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:VOIP:ENABle
	SOURce:AFRF:GENerator<Instance>:VOIP:PCODec
	SOURce:AFRF:GENerator<Instance>:VOIP:LEVel
	SOURce:AFRF:GENerator<Instance>:VOIP:AUDio
	SOURce:AFRF:GENerator<Instance>:VOIP:FID
	SOURce:AFRF:GENerator<Instance>:VOIP:FREQuency
	SOURce:AFRF:GENerator<Instance>:VOIP



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Voip.VoipCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.voip.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Voip_AtmFrequency.rst
	Source_AfRf_Generator_Voip_Ptt.rst
	Source_AfRf_Generator_Voip_Sip.rst
	Source_AfRf_Generator_Voip_Uri.rst