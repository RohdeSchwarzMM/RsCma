Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:PDEViation:TRACe
	single: READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:PDEViation:TRACe

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:PDEViation:TRACe
	READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:PDEViation:TRACe



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Tsensitivity.Pdeviation.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: