Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.UsbPower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: