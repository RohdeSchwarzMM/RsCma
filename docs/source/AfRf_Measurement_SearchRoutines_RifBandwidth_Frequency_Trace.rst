Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:FREQuency:TRACe
	single: READ:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:FREQuency:TRACe

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:FREQuency:TRACe
	READ:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:FREQuency:TRACe



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.RifBandwidth.Frequency.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: