Notch<Notch>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr3
	rc = driver.configure.afRf.measurement.spdif.filterPy.notch.repcap_notch_get()
	driver.configure.afRf.measurement.spdif.filterPy.notch.repcap_notch_set(repcap.Notch.Nr1)





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.FilterPy.Notch.NotchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.spdif.filterPy.notch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Spdif_FilterPy_Notch_Enable.rst
	Configure_AfRf_Measurement_Spdif_FilterPy_Notch_Frequency.rst