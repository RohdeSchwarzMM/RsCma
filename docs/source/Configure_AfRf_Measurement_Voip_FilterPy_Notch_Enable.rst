Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:NOTCh<Num>:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:NOTCh<Num>:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.FilterPy.Notch.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: