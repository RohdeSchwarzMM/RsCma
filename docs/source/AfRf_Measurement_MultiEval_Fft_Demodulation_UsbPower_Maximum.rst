Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.UsbPower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: