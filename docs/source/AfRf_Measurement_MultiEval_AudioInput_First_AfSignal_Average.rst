Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.First.AfSignal.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: