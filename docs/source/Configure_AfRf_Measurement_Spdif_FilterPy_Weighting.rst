Weighting
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:WEIGhting

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:WEIGhting



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.FilterPy.Weighting.WeightingCls
	:members:
	:undoc-members:
	:noindex: