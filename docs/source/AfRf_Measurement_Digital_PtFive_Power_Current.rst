Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:PTFive:POWer:CURRent
	single: READ:AFRF:MEASurement<Instance>:DIGital:PTFive:POWer:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:PTFive:POWer:CURRent
	READ:AFRF:MEASurement<Instance>:DIGital:PTFive:POWer:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.PtFive.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: