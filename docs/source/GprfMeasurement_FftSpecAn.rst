FftSpecAn
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:FFTSanalyzer
	single: STOP:GPRF:MEASurement<Instance>:FFTSanalyzer
	single: ABORt:GPRF:MEASurement<Instance>:FFTSanalyzer

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:FFTSanalyzer
	STOP:GPRF:MEASurement<Instance>:FFTSanalyzer
	ABORt:GPRF:MEASurement<Instance>:FFTSanalyzer



.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.FftSpecAnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.fftSpecAn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_FftSpecAn_Icomponent.rst
	GprfMeasurement_FftSpecAn_Marker.rst
	GprfMeasurement_FftSpecAn_Peaks.rst
	GprfMeasurement_FftSpecAn_Power.rst
	GprfMeasurement_FftSpecAn_Qcomponent.rst
	GprfMeasurement_FftSpecAn_State.rst
	GprfMeasurement_FftSpecAn_Tdomain.rst