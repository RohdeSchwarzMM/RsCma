Placement
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:MARKer<nr>:PLACement

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:MARKer<nr>:PLACement



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.ZeroSpan.Marker.Placement.PlacementCls
	:members:
	:undoc-members:
	:noindex: