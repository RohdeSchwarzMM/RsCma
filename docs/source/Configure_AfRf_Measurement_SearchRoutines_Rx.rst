Rx
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:RX:SETime
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:RX:MODE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:RX:SETime
	CONFigure:AFRF:MEASurement<Instance>:SROutines:RX:MODE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Rx.RxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.searchRoutines.rx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_SearchRoutines_Rx_AmPoints.rst