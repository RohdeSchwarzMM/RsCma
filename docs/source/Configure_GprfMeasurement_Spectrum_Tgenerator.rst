Tgenerator
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:TGENerator:ENABle
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:TGENerator:NORMalize

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:TGENerator:ENABle
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:TGENerator:NORMalize



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.Tgenerator.TgeneratorCls
	:members:
	:undoc-members:
	:noindex: