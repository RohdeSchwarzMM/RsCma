AtmFrequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:VOIP:ATMFrequency:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:VOIP:ATMFrequency:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Voip.AtmFrequency.AtmFrequencyCls
	:members:
	:undoc-members:
	:noindex: