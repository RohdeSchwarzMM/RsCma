Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:GENerator<Instance>:VOIP:FREQuency:ATMFrequency

.. code-block:: python

	CONFigure:AFRF:GENerator<Instance>:VOIP:FREQuency:ATMFrequency



.. autoclass:: RsCma.Implementations.Configure.AfRf.Generator.Voip.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: