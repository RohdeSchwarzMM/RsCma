Dwidth
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:DWIDth

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:DWIDth



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.FilterPy.Dwidth.DwidthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.spdif.filterPy.dwidth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Spdif_FilterPy_Dwidth_Sfactor.rst