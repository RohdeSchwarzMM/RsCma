Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:TETRa:BERate:CURRent
	single: CALCulate:AFRF:MEASurement<Instance>:DIGital:TETRa:BERate:CURRent
	single: READ:AFRF:MEASurement<Instance>:DIGital:TETRa:BERate:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:TETRa:BERate:CURRent
	CALCulate:AFRF:MEASurement<Instance>:DIGital:TETRa:BERate:CURRent
	READ:AFRF:MEASurement<Instance>:DIGital:TETRa:BERate:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Tetra.BitErrorRate.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: