ExtPwrSensor
----------------------------------------





.. autoclass:: RsCma.Implementations.Display.GprfMeasurement.ExtPwrSensor.ExtPwrSensorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.gprfMeasurement.extPwrSensor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_GprfMeasurement_ExtPwrSensor_Application.rst