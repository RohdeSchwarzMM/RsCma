Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:AOUT:LEVel:TRACe
	single: READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:AOUT:LEVel:TRACe

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:AOUT:LEVel:TRACe
	READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:AOUT:LEVel:TRACe



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Tsensitivity.AudioOutput.Level.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: