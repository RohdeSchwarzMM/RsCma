Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:FREQuency<nr>:ENABle

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:FREQuency<nr>:ENABle



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Gslope.AfSettings.Frequency.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: