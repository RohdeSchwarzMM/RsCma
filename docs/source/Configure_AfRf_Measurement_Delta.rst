Delta
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DELTa:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DELTa:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex: