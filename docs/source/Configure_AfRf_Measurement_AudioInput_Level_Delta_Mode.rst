Mode
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:LEVel:DELTa:MODE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:LEVel:DELTa:MODE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Level.Delta.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: