Status
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:HELP:STATus:BITS
	single: SYSTem:HELP:STATus[:REGister]

.. code-block:: python

	SYSTem:HELP:STATus:BITS
	SYSTem:HELP:STATus[:REGister]



.. autoclass:: RsCma.Implementations.System.Help.Status.StatusCls
	:members:
	:undoc-members:
	:noindex: