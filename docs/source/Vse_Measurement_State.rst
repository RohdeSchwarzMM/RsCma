State
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:STATe

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:STATe



.. autoclass:: RsCma.Implementations.Vse.Measurement.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.vse.measurement.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Vse_Measurement_State_All.rst