Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.AudioInput.Power.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: