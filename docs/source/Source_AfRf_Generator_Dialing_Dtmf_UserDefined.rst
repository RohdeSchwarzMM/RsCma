UserDefined
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:UDEFined:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:UDEFined:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.Dtmf.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: