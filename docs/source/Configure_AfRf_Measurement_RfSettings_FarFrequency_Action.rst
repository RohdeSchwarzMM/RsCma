Action
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:RFSettings:FARFrequency:ACTion

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:RFSettings:FARFrequency:ACTion



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.RfSettings.FarFrequency.Action.ActionCls
	:members:
	:undoc-members:
	:noindex: