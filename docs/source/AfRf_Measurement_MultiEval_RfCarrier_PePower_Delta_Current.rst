Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.PePower.Delta.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: