SymbolRate
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:IQRecorder:SRATe

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:IQRecorder:SRATe



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.IqRecorder.SymbolRate.SymbolRateCls
	:members:
	:undoc-members:
	:noindex: