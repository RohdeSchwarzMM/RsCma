Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:SSNR:CURRent
	single: READ:AFRF:MEASurement<Instance>:SROutines:SSNR:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:SSNR:CURRent
	READ:AFRF:MEASurement<Instance>:SROutines:SSNR:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Ssnr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: