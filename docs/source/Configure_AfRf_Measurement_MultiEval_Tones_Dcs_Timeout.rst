Timeout
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:TOUT

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:TOUT



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Dcs.Timeout.TimeoutCls
	:members:
	:undoc-members:
	:noindex: