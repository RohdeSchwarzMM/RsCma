Gcoupling
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:GCOupling

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:GCOupling



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Gcoupling.GcouplingCls
	:members:
	:undoc-members:
	:noindex: