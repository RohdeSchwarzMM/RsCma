Condition
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: STATus:OPERation:BIT<bitno>:CONDition

.. code-block:: python

	STATus:OPERation:BIT<bitno>:CONDition



.. autoclass:: RsCma.Implementations.Status.Operation.Bit.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex: