Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:FREQuency:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:FREQuency:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:FREQuency:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:FREQuency:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifLeft.Frequency.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: