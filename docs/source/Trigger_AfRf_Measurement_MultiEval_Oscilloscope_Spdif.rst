Spdif
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:SOURce
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:COUPling
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:STATe
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:ENABle
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:OFFSet
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:SLOPe
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:MODE
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:THReshold

.. code-block:: python

	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:SOURce
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:COUPling
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:STATe
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:ENABle
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:OFFSet
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:SLOPe
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:MODE
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SIN:THReshold



.. autoclass:: RsCma.Implementations.Trigger.AfRf.Measurement.MultiEval.Oscilloscope.Spdif.SpdifCls
	:members:
	:undoc-members:
	:noindex: