Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:MINimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:MINimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:MINimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:MINimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Maximum.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: