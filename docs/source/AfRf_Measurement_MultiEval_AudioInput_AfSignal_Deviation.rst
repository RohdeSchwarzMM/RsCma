Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.AfSignal.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: