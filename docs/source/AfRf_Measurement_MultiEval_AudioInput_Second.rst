Second
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.Second.SecondCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.audioInput.second.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_AudioInput_Second_AfSignal.rst
	AfRf_Measurement_MultiEval_AudioInput_Second_Level.rst