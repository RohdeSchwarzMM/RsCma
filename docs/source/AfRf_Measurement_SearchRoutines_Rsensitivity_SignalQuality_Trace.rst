Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:SQUality:TRACe
	single: READ:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:SQUality:TRACe

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:SQUality:TRACe
	READ:AFRF:MEASurement<Instance>:SROutines:RSENsitivity:SQUality:TRACe



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsensitivity.SignalQuality.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: