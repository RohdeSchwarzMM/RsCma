PiDeviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:DEModulation:FMSTereo:PIDeviation

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:DEModulation:FMSTereo:PIDeviation



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Demodulation.FmStereo.PiDeviation.PiDeviationCls
	:members:
	:undoc-members:
	:noindex: