User
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FREQuency:DELTa:USER

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FREQuency:DELTa:USER



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Frequency.Delta.User.UserCls
	:members:
	:undoc-members:
	:noindex: