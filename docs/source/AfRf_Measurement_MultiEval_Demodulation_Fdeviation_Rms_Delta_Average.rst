Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:RMS:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:RMS:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:RMS:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:RMS:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Fdeviation.Rms.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: