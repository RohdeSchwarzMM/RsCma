FilterPy
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:ENABle
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:WEIGhting
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:DEEMphasis
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:ROBustauto
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:LPASs
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:HPASs

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:ENABle
	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:WEIGhting
	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:DEEMphasis
	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:ROBustauto
	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:LPASs
	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:HPASs



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.demodulation.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Demodulation_FilterPy_Bpass.rst
	Configure_AfRf_Measurement_Demodulation_FilterPy_Dfrequency.rst
	Configure_AfRf_Measurement_Demodulation_FilterPy_Dwidth.rst
	Configure_AfRf_Measurement_Demodulation_FilterPy_Notch.rst