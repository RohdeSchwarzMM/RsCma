Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:BERate:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.BitErrorRate.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: