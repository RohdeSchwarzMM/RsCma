State
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:XRT:GENerator<Instance>:STATe
	single: SOURce:XRT:GENerator<Instance>:STATe:ALL

.. code-block:: python

	SOURce:XRT:GENerator<Instance>:STATe
	SOURce:XRT:GENerator<Instance>:STATe:ALL



.. autoclass:: RsCma.Implementations.Source.Xrt.Generator.State.StateCls
	:members:
	:undoc-members:
	:noindex: