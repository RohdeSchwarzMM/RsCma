Level
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:VOIP:LEVel
	single: CALCulate:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:VOIP:LEVel
	single: READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:VOIP:LEVel

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:VOIP:LEVel
	CALCulate:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:VOIP:LEVel
	READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:VOIP:LEVel



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Tsensitivity.Voip.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.searchRoutines.tsensitivity.voip.level.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_SearchRoutines_Tsensitivity_Voip_Level_Trace.rst