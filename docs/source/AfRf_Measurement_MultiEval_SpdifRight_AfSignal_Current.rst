Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifRight.AfSignal.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: