Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:PEAK:DELTa:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:PEAK:DELTa:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:PEAK:DELTa:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FDEViation:PEAK:DELTa:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Fdeviation.Peak.Delta.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: