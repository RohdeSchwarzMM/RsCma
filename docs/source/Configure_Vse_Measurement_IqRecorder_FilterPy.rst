FilterPy
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.IqRecorder.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.iqRecorder.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_IqRecorder_FilterPy_Bandpass.rst
	Configure_Vse_Measurement_IqRecorder_FilterPy_Gauss.rst
	Configure_Vse_Measurement_IqRecorder_FilterPy_TypePy.rst