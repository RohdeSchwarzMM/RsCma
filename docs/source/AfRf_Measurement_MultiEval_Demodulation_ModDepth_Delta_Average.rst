Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.ModDepth.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: