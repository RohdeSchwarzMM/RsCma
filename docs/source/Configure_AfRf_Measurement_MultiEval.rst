MultiEval
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:CREPetition
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:REPetition
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:RCOupling
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:SAUTomatic
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:STFind

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:CREPetition
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:REPetition
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:RCOupling
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:SAUTomatic
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:STFind



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Af.rst
	Configure_AfRf_Measurement_MultiEval_AfFft.rst
	Configure_AfRf_Measurement_MultiEval_AudioInput.rst
	Configure_AfRf_Measurement_MultiEval_Demodulation.rst
	Configure_AfRf_Measurement_MultiEval_Fft.rst
	Configure_AfRf_Measurement_MultiEval_FilterPy.rst
	Configure_AfRf_Measurement_MultiEval_Limit.rst
	Configure_AfRf_Measurement_MultiEval_Oscilloscope.rst
	Configure_AfRf_Measurement_MultiEval_Result.rst
	Configure_AfRf_Measurement_MultiEval_Rf.rst
	Configure_AfRf_Measurement_MultiEval_SpdifLeft.rst
	Configure_AfRf_Measurement_MultiEval_SpdifRight.rst
	Configure_AfRf_Measurement_MultiEval_Tones.rst