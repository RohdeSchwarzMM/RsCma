Sinfo
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:TETRa:SINFo
	single: READ:AFRF:MEASurement<Instance>:DIGital:TETRa:SINFo

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:TETRa:SINFo
	READ:AFRF:MEASurement<Instance>:DIGital:TETRa:SINFo



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Tetra.Sinfo.SinfoCls
	:members:
	:undoc-members:
	:noindex: