FilterPy
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:NXDN:FILTer

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:NXDN:FILTer



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Nxdn.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.nxdn.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Nxdn_FilterPy_Rrc.rst