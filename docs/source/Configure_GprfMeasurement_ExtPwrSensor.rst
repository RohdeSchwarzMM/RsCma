ExtPwrSensor
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:TOUT
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:RESolution
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:SCOunt
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:REPetition
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:RCOupling
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:FREQuency

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:EPSensor:TOUT
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:RESolution
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:SCOunt
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:REPetition
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:RCOupling
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:FREQuency



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.ExtPwrSensor.ExtPwrSensorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.extPwrSensor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_ExtPwrSensor_Attenuation.rst
	Configure_GprfMeasurement_ExtPwrSensor_Average.rst