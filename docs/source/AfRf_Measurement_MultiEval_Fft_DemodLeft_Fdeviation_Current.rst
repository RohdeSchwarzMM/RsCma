Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.DemodLeft.Fdeviation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: