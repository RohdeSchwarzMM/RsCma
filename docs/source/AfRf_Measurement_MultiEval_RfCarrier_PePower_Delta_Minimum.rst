Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.PePower.Delta.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: