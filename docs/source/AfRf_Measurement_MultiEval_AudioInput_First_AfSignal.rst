AfSignal
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.First.AfSignal.AfSignalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.audioInput.first.afSignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_AudioInput_First_AfSignal_Average.rst
	AfRf_Measurement_MultiEval_AudioInput_First_AfSignal_Current.rst
	AfRf_Measurement_MultiEval_AudioInput_First_AfSignal_Deviation.rst
	AfRf_Measurement_MultiEval_AudioInput_First_AfSignal_Maximum.rst