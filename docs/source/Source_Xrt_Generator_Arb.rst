Arb
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:XRT:GENerator<Instance>:ARB:CRATe
	single: SOURce:XRT:GENerator<Instance>:ARB:CRCProtect
	single: SOURce:XRT:GENerator<Instance>:ARB:FOFFset
	single: SOURce:XRT:GENerator<Instance>:ARB:LOFFset
	single: SOURce:XRT:GENerator<Instance>:ARB:POFFset
	single: SOURce:XRT:GENerator<Instance>:ARB:REPetition

.. code-block:: python

	SOURce:XRT:GENerator<Instance>:ARB:CRATe
	SOURce:XRT:GENerator<Instance>:ARB:CRCProtect
	SOURce:XRT:GENerator<Instance>:ARB:FOFFset
	SOURce:XRT:GENerator<Instance>:ARB:LOFFset
	SOURce:XRT:GENerator<Instance>:ARB:POFFset
	SOURce:XRT:GENerator<Instance>:ARB:REPetition



.. autoclass:: RsCma.Implementations.Source.Xrt.Generator.Arb.ArbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.xrt.generator.arb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Xrt_Generator_Arb_File.rst
	Source_Xrt_Generator_Arb_Samples.rst