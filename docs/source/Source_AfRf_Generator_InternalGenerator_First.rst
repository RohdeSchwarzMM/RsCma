First
----------------------------------------





.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.First.FirstCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.internalGenerator.first.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_InternalGenerator_First_MultiTone.rst