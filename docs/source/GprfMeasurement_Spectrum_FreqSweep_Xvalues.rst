Xvalues
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:XVALues

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:XVALues



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.FreqSweep.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: