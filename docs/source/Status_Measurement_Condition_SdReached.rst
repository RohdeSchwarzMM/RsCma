SdReached
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: STATus:MEASurement:CONDition:SDReached

.. code-block:: python

	STATus:MEASurement:CONDition:SDReached



.. autoclass:: RsCma.Implementations.Status.Measurement.Condition.SdReached.SdReachedCls
	:members:
	:undoc-members:
	:noindex: