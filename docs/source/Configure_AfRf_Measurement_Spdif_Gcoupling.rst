Gcoupling
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:GCOupling

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:GCOupling



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.Gcoupling.GcouplingCls
	:members:
	:undoc-members:
	:noindex: