Absolute
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:MARKer<nr>:ABSolute

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:MARKer<nr>:ABSolute



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.ZeroSpan.Marker.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex: