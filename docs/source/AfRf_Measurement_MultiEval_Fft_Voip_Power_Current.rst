Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Voip.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: