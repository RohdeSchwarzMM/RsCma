Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:AFSignal:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.First.AfSignal.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: