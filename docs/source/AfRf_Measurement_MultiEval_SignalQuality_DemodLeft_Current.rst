Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:CURRent
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:CURRent

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:CURRent
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.DemodLeft.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: