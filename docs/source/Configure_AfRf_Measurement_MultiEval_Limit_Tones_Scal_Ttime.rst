Ttime
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:TONes:SCAL:TTIMe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:TONes:SCAL:TTIMe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Tones.Scal.Ttime.TtimeCls
	:members:
	:undoc-members:
	:noindex: