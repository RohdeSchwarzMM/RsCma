Fly
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:FLY:IDIRection
	single: SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:FLY

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:FLY:IDIRection
	SOURce:AVIonics:GENerator<Instance>:ILS:GSLope:AFSettings:FLY



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Gslope.AfSettings.Fly.FlyCls
	:members:
	:undoc-members:
	:noindex: