ThDistortion
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:AIN<Nr>:THDistortion

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:AIN<Nr>:THDistortion



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.AudioInput.ThDistortion.ThDistortionCls
	:members:
	:undoc-members:
	:noindex: