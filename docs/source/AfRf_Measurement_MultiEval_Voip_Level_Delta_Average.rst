Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:LEVel:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:LEVel:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:LEVel:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:LEVel:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Voip.Level.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: