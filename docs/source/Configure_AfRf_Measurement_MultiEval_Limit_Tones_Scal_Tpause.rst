Tpause
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:TONes:SCAL:TPAuse

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:TONes:SCAL:TPAuse



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Tones.Scal.Tpause.TpauseCls
	:members:
	:undoc-members:
	:noindex: