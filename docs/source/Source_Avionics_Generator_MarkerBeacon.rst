MarkerBeacon
----------------------------------------





.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.MarkerBeacon.MarkerBeaconCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.markerBeacon.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_MarkerBeacon_AfSettings.rst
	Source_Avionics_Generator_MarkerBeacon_IdSignal.rst
	Source_Avionics_Generator_MarkerBeacon_RfSettings.rst
	Source_Avionics_Generator_MarkerBeacon_State.rst