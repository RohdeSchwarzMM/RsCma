Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:PEPower:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.PePower.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: