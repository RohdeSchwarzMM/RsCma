Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINRight:LEVel:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINRight:LEVel:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINRight:LEVel:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:SINRight:LEVel:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifRight.Level.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: