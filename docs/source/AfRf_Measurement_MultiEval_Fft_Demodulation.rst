Demodulation<Channel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.afRf.measurement.multiEval.fft.demodulation.repcap_channel_get()
	driver.afRf.measurement.multiEval.fft.demodulation.repcap_channel_set(repcap.Channel.Nr1)





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.DemodulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.fft.demodulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Fft_Demodulation_LsbPower.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation_Marker.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation_ModDepth.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation_Pdeviation.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation_UsbPower.rst