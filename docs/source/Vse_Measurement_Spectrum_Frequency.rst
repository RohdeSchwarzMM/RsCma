Frequency
----------------------------------------





.. autoclass:: RsCma.Implementations.Vse.Measurement.Spectrum.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.vse.measurement.spectrum.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Vse_Measurement_Spectrum_Frequency_Start.rst
	Vse_Measurement_Spectrum_Frequency_Stop.rst