TocLength
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:TOCLength
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:TOCLength

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:TOCLength
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:TOCLength



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.TocLength.TocLengthCls
	:members:
	:undoc-members:
	:noindex: