ToStart
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DIALing:TOSTart

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DIALing:TOSTart



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Dialing.ToStart.ToStartCls
	:members:
	:undoc-members:
	:noindex: