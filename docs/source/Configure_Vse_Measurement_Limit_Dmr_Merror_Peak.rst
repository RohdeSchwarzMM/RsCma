Peak
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:DMR:MERRor:PEAK

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:DMR:MERRor:PEAK



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Dmr.Merror.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: