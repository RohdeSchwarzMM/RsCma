Placement
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:FFT:MARKer<mnr>:PLACement

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:FFT:MARKer<mnr>:PLACement



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Fft.Marker.Placement.PlacementCls
	:members:
	:undoc-members:
	:noindex: