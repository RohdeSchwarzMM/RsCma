Vbw
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:VBW:AUTO
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:VBW

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:VBW:AUTO
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:VBW



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.ZeroSpan.Vbw.VbwCls
	:members:
	:undoc-members:
	:noindex: