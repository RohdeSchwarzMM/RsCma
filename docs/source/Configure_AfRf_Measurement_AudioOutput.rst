AudioOutput<AudioOutput>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.afRf.measurement.audioOutput.repcap_audioOutput_get()
	driver.configure.afRf.measurement.audioOutput.repcap_audioOutput_set(repcap.AudioOutput.Nr1)





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioOutput.AudioOutputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.audioOutput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_AudioOutput_Enable.rst
	Configure_AfRf_Measurement_AudioOutput_First.rst
	Configure_AfRf_Measurement_AudioOutput_Level.rst
	Configure_AfRf_Measurement_AudioOutput_Second.rst
	Configure_AfRf_Measurement_AudioOutput_Source.rst