Result
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:RESult:BER

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:RESult:BER



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: