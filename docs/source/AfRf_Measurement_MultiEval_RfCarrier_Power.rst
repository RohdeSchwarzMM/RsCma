Power
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.rfCarrier.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_RfCarrier_Power_Delta.rst