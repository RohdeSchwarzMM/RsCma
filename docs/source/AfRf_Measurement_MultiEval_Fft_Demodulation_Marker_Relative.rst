Relative
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation<nr>:MARKer<mnr>:RELative

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation<nr>:MARKer<mnr>:RELative



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.Marker.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: