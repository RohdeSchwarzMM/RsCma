Zbox
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:ZBOX:ENABle
	single: CONFigure:BASE:ZBOX:IMPedance

.. code-block:: python

	CONFigure:BASE:ZBOX:ENABle
	CONFigure:BASE:ZBOX:IMPedance



.. autoclass:: RsCma.Implementations.Configure.Base.Zbox.ZboxCls
	:members:
	:undoc-members:
	:noindex: