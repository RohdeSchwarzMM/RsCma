Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.SpdifRight.Power.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: