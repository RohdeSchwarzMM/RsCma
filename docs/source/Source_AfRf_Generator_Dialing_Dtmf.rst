Dtmf
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:SEQuence
	single: SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:SREPeat
	single: SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:SPAuse
	single: SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:DTIMe
	single: SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:DPAuse

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:SEQuence
	SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:SREPeat
	SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:SPAuse
	SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:DTIMe
	SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:DPAuse



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.Dtmf.DtmfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.dialing.dtmf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Dialing_Dtmf_Frequency.rst
	Source_AfRf_Generator_Dialing_Dtmf_UserDefined.rst