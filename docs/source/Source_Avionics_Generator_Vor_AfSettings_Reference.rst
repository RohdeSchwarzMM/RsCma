Reference
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:REFerence:ENABle
	single: SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:REFerence:MDEPth
	single: SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:REFerence:CFRequency
	single: SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:REFerence:FREQuency

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:REFerence:ENABle
	SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:REFerence:MDEPth
	SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:REFerence:CFRequency
	SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:REFerence:FREQuency



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Vor.AfSettings.Reference.ReferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.vor.afSettings.reference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_Vor_AfSettings_Reference_Fdeviation.rst