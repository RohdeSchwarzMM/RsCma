Sequence
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>:SEQuence
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>:SEQuence

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>:SEQuence
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>:SEQuence



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.AudioInput.Sequence.SequenceCls
	:members:
	:undoc-members:
	:noindex: