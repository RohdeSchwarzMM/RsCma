Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:NOTCh<Num>:FREQuency

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:NOTCh<Num>:FREQuency



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.FilterPy.Notch.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: