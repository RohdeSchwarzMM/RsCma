Fdeviation
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.DemodRight.Fdeviation.FdeviationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.fft.demodRight.fdeviation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Fft_DemodRight_Fdeviation_Average.rst
	AfRf_Measurement_MultiEval_Fft_DemodRight_Fdeviation_Current.rst
	AfRf_Measurement_MultiEval_Fft_DemodRight_Fdeviation_Maximum.rst
	AfRf_Measurement_MultiEval_Fft_DemodRight_Fdeviation_Minimum.rst