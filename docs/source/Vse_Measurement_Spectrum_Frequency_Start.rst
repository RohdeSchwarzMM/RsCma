Start
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:SPECtrum:FREQuency:STARt

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:SPECtrum:FREQuency:STARt



.. autoclass:: RsCma.Implementations.Vse.Measurement.Spectrum.Frequency.Start.StartCls
	:members:
	:undoc-members:
	:noindex: