Zero
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALibration:GPRF:MEASurement<Instance>:NRT:ZERO

.. code-block:: python

	CALibration:GPRF:MEASurement<Instance>:NRT:ZERO



.. autoclass:: RsCma.Implementations.Calibration.GprfMeasurement.Nrt.Zero.ZeroCls
	:members:
	:undoc-members:
	:noindex: