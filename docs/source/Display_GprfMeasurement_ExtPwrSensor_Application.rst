Application
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: DISPlay:GPRF:MEASurement<Instance>:EPSensor:APPLication:SELect

.. code-block:: python

	DISPlay:GPRF:MEASurement<Instance>:EPSensor:APPLication:SELect



.. autoclass:: RsCma.Implementations.Display.GprfMeasurement.ExtPwrSensor.Application.ApplicationCls
	:members:
	:undoc-members:
	:noindex: