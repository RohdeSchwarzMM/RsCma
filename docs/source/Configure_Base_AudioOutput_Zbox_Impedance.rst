Impedance
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:AOUT<nr>:ZBOX:IMPedance

.. code-block:: python

	CONFigure:BASE:AOUT<nr>:ZBOX:IMPedance



.. autoclass:: RsCma.Implementations.Configure.Base.AudioOutput.Zbox.Impedance.ImpedanceCls
	:members:
	:undoc-members:
	:noindex: