Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MAXimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MAXimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MAXimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Minimum.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: