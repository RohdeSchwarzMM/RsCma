Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: