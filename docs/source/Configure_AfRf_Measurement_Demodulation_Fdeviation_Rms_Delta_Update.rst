Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FDEViation:RMS:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FDEViation:RMS:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.Fdeviation.Rms.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: