Power
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.SpdifLeft.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.fft.spdifLeft.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Fft_SpdifLeft_Power_Average.rst
	AfRf_Measurement_MultiEval_Fft_SpdifLeft_Power_Current.rst
	AfRf_Measurement_MultiEval_Fft_SpdifLeft_Power_Maximum.rst
	AfRf_Measurement_MultiEval_Fft_SpdifLeft_Power_Minimum.rst