GprfMeasurement
----------------------------------------





.. autoclass:: RsCma.Implementations.Display.GprfMeasurement.GprfMeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.gprfMeasurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_GprfMeasurement_Acp.rst
	Display_GprfMeasurement_ExtPwrSensor.rst
	Display_GprfMeasurement_FftSpecAn.rst
	Display_GprfMeasurement_Spectrum.rst