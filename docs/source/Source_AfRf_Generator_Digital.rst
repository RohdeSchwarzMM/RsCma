Digital
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIGital:FILE

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIGital:FILE



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Digital.DigitalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.digital.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Digital_Rf.rst