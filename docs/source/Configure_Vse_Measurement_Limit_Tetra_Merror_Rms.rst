Rms
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:TETRa:MERRor:RMS

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:TETRa:MERRor:RMS



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Tetra.Merror.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: