Samples
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:XRT:GENerator<Instance>:ARB:SAMPles

.. code-block:: python

	SOURce:XRT:GENerator<Instance>:ARB:SAMPles



.. autoclass:: RsCma.Implementations.Source.Xrt.Generator.Arb.Samples.SamplesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.xrt.generator.arb.samples.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Xrt_Generator_Arb_Samples_Range.rst