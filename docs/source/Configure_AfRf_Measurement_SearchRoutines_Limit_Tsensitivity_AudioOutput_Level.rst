Level
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:TSENsitivity:AOUT:LEVel

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:TSENsitivity:AOUT:LEVel



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Limit.Tsensitivity.AudioOutput.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: