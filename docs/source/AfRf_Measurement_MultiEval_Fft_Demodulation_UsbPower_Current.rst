Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.UsbPower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: