Vphase
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:VPHase:ENABle
	single: SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:VPHase:MDEPth
	single: SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:VPHase:BANGle
	single: SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:VPHase:DIRection

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:VPHase:ENABle
	SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:VPHase:MDEPth
	SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:VPHase:BANGle
	SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:VPHase:DIRection



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Vor.AfSettings.Vphase.VphaseCls
	:members:
	:undoc-members:
	:noindex: