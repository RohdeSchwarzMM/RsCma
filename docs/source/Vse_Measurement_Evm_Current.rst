Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:EVM:CURRent
	single: READ:VSE:MEASurement<Instance>:EVM:CURRent
	single: CALCulate:VSE:MEASurement<Instance>:EVM:CURRent

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:EVM:CURRent
	READ:VSE:MEASurement<Instance>:EVM:CURRent
	CALCulate:VSE:MEASurement<Instance>:EVM:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.Evm.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: