Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:CURRent
	single: READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:CURRent
	READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:CURRent



.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: