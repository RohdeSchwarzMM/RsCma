Cdefinition
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:CDEFinition:RCHannel
	single: CONFigure:AFRF:MEASurement<Instance>:CDEFinition:RFRequency
	single: CONFigure:AFRF:MEASurement<Instance>:CDEFinition:CSPace
	single: CONFigure:AFRF:MEASurement<Instance>:CDEFinition

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:CDEFinition:RCHannel
	CONFigure:AFRF:MEASurement<Instance>:CDEFinition:RFRequency
	CONFigure:AFRF:MEASurement<Instance>:CDEFinition:CSPace
	CONFigure:AFRF:MEASurement<Instance>:CDEFinition



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Cdefinition.CdefinitionCls
	:members:
	:undoc-members:
	:noindex: