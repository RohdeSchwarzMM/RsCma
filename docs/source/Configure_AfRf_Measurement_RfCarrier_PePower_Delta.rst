Delta
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:RFCarrier:PEPower:DELTa:MEASured
	single: CONFigure:AFRF:MEASurement<Instance>:RFCarrier:PEPower:DELTa:USER
	single: CONFigure:AFRF:MEASurement<Instance>:RFCarrier:PEPower:DELTa:MODE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:RFCarrier:PEPower:DELTa:MEASured
	CONFigure:AFRF:MEASurement<Instance>:RFCarrier:PEPower:DELTa:USER
	CONFigure:AFRF:MEASurement<Instance>:RFCarrier:PEPower:DELTa:MODE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.RfCarrier.PePower.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.rfCarrier.pePower.delta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_RfCarrier_PePower_Delta_Update.rst