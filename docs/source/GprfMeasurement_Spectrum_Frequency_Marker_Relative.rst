Relative
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:MARKer<nr>:RELative

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:MARKer<nr>:RELative



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Frequency.Marker.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: