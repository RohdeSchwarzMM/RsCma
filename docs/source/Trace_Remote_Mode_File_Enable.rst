Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRACe:REMote:MODE:FILE<instrument>:ENABle

.. code-block:: python

	TRACe:REMote:MODE:FILE<instrument>:ENABle



.. autoclass:: RsCma.Implementations.Trace.Remote.Mode.File.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: