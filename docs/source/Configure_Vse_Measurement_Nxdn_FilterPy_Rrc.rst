Rrc
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:NXDN:FILTer:RRC:ROFFfactor

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:NXDN:FILTer:RRC:ROFFfactor



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Nxdn.FilterPy.Rrc.RrcCls
	:members:
	:undoc-members:
	:noindex: