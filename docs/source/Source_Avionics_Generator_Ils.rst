Ils
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:FPAirment
	single: SOURce:AVIonics:GENerator<Instance>:ILS

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:FPAirment
	SOURce:AVIonics:GENerator<Instance>:ILS



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.IlsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.ils.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_Ils_Gslope.rst
	Source_Avionics_Generator_Ils_Localizer.rst
	Source_Avionics_Generator_Ils_State.rst