Capture
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:IQRecorder:CAPTure

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:IQRecorder:CAPTure



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.IqRecorder.Capture.CaptureCls
	:members:
	:undoc-members:
	:noindex: