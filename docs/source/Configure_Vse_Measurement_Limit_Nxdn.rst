Nxdn
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Nxdn.NxdnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.limit.nxdn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Limit_Nxdn_FdError.rst
	Configure_Vse_Measurement_Limit_Nxdn_FfError.rst
	Configure_Vse_Measurement_Limit_Nxdn_Merror.rst