RfCarrier
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.RfCarrier.RfCarrierCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.limit.rfCarrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Limit_RfCarrier_FreqError.rst
	Configure_Vse_Measurement_Limit_RfCarrier_Power.rst