State
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:STATe



.. autoclass:: RsCma.Implementations.GprfMeasurement.Power.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.power.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Power_State_All.rst