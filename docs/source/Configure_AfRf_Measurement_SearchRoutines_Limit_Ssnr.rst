Ssnr
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:SSNR

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:SSNR



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Limit.Ssnr.SsnrCls
	:members:
	:undoc-members:
	:noindex: