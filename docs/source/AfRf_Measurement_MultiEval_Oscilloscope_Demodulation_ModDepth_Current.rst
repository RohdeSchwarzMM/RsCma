Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:MDEPth:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:MDEPth:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:MDEPth:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:MDEPth:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.Demodulation.ModDepth.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: