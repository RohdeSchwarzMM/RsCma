Repetitions
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft:REPetitions
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft:REPetitions

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft:REPetitions
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft:REPetitions



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.SpdifLeft.Repetitions.RepetitionsCls
	:members:
	:undoc-members:
	:noindex: