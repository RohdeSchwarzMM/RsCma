IdSignal
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:VOR:IDSignal:ENABle
	single: SOURce:AVIonics:GENerator<Instance>:VOR:IDSignal:MDEPth
	single: SOURce:AVIonics:GENerator<Instance>:VOR:IDSignal:FREQuency

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:VOR:IDSignal:ENABle
	SOURce:AVIonics:GENerator<Instance>:VOR:IDSignal:MDEPth
	SOURce:AVIonics:GENerator<Instance>:VOR:IDSignal:FREQuency



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Vor.IdSignal.IdSignalCls
	:members:
	:undoc-members:
	:noindex: