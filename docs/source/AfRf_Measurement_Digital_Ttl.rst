Ttl
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Ttl.TtlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.digital.ttl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_Digital_Ttl_BitErrorRate.rst