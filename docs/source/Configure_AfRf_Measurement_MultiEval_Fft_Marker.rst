Marker<MarkerOther>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr2 .. Nr5
	rc = driver.configure.afRf.measurement.multiEval.fft.marker.repcap_markerOther_get()
	driver.configure.afRf.measurement.multiEval.fft.marker.repcap_markerOther_set(repcap.MarkerOther.Nr2)





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Fft.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.fft.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Fft_Marker_Enable.rst
	Configure_AfRf_Measurement_MultiEval_Fft_Marker_Placement.rst