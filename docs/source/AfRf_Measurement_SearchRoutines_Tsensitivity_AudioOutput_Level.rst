Level
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:AOUT:LEVel
	single: CALCulate:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:AOUT:LEVel
	single: READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:AOUT:LEVel

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:AOUT:LEVel
	CALCulate:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:AOUT:LEVel
	READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:AOUT:LEVel



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Tsensitivity.AudioOutput.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.searchRoutines.tsensitivity.audioOutput.level.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_SearchRoutines_Tsensitivity_AudioOutput_Level_Trace.rst