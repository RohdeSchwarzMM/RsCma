AudioInput<AudioInput>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.afRf.measurement.multiEval.signalQuality.audioInput.repcap_audioInput_get()
	driver.afRf.measurement.multiEval.signalQuality.audioInput.repcap_audioInput_set(repcap.AudioInput.Nr1)





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.AudioInput.AudioInputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.signalQuality.audioInput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_SignalQuality_AudioInput_Average.rst
	AfRf_Measurement_MultiEval_SignalQuality_AudioInput_Current.rst
	AfRf_Measurement_MultiEval_SignalQuality_AudioInput_Deviation.rst
	AfRf_Measurement_MultiEval_SignalQuality_AudioInput_Extreme.rst