First
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.First.FirstCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.audioInput.first.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_AudioInput_First_AfSignal.rst
	AfRf_Measurement_MultiEval_AudioInput_First_Level.rst