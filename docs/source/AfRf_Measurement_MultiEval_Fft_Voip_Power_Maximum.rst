Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:POWer:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Voip.Power.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: