BitErrorRate
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:LIMit:PTFive:BERate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:LIMit:PTFive:BERate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.Limit.PtFive.BitErrorRate.BitErrorRateCls
	:members:
	:undoc-members:
	:noindex: