FilterPy
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.spdif.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Spdif_FilterPy_Bpass.rst
	Configure_AfRf_Measurement_Spdif_FilterPy_Dfrequency.rst
	Configure_AfRf_Measurement_Spdif_FilterPy_Dwidth.rst
	Configure_AfRf_Measurement_Spdif_FilterPy_Hpass.rst
	Configure_AfRf_Measurement_Spdif_FilterPy_Lpass.rst
	Configure_AfRf_Measurement_Spdif_FilterPy_Notch.rst
	Configure_AfRf_Measurement_Spdif_FilterPy_RobustAuto.rst
	Configure_AfRf_Measurement_Spdif_FilterPy_Weighting.rst