Peaks
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.Peaks.PeaksCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.fftSpecAn.peaks.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_FftSpecAn_Peaks_Average.rst
	GprfMeasurement_FftSpecAn_Peaks_Current.rst