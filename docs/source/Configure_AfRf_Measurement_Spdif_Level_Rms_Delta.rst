Delta
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:RMS:DELTa:MODE
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:RMS:DELTa:MEASured

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:RMS:DELTa:MODE
	CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:RMS:DELTa:MEASured



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.Level.Rms.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.spdif.level.rms.delta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Spdif_Level_Rms_Delta_Update.rst
	Configure_AfRf_Measurement_Spdif_Level_Rms_Delta_User.rst