Demod
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:DEMod:SCHeme

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:DEMod:SCHeme



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Tx.Demod.DemodCls
	:members:
	:undoc-members:
	:noindex: