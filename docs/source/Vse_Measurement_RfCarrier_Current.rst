Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:RFCarrier:CURRent
	single: READ:VSE:MEASurement<Instance>:RFCarrier:CURRent
	single: CALCulate:VSE:MEASurement<Instance>:RFCarrier:CURRent

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:RFCarrier:CURRent
	READ:VSE:MEASurement<Instance>:RFCarrier:CURRent
	CALCulate:VSE:MEASurement<Instance>:RFCarrier:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.RfCarrier.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: