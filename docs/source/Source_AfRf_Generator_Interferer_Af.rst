Af
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IFERer:AF:ENABle
	single: SOURce:AFRF:GENerator<Instance>:IFERer:AF:FREQuency

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IFERer:AF:ENABle
	SOURce:AFRF:GENerator<Instance>:IFERer:AF:FREQuency



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Interferer.Af.AfCls
	:members:
	:undoc-members:
	:noindex: