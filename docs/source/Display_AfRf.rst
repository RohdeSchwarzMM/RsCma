AfRf
----------------------------------------





.. autoclass:: RsCma.Implementations.Display.AfRf.AfRfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.afRf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_AfRf_Measurement.rst