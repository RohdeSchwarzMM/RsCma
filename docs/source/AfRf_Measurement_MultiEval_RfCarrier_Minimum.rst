Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:MINimum
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:MINimum

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:MINimum
	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: