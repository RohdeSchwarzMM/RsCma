Mfidelity
----------------------------------------





.. autoclass:: RsCma.Implementations.Vse.Measurement.PtFive.Mfidelity.MfidelityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.vse.measurement.ptFive.mfidelity.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Vse_Measurement_PtFive_Mfidelity_Current.rst
	Vse_Measurement_PtFive_Mfidelity_Maximum.rst