Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:ACP:ACLR:CURRent
	single: READ:GPRF:MEASurement<Instance>:ACP:ACLR:CURRent
	single: CALCulate:GPRF:MEASurement<Instance>:ACP:ACLR:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:ACP:ACLR:CURRent
	READ:GPRF:MEASurement<Instance>:ACP:ACLR:CURRent
	CALCulate:GPRF:MEASurement<Instance>:ACP:ACLR:CURRent



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Aclr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: