DemodLeft
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.DemodLeft.DemodLeftCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.signalQuality.demodLeft.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_SignalQuality_DemodLeft_Average.rst
	AfRf_Measurement_MultiEval_SignalQuality_DemodLeft_Current.rst
	AfRf_Measurement_MultiEval_SignalQuality_DemodLeft_Deviation.rst
	AfRf_Measurement_MultiEval_SignalQuality_DemodLeft_Extreme.rst