Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FREQuency:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FREQuency:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Frequency.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: