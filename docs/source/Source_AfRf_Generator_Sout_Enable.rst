Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:SOUT:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:SOUT:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Sout.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: