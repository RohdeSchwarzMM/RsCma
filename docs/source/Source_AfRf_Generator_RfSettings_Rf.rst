Rf
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:RFSettings:RF:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:RFSettings:RF:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.RfSettings.Rf.RfCls
	:members:
	:undoc-members:
	:noindex: