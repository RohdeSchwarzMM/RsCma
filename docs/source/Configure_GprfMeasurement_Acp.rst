Acp
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:MOEXception
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:STANdard
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:TOUT
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:REPetition
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:RCOupling
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:SCOunt
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:CSPace
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:MBWidth
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:OFFSet

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:ACP:MOEXception
	CONFigure:GPRF:MEASurement<Instance>:ACP:STANdard
	CONFigure:GPRF:MEASurement<Instance>:ACP:TOUT
	CONFigure:GPRF:MEASurement<Instance>:ACP:REPetition
	CONFigure:GPRF:MEASurement<Instance>:ACP:RCOupling
	CONFigure:GPRF:MEASurement<Instance>:ACP:SCOunt
	CONFigure:GPRF:MEASurement<Instance>:ACP:CSPace
	CONFigure:GPRF:MEASurement<Instance>:ACP:MBWidth
	CONFigure:GPRF:MEASurement<Instance>:ACP:OFFSet



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Acp.AcpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.acp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_Acp_Limit.rst
	Configure_GprfMeasurement_Acp_Nxdn.rst
	Configure_GprfMeasurement_Acp_Obw.rst