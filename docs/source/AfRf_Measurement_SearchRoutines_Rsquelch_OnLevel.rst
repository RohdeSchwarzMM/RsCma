OnLevel
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:ONLevel
	single: READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:ONLevel

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:ONLevel
	READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:ONLevel



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsquelch.OnLevel.OnLevelCls
	:members:
	:undoc-members:
	:noindex: