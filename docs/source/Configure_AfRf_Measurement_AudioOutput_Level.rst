Level
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AOUT<nr>:LEVel

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AOUT<nr>:LEVel



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioOutput.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: