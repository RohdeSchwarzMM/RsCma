Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:RFCarrier:FREQuency:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:RFCarrier:FREQuency:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.RfCarrier.Frequency.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: