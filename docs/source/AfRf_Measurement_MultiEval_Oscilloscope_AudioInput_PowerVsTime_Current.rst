Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN<Nr>:PVTime:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN<Nr>:PVTime:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN<Nr>:PVTime:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN<Nr>:PVTime:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.AudioInput.PowerVsTime.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: