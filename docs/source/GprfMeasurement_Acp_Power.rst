Power
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.acp.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Acp_Power_Average.rst
	GprfMeasurement_Acp_Power_Current.rst
	GprfMeasurement_Acp_Power_Maximum.rst
	GprfMeasurement_Acp_Power_Minimum.rst