MultiEval
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: INITiate:AFRF:MEASurement<Instance>:MEValuation
	single: STOP:AFRF:MEASurement<Instance>:MEValuation
	single: ABORt:AFRF:MEASurement<Instance>:MEValuation

.. code-block:: python

	INITiate:AFRF:MEASurement<Instance>:MEValuation
	STOP:AFRF:MEASurement<Instance>:MEValuation
	ABORt:AFRF:MEASurement<Instance>:MEValuation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_AudioInput.rst
	AfRf_Measurement_MultiEval_DemodLeft.rst
	AfRf_Measurement_MultiEval_DemodRight.rst
	AfRf_Measurement_MultiEval_Demodulation.rst
	AfRf_Measurement_MultiEval_Fft.rst
	AfRf_Measurement_MultiEval_Oscilloscope.rst
	AfRf_Measurement_MultiEval_RfCarrier.rst
	AfRf_Measurement_MultiEval_SignalQuality.rst
	AfRf_Measurement_MultiEval_SpdifLeft.rst
	AfRf_Measurement_MultiEval_SpdifRight.rst
	AfRf_Measurement_MultiEval_State.rst
	AfRf_Measurement_MultiEval_Tones.rst
	AfRf_Measurement_MultiEval_Voip.rst