Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:CURRent
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:CURRent
	READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:CURRent



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Sample.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: