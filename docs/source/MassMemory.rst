MassMemory
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: MMEMory:COPY
	single: MMEMory:DELete
	single: MMEMory:DRIVes
	single: MMEMory:MDIRectory
	single: MMEMory:MOVE
	single: MMEMory:MSIS
	single: MMEMory:RDIRectory

.. code-block:: python

	MMEMory:COPY
	MMEMory:DELete
	MMEMory:DRIVes
	MMEMory:MDIRectory
	MMEMory:MOVE
	MMEMory:MSIS
	MMEMory:RDIRectory



.. autoclass:: RsCma.Implementations.MassMemory.MassMemoryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.massMemory.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MassMemory_Attribute.rst
	MassMemory_Catalog.rst
	MassMemory_CurrentDirectory.rst
	MassMemory_Dcatalog.rst
	MassMemory_Load.rst
	MassMemory_Store.rst