CurrentDirectory
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: MMEMory:CDIRectory

.. code-block:: python

	MMEMory:CDIRectory



.. autoclass:: RsCma.Implementations.MassMemory.CurrentDirectory.CurrentDirectoryCls
	:members:
	:undoc-members:
	:noindex: