Dpmr
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DPMR:PATTern
	single: SOURce:AFRF:GENerator<Instance>:DPMR:SVALue
	single: SOURce:AFRF:GENerator<Instance>:DPMR:SID
	single: SOURce:AFRF:GENerator<Instance>:DPMR:DID
	single: SOURce:AFRF:GENerator<Instance>:DPMR:PTPeer
	single: SOURce:AFRF:GENerator<Instance>:DPMR:EMERgency
	single: SOURce:AFRF:GENerator<Instance>:DPMR:MODE
	single: SOURce:AFRF:GENerator<Instance>:DPMR:SDEViation
	single: SOURce:AFRF:GENerator<Instance>:DPMR:SRATe
	single: SOURce:AFRF:GENerator<Instance>:DPMR:FILTer
	single: SOURce:AFRF:GENerator<Instance>:DPMR:ROFactor

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DPMR:PATTern
	SOURce:AFRF:GENerator<Instance>:DPMR:SVALue
	SOURce:AFRF:GENerator<Instance>:DPMR:SID
	SOURce:AFRF:GENerator<Instance>:DPMR:DID
	SOURce:AFRF:GENerator<Instance>:DPMR:PTPeer
	SOURce:AFRF:GENerator<Instance>:DPMR:EMERgency
	SOURce:AFRF:GENerator<Instance>:DPMR:MODE
	SOURce:AFRF:GENerator<Instance>:DPMR:SDEViation
	SOURce:AFRF:GENerator<Instance>:DPMR:SRATe
	SOURce:AFRF:GENerator<Instance>:DPMR:FILTer
	SOURce:AFRF:GENerator<Instance>:DPMR:ROFactor



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dpmr.DpmrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.dpmr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Dpmr_Ccode.rst