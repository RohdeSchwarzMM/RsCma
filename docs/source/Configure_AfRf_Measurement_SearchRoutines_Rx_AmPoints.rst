AmPoints
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:RX:AMPoints:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:RX:AMPoints:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Rx.AmPoints.AmPointsCls
	:members:
	:undoc-members:
	:noindex: