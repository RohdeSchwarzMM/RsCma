Lpass
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:LPASs

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:LPASs



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.FilterPy.Lpass.LpassCls
	:members:
	:undoc-members:
	:noindex: