Delta
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FDEViation:PEAK:DELTa:MODE
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FDEViation:PEAK:DELTa:USER
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FDEViation:PEAK:DELTa:MEASured

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FDEViation:PEAK:DELTa:MODE
	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FDEViation:PEAK:DELTa:USER
	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FDEViation:PEAK:DELTa:MEASured



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.Fdeviation.Peak.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.demodulation.fdeviation.peak.delta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Demodulation_Fdeviation_Peak_Delta_Update.rst