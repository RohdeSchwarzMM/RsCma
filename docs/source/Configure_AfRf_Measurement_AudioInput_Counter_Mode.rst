Mode
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:COUNter:MODE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:COUNter:MODE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Counter.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: