Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:SQUality:TRACe
	single: READ:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:SQUality:TRACe

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:SQUality:TRACe
	READ:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:SQUality:TRACe



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.RifBandwidth.SignalQuality.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: