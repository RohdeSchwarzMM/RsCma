Sip
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:VOIP:SIP:RESPonse
	single: SOURce:AFRF:GENerator<Instance>:VOIP:SIP:STATe
	single: SOURce:AFRF:GENerator<Instance>:VOIP:SIP:CODE
	single: SOURce:AFRF:GENerator<Instance>:VOIP:SIP:RPRotocol
	single: SOURce:AFRF:GENerator<Instance>:VOIP:SIP:RCAuse
	single: SOURce:AFRF:GENerator<Instance>:VOIP:SIP:RTEXt

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:VOIP:SIP:RESPonse
	SOURce:AFRF:GENerator<Instance>:VOIP:SIP:STATe
	SOURce:AFRF:GENerator<Instance>:VOIP:SIP:CODE
	SOURce:AFRF:GENerator<Instance>:VOIP:SIP:RPRotocol
	SOURce:AFRF:GENerator<Instance>:VOIP:SIP:RCAuse
	SOURce:AFRF:GENerator<Instance>:VOIP:SIP:RTEXt



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Voip.Sip.SipCls
	:members:
	:undoc-members:
	:noindex: