Stop
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:SPECtrum:FREQuency:STOP

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:SPECtrum:FREQuency:STOP



.. autoclass:: RsCma.Implementations.Vse.Measurement.Spectrum.Frequency.Stop.StopCls
	:members:
	:undoc-members:
	:noindex: