Dimpedance
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:AOUT<nr>:DIMPedance

.. code-block:: python

	CONFigure:BASE:AOUT<nr>:DIMPedance



.. autoclass:: RsCma.Implementations.Configure.Base.AudioOutput.Dimpedance.DimpedanceCls
	:members:
	:undoc-members:
	:noindex: