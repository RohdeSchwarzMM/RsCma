FskDeviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:TONes:DCS:FSKDeviation

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:TONes:DCS:FSKDeviation



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Tones.Dcs.FskDeviation.FskDeviationCls
	:members:
	:undoc-members:
	:noindex: