Oscilloscope
----------------------------------------





.. autoclass:: RsCma.Implementations.Trigger.AfRf.Measurement.MultiEval.Oscilloscope.OscilloscopeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.afRf.measurement.multiEval.oscilloscope.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_AfRf_Measurement_MultiEval_Oscilloscope_AudioInput.rst
	Trigger_AfRf_Measurement_MultiEval_Oscilloscope_Demodulation.rst
	Trigger_AfRf_Measurement_MultiEval_Oscilloscope_Spdif.rst
	Trigger_AfRf_Measurement_MultiEval_Oscilloscope_Timeout.rst
	Trigger_AfRf_Measurement_MultiEval_Oscilloscope_Voip.rst