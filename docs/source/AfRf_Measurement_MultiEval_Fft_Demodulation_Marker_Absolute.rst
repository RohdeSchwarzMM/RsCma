Absolute
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation<nr>:MARKer<mnr>:ABSolute

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation<nr>:MARKer<mnr>:ABSolute



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.Marker.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex: