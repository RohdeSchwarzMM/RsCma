RfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:MBEacon:RFSettings:FREQuency
	single: SOURce:AVIonics:GENerator<Instance>:MBEacon:RFSettings:LEVel

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:MBEacon:RFSettings:FREQuency
	SOURce:AVIonics:GENerator<Instance>:MBEacon:RFSettings:LEVel



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.MarkerBeacon.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.markerBeacon.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_MarkerBeacon_RfSettings_RfOut.rst