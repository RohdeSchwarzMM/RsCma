All
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:STATe:ALL

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:STATe:ALL



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: