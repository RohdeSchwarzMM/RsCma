Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodLeft.AfSignal.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: