Absolute
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SIN<nr>:MARKer<mnr>:ABSolute

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SIN<nr>:MARKer<mnr>:ABSolute



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Spdif.Marker.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex: