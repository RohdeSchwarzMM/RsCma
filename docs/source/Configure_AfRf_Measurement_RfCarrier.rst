RfCarrier
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.RfCarrier.RfCarrierCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.rfCarrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_RfCarrier_FreqError.rst
	Configure_AfRf_Measurement_RfCarrier_Frequency.rst
	Configure_AfRf_Measurement_RfCarrier_PePower.rst
	Configure_AfRf_Measurement_RfCarrier_Power.rst