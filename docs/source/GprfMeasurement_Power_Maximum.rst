Maximum
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Power.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.power.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Power_Maximum_Current.rst