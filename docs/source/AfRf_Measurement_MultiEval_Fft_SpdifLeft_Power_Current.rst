Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINLeft:POWer:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.SpdifLeft.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: