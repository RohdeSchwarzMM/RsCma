Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:CURRent
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:CURRent

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:CURRent
	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:PDEViation:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.Pdeviation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: