Reliability
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:RELiability
	single: SOURce:AFRF:GENerator<Instance>:RELiability:ALL

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:RELiability
	SOURce:AFRF:GENerator<Instance>:RELiability:ALL



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Reliability.ReliabilityCls
	:members:
	:undoc-members:
	:noindex: