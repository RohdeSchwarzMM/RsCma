Device
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:SHUTdown:DEVice

.. code-block:: python

	SYSTem:BASE:SHUTdown:DEVice



.. autoclass:: RsCma.Implementations.System.Base.Shutdown.Device.DeviceCls
	:members:
	:undoc-members:
	:noindex: