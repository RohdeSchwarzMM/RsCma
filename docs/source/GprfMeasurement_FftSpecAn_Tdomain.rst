Tdomain
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.Tdomain.TdomainCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.fftSpecAn.tdomain.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_FftSpecAn_Tdomain_Xvalues.rst