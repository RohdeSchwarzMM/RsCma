State
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:STATe
	single: SOURce:AVIonics:GENerator<Instance>:ILS:STATe:ALL

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:STATe
	SOURce:AVIonics:GENerator<Instance>:ILS:STATe:ALL



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.State.StateCls
	:members:
	:undoc-members:
	:noindex: