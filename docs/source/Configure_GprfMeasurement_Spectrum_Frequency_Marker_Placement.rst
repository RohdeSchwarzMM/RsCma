Placement
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:MARKer<nr>:PLACement

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:MARKer<nr>:PLACement



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Spectrum.Frequency.Marker.Placement.PlacementCls
	:members:
	:undoc-members:
	:noindex: