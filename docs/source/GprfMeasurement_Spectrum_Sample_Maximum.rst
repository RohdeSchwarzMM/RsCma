Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MAXimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MAXimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MAXimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Sample.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: