PoOff
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Dmr.PoOff.PoOffCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.digital.dmr.poOff.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_Digital_Dmr_PoOff_Current.rst