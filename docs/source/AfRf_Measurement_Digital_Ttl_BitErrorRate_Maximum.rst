Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:MAXimum
	single: CALCulate:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:MAXimum
	single: READ:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:MAXimum
	CALCulate:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:MAXimum
	READ:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Ttl.BitErrorRate.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: