SignalQuality
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsensitivity.SignalQuality.SignalQualityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.searchRoutines.rsensitivity.signalQuality.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_SearchRoutines_Rsensitivity_SignalQuality_Trace.rst