Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:NRT:REVerse:MINimum
	single: FETCh:GPRF:MEASurement<Instance>:NRT:REVerse:MINimum
	single: READ:GPRF:MEASurement<Instance>:NRT:REVerse:MINimum

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:NRT:REVerse:MINimum
	FETCh:GPRF:MEASurement<Instance>:NRT:REVerse:MINimum
	READ:GPRF:MEASurement<Instance>:NRT:REVerse:MINimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Nrt.Reverse.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: