Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:MTONe:TONE<no>:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:MTONe:TONE<no>:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.MultiTone.Tone.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: