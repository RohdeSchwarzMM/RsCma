SysSound
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:SYSSound:VOLume

.. code-block:: python

	CONFigure:BASE:SYSSound:VOLume



.. autoclass:: RsCma.Implementations.Configure.Base.SysSound.SysSoundCls
	:members:
	:undoc-members:
	:noindex: