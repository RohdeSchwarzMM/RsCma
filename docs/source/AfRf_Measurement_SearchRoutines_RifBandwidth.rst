RifBandwidth
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.RifBandwidth.RifBandwidthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.searchRoutines.rifBandwidth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_SearchRoutines_RifBandwidth_Bandwidth.rst
	AfRf_Measurement_SearchRoutines_RifBandwidth_Coffset.rst
	AfRf_Measurement_SearchRoutines_RifBandwidth_Frequency.rst
	AfRf_Measurement_SearchRoutines_RifBandwidth_Nlevel.rst
	AfRf_Measurement_SearchRoutines_RifBandwidth_SignalQuality.rst
	AfRf_Measurement_SearchRoutines_RifBandwidth_Slevel.rst