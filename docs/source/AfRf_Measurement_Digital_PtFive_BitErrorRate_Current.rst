Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:PTFive:BERate:CURRent
	single: CALCulate:AFRF:MEASurement<Instance>:DIGital:PTFive:BERate:CURRent
	single: READ:AFRF:MEASurement<Instance>:DIGital:PTFive:BERate:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:PTFive:BERate:CURRent
	CALCulate:AFRF:MEASurement<Instance>:DIGital:PTFive:BERate:CURRent
	READ:AFRF:MEASurement<Instance>:DIGital:PTFive:BERate:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.PtFive.BitErrorRate.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: