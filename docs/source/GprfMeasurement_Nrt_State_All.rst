All
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:NRT:STATe:ALL

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:NRT:STATe:ALL



.. autoclass:: RsCma.Implementations.GprfMeasurement.Nrt.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: