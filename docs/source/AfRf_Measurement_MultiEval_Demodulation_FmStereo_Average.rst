Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:AVERage
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:AVERage

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:AVERage
	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.FmStereo.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: