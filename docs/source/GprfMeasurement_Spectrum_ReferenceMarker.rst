ReferenceMarker
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.ReferenceMarker.ReferenceMarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.spectrum.referenceMarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Spectrum_ReferenceMarker_Npeak.rst
	GprfMeasurement_Spectrum_ReferenceMarker_Speak.rst