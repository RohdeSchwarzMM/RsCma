Bandpass
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.IqRecorder.FilterPy.Bandpass.BandpassCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.iqRecorder.filterPy.bandpass.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_IqRecorder_FilterPy_Bandpass_Bandwidth.rst