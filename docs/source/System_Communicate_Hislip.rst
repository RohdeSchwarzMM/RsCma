Hislip
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:COMMunicate:HISLip:VRESource

.. code-block:: python

	SYSTem:COMMunicate:HISLip:VRESource



.. autoclass:: RsCma.Implementations.System.Communicate.Hislip.HislipCls
	:members:
	:undoc-members:
	:noindex: