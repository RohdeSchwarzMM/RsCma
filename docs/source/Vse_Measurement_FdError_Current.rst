Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:FDERror:CURRent
	single: READ:VSE:MEASurement<Instance>:FDERror:CURRent
	single: CALCulate:VSE:MEASurement<Instance>:FDERror:CURRent

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:FDERror:CURRent
	READ:VSE:MEASurement<Instance>:FDERror:CURRent
	CALCulate:VSE:MEASurement<Instance>:FDERror:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.FdError.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: