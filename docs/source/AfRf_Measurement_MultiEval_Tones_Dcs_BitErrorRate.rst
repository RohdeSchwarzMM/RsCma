BitErrorRate
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.BitErrorRate.BitErrorRateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.tones.dcs.bitErrorRate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Tones_Dcs_BitErrorRate_Average.rst
	AfRf_Measurement_MultiEval_Tones_Dcs_BitErrorRate_Current.rst
	AfRf_Measurement_MultiEval_Tones_Dcs_BitErrorRate_Deviation.rst
	AfRf_Measurement_MultiEval_Tones_Dcs_BitErrorRate_Maximum.rst