Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:ENABle

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:ENABle



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: