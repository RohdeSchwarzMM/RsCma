Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:LLISt:TRACe
	single: READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:LLISt:TRACe

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RSQuelch:LLISt:TRACe
	READ:AFRF:MEASurement<Instance>:SROutines:RSQuelch:LLISt:TRACe



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsquelch.Llist.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: