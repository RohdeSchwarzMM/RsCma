SearchRoutines
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:MRFLevel
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:SQValue
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:SQTYpe
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:STOLerance
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:PATH
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TOUT

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:MRFLevel
	CONFigure:AFRF:MEASurement<Instance>:SROutines:SQValue
	CONFigure:AFRF:MEASurement<Instance>:SROutines:SQTYpe
	CONFigure:AFRF:MEASurement<Instance>:SROutines:STOLerance
	CONFigure:AFRF:MEASurement<Instance>:SROutines:PATH
	CONFigure:AFRF:MEASurement<Instance>:SROutines:TOUT



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.SearchRoutinesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.searchRoutines.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_SearchRoutines_Dialing.rst
	Configure_AfRf_Measurement_SearchRoutines_Limit.rst
	Configure_AfRf_Measurement_SearchRoutines_LrfLevel.rst
	Configure_AfRf_Measurement_SearchRoutines_RifBandwidth.rst
	Configure_AfRf_Measurement_SearchRoutines_Rsquelch.rst
	Configure_AfRf_Measurement_SearchRoutines_Rx.rst
	Configure_AfRf_Measurement_SearchRoutines_Ssnr.rst
	Configure_AfRf_Measurement_SearchRoutines_Tsensitivity.rst
	Configure_AfRf_Measurement_SearchRoutines_Tx.rst