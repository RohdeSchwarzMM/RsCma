Bandwidth
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:IQRecorder:FILTer:BANDpass:BWIDth

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:IQRecorder:FILTer:BANDpass:BWIDth



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.IqRecorder.FilterPy.Bandpass.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: