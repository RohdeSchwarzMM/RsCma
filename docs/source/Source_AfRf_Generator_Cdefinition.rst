Cdefinition
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:CDEFinition:RCHannel
	single: SOURce:AFRF:GENerator<Instance>:CDEFinition:RFRequency
	single: SOURce:AFRF:GENerator<Instance>:CDEFinition:CSPace
	single: SOURce:AFRF:GENerator<Instance>:CDEFinition

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:CDEFinition:RCHannel
	SOURce:AFRF:GENerator<Instance>:CDEFinition:RFRequency
	SOURce:AFRF:GENerator<Instance>:CDEFinition:CSPace
	SOURce:AFRF:GENerator<Instance>:CDEFinition



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Cdefinition.CdefinitionCls
	:members:
	:undoc-members:
	:noindex: