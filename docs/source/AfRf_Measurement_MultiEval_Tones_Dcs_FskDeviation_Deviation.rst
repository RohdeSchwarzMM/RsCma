Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.FskDeviation.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: