Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:DEViation
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:DEViation

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:DEViation
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINRight:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.SpdifRight.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: