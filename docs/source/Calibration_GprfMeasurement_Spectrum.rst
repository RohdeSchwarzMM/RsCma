Spectrum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALibration:GPRF:MEASurement<Instance>:SPECtrum:TGENerator

.. code-block:: python

	CALibration:GPRF:MEASurement<Instance>:SPECtrum:TGENerator



.. autoclass:: RsCma.Implementations.Calibration.GprfMeasurement.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex: