Device
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:DEVice:ID

.. code-block:: python

	SYSTem:BASE:DEVice:ID



.. autoclass:: RsCma.Implementations.System.Base.Device.DeviceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.base.device.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Base_Device_License.rst
	System_Base_Device_Setup.rst