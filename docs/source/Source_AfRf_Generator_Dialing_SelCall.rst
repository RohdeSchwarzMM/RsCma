SelCall
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:STANdard
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:SEQuence
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:SREPeat
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:SPAuse
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:DTIMe
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:DPAuse
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:DREPeat

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:STANdard
	SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:SEQuence
	SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:SREPeat
	SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:SPAuse
	SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:DTIMe
	SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:DPAuse
	SOURce:AFRF:GENerator<Instance>:DIALing:SELCall:DREPeat



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.SelCall.SelCallCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.dialing.selCall.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Dialing_SelCall_Frequency.rst
	Source_AfRf_Generator_Dialing_SelCall_UserDefined.rst