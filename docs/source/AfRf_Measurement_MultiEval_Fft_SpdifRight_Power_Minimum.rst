Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.SpdifRight.Power.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: