Obw
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:LIMit:OBW:ENABle
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:LIMit:OBW

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:ACP:LIMit:OBW:ENABle
	CONFigure:GPRF:MEASurement<Instance>:ACP:LIMit:OBW



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Acp.Limit.Obw.ObwCls
	:members:
	:undoc-members:
	:noindex: