Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:MDEPth:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:MDEPth:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.ModDepth.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: