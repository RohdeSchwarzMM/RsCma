AudioOutput
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:AOUT:ENABle
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:AOUT:LEVel

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:AOUT:ENABle
	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:AOUT:LEVel



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Localizer.AfSettings.AudioOutput.AudioOutputCls
	:members:
	:undoc-members:
	:noindex: