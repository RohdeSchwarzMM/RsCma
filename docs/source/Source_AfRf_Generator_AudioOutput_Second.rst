Second
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:AOUT:SECond:LEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:AOUT:SECond:LEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.AudioOutput.Second.SecondCls
	:members:
	:undoc-members:
	:noindex: