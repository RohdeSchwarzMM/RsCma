Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.ModDepth.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: