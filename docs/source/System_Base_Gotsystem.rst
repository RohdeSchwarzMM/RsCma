Gotsystem
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:GOTSystem

.. code-block:: python

	SYSTem:BASE:GOTSystem



.. autoclass:: RsCma.Implementations.System.Base.Gotsystem.GotsystemCls
	:members:
	:undoc-members:
	:noindex: