Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:AFSignal:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Voip.AfSignal.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: