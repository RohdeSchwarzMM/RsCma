Fft
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:FFT:SPAN
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:FFT:LENGth
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:FFT:WINDow

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:FFT:SPAN
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:FFT:LENGth
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:FFT:WINDow



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Fft.FftCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.fft.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Fft_Marker.rst