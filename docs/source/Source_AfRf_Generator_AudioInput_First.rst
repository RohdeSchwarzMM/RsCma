First
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:AIN:FIRSt:MLEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:AIN:FIRSt:MLEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.AudioInput.First.FirstCls
	:members:
	:undoc-members:
	:noindex: