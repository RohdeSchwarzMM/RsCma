Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:NRT:REVerse:CURRent
	single: FETCh:GPRF:MEASurement<Instance>:NRT:REVerse:CURRent
	single: READ:GPRF:MEASurement<Instance>:NRT:REVerse:CURRent

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:NRT:REVerse:CURRent
	FETCh:GPRF:MEASurement<Instance>:NRT:REVerse:CURRent
	READ:GPRF:MEASurement<Instance>:NRT:REVerse:CURRent



.. autoclass:: RsCma.Implementations.GprfMeasurement.Nrt.Reverse.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: