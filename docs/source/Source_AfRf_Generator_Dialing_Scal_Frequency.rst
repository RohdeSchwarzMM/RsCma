Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:FREQuency:RESet
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:FREQuency

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:FREQuency:RESet
	SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:FREQuency



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.Scal.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: