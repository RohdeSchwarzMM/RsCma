Latest
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALibration:BASE:LATest

.. code-block:: python

	CALibration:BASE:LATest



.. autoclass:: RsCma.Implementations.Calibration.Base.Latest.LatestCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calibration.base.latest.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_Base_Latest_Specific.rst