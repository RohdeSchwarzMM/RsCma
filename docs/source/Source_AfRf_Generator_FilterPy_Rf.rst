Rf
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:FILTer:RF:PEMPhasis
	single: SOURce:AFRF:GENerator<Instance>:FILTer:RF:HPASs
	single: SOURce:AFRF:GENerator<Instance>:FILTer:RF:LPASs

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:FILTer:RF:PEMPhasis
	SOURce:AFRF:GENerator<Instance>:FILTer:RF:HPASs
	SOURce:AFRF:GENerator<Instance>:FILTer:RF:LPASs



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.FilterPy.Rf.RfCls
	:members:
	:undoc-members:
	:noindex: