Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:MDEPth:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.ModDepth.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: