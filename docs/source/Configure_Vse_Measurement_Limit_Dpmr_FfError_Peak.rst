Peak
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:DPMR:FFERor:PEAK

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:DPMR:FFERor:PEAK



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Dpmr.FfError.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: