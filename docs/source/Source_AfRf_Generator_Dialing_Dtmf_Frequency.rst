Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:FREQuency:RESet
	single: SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:FREQuency

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:FREQuency:RESet
	SOURce:AFRF:GENerator<Instance>:DIALing:DTMF:FREQuency



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.Dtmf.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: