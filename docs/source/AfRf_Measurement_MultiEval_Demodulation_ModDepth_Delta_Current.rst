Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.ModDepth.Delta.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: