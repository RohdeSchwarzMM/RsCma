Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.AudioInput.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: