Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:AVERage
	single: READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:AVERage
	READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:AVERage



.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.Power.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: