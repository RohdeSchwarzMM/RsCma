Rloss
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:REVerse:LIMit:RLOSs

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRT:REVerse:LIMit:RLOSs



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.Reverse.Limit.Rloss.RlossCls
	:members:
	:undoc-members:
	:noindex: