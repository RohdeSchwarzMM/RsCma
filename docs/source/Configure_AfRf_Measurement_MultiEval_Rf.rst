Rf
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:RF:SCOunt

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:RF:SCOunt



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Rf.RfCls
	:members:
	:undoc-members:
	:noindex: