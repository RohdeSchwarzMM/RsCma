Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.FreqError.Delta.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: