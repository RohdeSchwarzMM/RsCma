SpdifLeft
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft:MODE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:SINLeft:MODE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.SpdifLeft.SpdifLeftCls
	:members:
	:undoc-members:
	:noindex: