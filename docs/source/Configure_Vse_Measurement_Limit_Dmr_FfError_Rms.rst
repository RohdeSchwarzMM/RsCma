Rms
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:LIMit:DMR:FFERor:RMS

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:LIMit:DMR:FFERor:RMS



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Dmr.FfError.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: