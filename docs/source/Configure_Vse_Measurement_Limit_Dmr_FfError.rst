FfError
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Limit.Dmr.FfError.FfErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.vse.measurement.limit.dmr.ffError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Vse_Measurement_Limit_Dmr_FfError_Peak.rst
	Configure_Vse_Measurement_Limit_Dmr_FfError_Rms.rst