Digital
----------------------------------------





.. autoclass:: RsCma.Implementations.Display.AfRf.Measurement.Digital.DigitalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.afRf.measurement.digital.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_AfRf_Measurement_Digital_Application.rst