Specific
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALibration:BASE:LATest:SPECific

.. code-block:: python

	CALibration:BASE:LATest:SPECific



.. autoclass:: RsCma.Implementations.Calibration.Base.Latest.Specific.SpecificCls
	:members:
	:undoc-members:
	:noindex: