Absolute
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:MARKer<mnr>:ABSolute

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:MARKer<mnr>:ABSolute



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Voip.Marker.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex: