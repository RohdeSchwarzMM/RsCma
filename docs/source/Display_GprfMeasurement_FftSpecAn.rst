FftSpecAn
----------------------------------------





.. autoclass:: RsCma.Implementations.Display.GprfMeasurement.FftSpecAn.FftSpecAnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.gprfMeasurement.fftSpecAn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_GprfMeasurement_FftSpecAn_Trace.rst