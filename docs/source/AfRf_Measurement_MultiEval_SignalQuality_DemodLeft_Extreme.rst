Extreme
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:EXTReme
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:EXTReme
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:EXTReme

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:EXTReme
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:EXTReme
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMLeft:EXTReme



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.DemodLeft.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: