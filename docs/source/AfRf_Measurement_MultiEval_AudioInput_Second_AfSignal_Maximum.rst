Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:AFSignal:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:AFSignal:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:AFSignal:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN:SECond:AFSignal:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.Second.AfSignal.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: