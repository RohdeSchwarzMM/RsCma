Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:CURRent
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:CURRent

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:CURRent
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:SINLeft:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.SpdifLeft.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: