Sinfo
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:PTFive:SINFo
	single: READ:AFRF:MEASurement<Instance>:DIGital:PTFive:SINFo

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:PTFive:SINFo
	READ:AFRF:MEASurement<Instance>:DIGital:PTFive:SINFo



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.PtFive.Sinfo.SinfoCls
	:members:
	:undoc-members:
	:noindex: