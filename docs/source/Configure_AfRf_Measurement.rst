Measurement
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_AudioInput.rst
	Configure_AfRf_Measurement_AudioOutput.rst
	Configure_AfRf_Measurement_Cdefinition.rst
	Configure_AfRf_Measurement_Delta.rst
	Configure_AfRf_Measurement_Demodulation.rst
	Configure_AfRf_Measurement_Digital.rst
	Configure_AfRf_Measurement_FilterPy.rst
	Configure_AfRf_Measurement_Frequency.rst
	Configure_AfRf_Measurement_MultiEval.rst
	Configure_AfRf_Measurement_RfCarrier.rst
	Configure_AfRf_Measurement_RfSettings.rst
	Configure_AfRf_Measurement_SearchRoutines.rst
	Configure_AfRf_Measurement_Sout.rst
	Configure_AfRf_Measurement_Spdif.rst
	Configure_AfRf_Measurement_Voip.rst