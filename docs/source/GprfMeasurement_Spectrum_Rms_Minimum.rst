Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:MINimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:MINimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:MINimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:MINimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Rms.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: