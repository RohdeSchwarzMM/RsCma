Frequency
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.audioInput.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_AudioInput_Frequency_Delta.rst