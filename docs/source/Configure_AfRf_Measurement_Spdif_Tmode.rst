Tmode
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:TMODe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:TMODe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.Tmode.TmodeCls
	:members:
	:undoc-members:
	:noindex: