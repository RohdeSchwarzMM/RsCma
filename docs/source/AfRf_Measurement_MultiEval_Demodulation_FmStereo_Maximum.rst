Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:MAXimum
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:MAXimum

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:MAXimum
	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:FMSTereo:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.FmStereo.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: