FmStereo
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:MODulator:FMSTereo:MDEViation

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:MODulator:FMSTereo:MDEViation



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Modulator.FmStereo.FmStereoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.modulator.fmStereo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Modulator_FmStereo_Madeviation.rst
	Source_AfRf_Generator_Modulator_FmStereo_Pdeviation.rst
	Source_AfRf_Generator_Modulator_FmStereo_RdsDeviation.rst