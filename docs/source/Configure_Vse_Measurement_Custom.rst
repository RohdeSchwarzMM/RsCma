Custom
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:CUSTom:LOAD
	single: CONFigure:VSE:MEASurement<Instance>:CUSTom:SAVE

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:CUSTom:LOAD
	CONFigure:VSE:MEASurement<Instance>:CUSTom:SAVE



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Custom.CustomCls
	:members:
	:undoc-members:
	:noindex: