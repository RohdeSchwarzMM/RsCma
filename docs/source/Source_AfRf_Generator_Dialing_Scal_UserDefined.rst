UserDefined
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:UDEFined:ENABle

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:SCAL:UDEFined:ENABle



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.Scal.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: