Gslope
----------------------------------------





.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Gslope.GslopeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.ils.gslope.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_Ils_Gslope_AfSettings.rst
	Source_Avionics_Generator_Ils_Gslope_RfSettings.rst