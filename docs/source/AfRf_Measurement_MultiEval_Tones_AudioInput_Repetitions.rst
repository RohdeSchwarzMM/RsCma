Repetitions
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>:REPetitions
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>:REPetitions

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>:REPetitions
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:AIN<Nr>:REPetitions



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.AudioInput.Repetitions.RepetitionsCls
	:members:
	:undoc-members:
	:noindex: