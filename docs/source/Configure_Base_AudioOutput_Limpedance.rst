Limpedance
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:AOUT<nr>:LIMPedance

.. code-block:: python

	CONFigure:BASE:AOUT<nr>:LIMPedance



.. autoclass:: RsCma.Implementations.Configure.Base.AudioOutput.Limpedance.LimpedanceCls
	:members:
	:undoc-members:
	:noindex: