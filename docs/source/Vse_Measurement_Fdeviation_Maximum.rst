Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:FDEViation:MAXimum
	single: READ:VSE:MEASurement<Instance>:FDEViation:MAXimum

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:FDEViation:MAXimum
	READ:VSE:MEASurement<Instance>:FDEViation:MAXimum



.. autoclass:: RsCma.Implementations.Vse.Measurement.Fdeviation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: