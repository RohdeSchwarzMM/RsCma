Aclr
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.acp.aclr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Acp_Aclr_Average.rst
	GprfMeasurement_Acp_Aclr_Current.rst
	GprfMeasurement_Acp_Aclr_Maximum.rst
	GprfMeasurement_Acp_Aclr_StandardDev.rst