Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodRight.AfSignal.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: