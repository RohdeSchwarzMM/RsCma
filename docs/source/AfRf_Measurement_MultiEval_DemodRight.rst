DemodRight
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodRight.DemodRightCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.demodRight.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_DemodRight_AfSignal.rst