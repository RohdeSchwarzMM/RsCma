Estatus
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SENSe:SEQuencer:TPLan:ESTatus

.. code-block:: python

	SENSe:SEQuencer:TPLan:ESTatus



.. autoclass:: RsCma.Implementations.Sense.Sequencer.Tplan.Estatus.EstatusCls
	:members:
	:undoc-members:
	:noindex: