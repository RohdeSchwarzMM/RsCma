Frequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DTMF:FREQuency:RESet
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DTMF:FREQuency

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DTMF:FREQuency:RESet
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DTMF:FREQuency



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Dtmf.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: