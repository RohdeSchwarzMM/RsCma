CfFm
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:PTFive:CFFM:SRATe
	single: SOURce:AFRF:GENerator<Instance>:PTFive:CFFM:FILTer
	single: SOURce:AFRF:GENerator<Instance>:PTFive:CFFM:ROFactor
	single: SOURce:AFRF:GENerator<Instance>:PTFive:CFFM:SDEViation

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:PTFive:CFFM:SRATe
	SOURce:AFRF:GENerator<Instance>:PTFive:CFFM:FILTer
	SOURce:AFRF:GENerator<Instance>:PTFive:CFFM:ROFactor
	SOURce:AFRF:GENerator<Instance>:PTFive:CFFM:SDEViation



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.PtFive.CfFm.CfFmCls
	:members:
	:undoc-members:
	:noindex: