Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:CURRent
	single: CALCulate:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:CURRent
	single: READ:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:CURRent
	CALCulate:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:CURRent
	READ:AFRF:MEASurement<Instance>:DIGital:TTL:BERate:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Ttl.BitErrorRate.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: