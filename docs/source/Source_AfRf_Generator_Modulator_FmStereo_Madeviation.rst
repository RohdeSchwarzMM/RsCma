Madeviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:MODulator:FMSTereo:MADeviation

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:MODulator:FMSTereo:MADeviation



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Modulator.FmStereo.Madeviation.MadeviationCls
	:members:
	:undoc-members:
	:noindex: