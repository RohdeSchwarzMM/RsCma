Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:NRT:REVerse:MAXimum
	single: FETCh:GPRF:MEASurement<Instance>:NRT:REVerse:MAXimum
	single: READ:GPRF:MEASurement<Instance>:NRT:REVerse:MAXimum

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:NRT:REVerse:MAXimum
	FETCh:GPRF:MEASurement<Instance>:NRT:REVerse:MAXimum
	READ:GPRF:MEASurement<Instance>:NRT:REVerse:MAXimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Nrt.Reverse.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: