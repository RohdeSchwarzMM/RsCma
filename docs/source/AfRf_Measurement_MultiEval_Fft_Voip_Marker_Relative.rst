Relative
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:MARKer<mnr>:RELative

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:VOIP:MARKer<mnr>:RELative



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Voip.Marker.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: