Cfrequency
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:BPASs:CFRequency

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:BPASs:CFRequency



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.FilterPy.Bpass.Cfrequency.CfrequencyCls
	:members:
	:undoc-members:
	:noindex: