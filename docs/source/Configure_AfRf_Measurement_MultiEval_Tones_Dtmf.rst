Dtmf
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DTMF:CFGenerator
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DTMF:SLENgth

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DTMF:CFGenerator
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DTMF:SLENgth



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Dtmf.DtmfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.tones.dtmf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Tones_Dtmf_Frequency.rst
	Configure_AfRf_Measurement_MultiEval_Tones_Dtmf_UserDefined.rst