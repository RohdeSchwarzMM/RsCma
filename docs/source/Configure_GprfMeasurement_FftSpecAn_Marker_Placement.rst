Placement
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:MARKer<nr>:PLACement

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:MARKer<nr>:PLACement



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.FftSpecAn.Marker.Placement.PlacementCls
	:members:
	:undoc-members:
	:noindex: