Binary
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:TETRa:SYMBols:BINary
	single: READ:VSE:MEASurement<Instance>:TETRa:SYMBols:BINary

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:TETRa:SYMBols:BINary
	READ:VSE:MEASurement<Instance>:TETRa:SYMBols:BINary



.. autoclass:: RsCma.Implementations.Vse.Measurement.Tetra.Symbols.Binary.BinaryCls
	:members:
	:undoc-members:
	:noindex: