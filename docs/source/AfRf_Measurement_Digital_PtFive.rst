PtFive
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.PtFive.PtFiveCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.digital.ptFive.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_Digital_PtFive_BitErrorRate.rst
	AfRf_Measurement_Digital_PtFive_Power.rst
	AfRf_Measurement_Digital_PtFive_Sinfo.rst