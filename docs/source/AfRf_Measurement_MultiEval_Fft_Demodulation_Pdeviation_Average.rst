Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:PDEViation:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.Pdeviation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: