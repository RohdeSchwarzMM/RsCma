Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:RFCarrier:POWer:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:RFCarrier:POWer:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.RfCarrier.Power.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: