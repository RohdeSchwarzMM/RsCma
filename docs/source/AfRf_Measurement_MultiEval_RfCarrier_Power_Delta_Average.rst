Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Power.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: