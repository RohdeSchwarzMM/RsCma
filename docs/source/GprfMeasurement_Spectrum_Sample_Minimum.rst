Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MINimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MINimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MINimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MINimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Sample.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: