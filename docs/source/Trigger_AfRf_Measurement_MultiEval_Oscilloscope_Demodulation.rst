Demodulation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:SOURce
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:COUPling
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:STATe
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:ENABle
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:OFFSet
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:SLOPe
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:MODE

.. code-block:: python

	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:SOURce
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:COUPling
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:STATe
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:ENABle
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:OFFSet
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:SLOPe
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:MODE



.. autoclass:: RsCma.Implementations.Trigger.AfRf.Measurement.MultiEval.Oscilloscope.Demodulation.DemodulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.afRf.measurement.multiEval.oscilloscope.demodulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_AfRf_Measurement_MultiEval_Oscilloscope_Demodulation_Fdeviation.rst
	Trigger_AfRf_Measurement_MultiEval_Oscilloscope_Demodulation_ModDepth.rst
	Trigger_AfRf_Measurement_MultiEval_Oscilloscope_Demodulation_Pdeviation.rst
	Trigger_AfRf_Measurement_MultiEval_Oscilloscope_Demodulation_Ssb.rst