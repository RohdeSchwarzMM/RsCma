Generator
----------------------------------------





.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.avionics.generator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Avionics_Generator_Ils.rst
	Source_Avionics_Generator_MarkerBeacon.rst
	Source_Avionics_Generator_RfSettings.rst
	Source_Avionics_Generator_Vor.rst