AfRf
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.AfRfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Generator.rst
	Configure_AfRf_Measurement.rst