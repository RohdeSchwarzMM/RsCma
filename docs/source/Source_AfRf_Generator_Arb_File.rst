File
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:ARB:FILE:DATE
	single: SOURce:AFRF:GENerator<Instance>:ARB:FILE:OPTion
	single: SOURce:AFRF:GENerator<Instance>:ARB:FILE:VERSion
	single: SOURce:AFRF:GENerator<Instance>:ARB:FILE

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:ARB:FILE:DATE
	SOURce:AFRF:GENerator<Instance>:ARB:FILE:OPTion
	SOURce:AFRF:GENerator<Instance>:ARB:FILE:VERSion
	SOURce:AFRF:GENerator<Instance>:ARB:FILE



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Arb.File.FileCls
	:members:
	:undoc-members:
	:noindex: