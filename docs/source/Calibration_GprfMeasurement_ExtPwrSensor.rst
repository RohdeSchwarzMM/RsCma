ExtPwrSensor
----------------------------------------





.. autoclass:: RsCma.Implementations.Calibration.GprfMeasurement.ExtPwrSensor.ExtPwrSensorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calibration.gprfMeasurement.extPwrSensor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_GprfMeasurement_ExtPwrSensor_Zero.rst