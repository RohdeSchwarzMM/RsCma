Speak
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:REFMarker:SPEak

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:REFMarker:SPEak



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.ReferenceMarker.Speak.SpeakCls
	:members:
	:undoc-members:
	:noindex: