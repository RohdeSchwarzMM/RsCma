Measured
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FREQuency:DELTa:MEASured

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN<nr>:FREQuency:DELTa:MEASured



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Frequency.Delta.Measured.MeasuredCls
	:members:
	:undoc-members:
	:noindex: