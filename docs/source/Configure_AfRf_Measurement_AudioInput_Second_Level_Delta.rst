Delta
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN:SECond:LEVel:DELTa:USER
	single: CONFigure:AFRF:MEASurement<Instance>:AIN:SECond:LEVel:DELTa:MEASured

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN:SECond:LEVel:DELTa:USER
	CONFigure:AFRF:MEASurement<Instance>:AIN:SECond:LEVel:DELTa:MEASured



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.Second.Level.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex: