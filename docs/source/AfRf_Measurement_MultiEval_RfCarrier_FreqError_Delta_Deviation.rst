Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FERRor:DELTa:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.FreqError.Delta.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: