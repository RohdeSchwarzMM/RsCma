Direction
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:TTL<Index>:DIRection

.. code-block:: python

	CONFigure:BASE:TTL<Index>:DIRection



.. autoclass:: RsCma.Implementations.Configure.Base.Ttl.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex: