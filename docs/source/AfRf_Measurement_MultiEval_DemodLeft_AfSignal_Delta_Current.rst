Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DELTa:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DELTa:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DELTa:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMLeft:AFSignal:DELTa:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodLeft.AfSignal.Delta.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: