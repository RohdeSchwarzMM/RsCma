Aclr
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:LIMit:ACLR

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:ACP:LIMit:ACLR



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Acp.Limit.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex: