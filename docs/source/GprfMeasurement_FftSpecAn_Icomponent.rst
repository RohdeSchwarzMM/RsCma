Icomponent
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: READ:GPRF:MEASurement<Instance>:FFTSanalyzer:I
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:I

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:FFTSanalyzer:I
	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:I



.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.Icomponent.IcomponentCls
	:members:
	:undoc-members:
	:noindex: