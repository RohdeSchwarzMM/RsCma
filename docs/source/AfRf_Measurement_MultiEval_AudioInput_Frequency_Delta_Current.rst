Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN<nr>:FREQuency:DELTa:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN<nr>:FREQuency:DELTa:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN<nr>:FREQuency:DELTa:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN<nr>:FREQuency:DELTa:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.Frequency.Delta.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: