Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEMLeft:FDEViation:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.DemodLeft.Fdeviation.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: