Timeout
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DIALing:TOUT

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DIALing:TOUT



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Dialing.Timeout.TimeoutCls
	:members:
	:undoc-members:
	:noindex: