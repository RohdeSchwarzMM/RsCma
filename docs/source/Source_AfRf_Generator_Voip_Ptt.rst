Ptt
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:VOIP:PTT:STATe
	single: SOURce:AFRF:GENerator<Instance>:VOIP:PTT

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:VOIP:PTT:STATe
	SOURce:AFRF:GENerator<Instance>:VOIP:PTT



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Voip.Ptt.PttCls
	:members:
	:undoc-members:
	:noindex: