Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:SPECtrum:CURRent
	single: READ:VSE:MEASurement<Instance>:SPECtrum:CURRent

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:SPECtrum:CURRent
	READ:VSE:MEASurement<Instance>:SPECtrum:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.Spectrum.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: