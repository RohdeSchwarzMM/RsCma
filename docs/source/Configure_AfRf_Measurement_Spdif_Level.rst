Level
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.spdif.level.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Spdif_Level_Peak.rst
	Configure_AfRf_Measurement_Spdif_Level_Rms.rst