Dcs
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:TONes:DCS:CWORd
	single: SOURce:AFRF:GENerator<Instance>:TONes:DCS:ENABle
	single: SOURce:AFRF:GENerator<Instance>:TONes:DCS:FSKDeviation
	single: SOURce:AFRF:GENerator<Instance>:TONes:DCS:DRATe
	single: SOURce:AFRF:GENerator<Instance>:TONes:DCS:DROFfset
	single: SOURce:AFRF:GENerator<Instance>:TONes:DCS:TOCLength

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:TONes:DCS:CWORd
	SOURce:AFRF:GENerator<Instance>:TONes:DCS:ENABle
	SOURce:AFRF:GENerator<Instance>:TONes:DCS:FSKDeviation
	SOURce:AFRF:GENerator<Instance>:TONes:DCS:DRATe
	SOURce:AFRF:GENerator<Instance>:TONes:DCS:DROFfset
	SOURce:AFRF:GENerator<Instance>:TONes:DCS:TOCLength



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Tones.Dcs.DcsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.tones.dcs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Tones_Dcs_Ifsk.rst
	Source_AfRf_Generator_Tones_Dcs_ToCode.rst