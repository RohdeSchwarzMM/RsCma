Hexadecimal
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:DMR:SYMBols:HEXadecimal
	single: READ:VSE:MEASurement<Instance>:DMR:SYMBols:HEXadecimal

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:DMR:SYMBols:HEXadecimal
	READ:VSE:MEASurement<Instance>:DMR:SYMBols:HEXadecimal



.. autoclass:: RsCma.Implementations.Vse.Measurement.Dmr.Symbols.Hexadecimal.HexadecimalCls
	:members:
	:undoc-members:
	:noindex: