RdsDeviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:MODulator:FMSTereo:RDSDeviation

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:MODulator:FMSTereo:RDSDeviation



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Modulator.FmStereo.RdsDeviation.RdsDeviationCls
	:members:
	:undoc-members:
	:noindex: