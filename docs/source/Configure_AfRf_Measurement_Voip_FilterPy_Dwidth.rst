Dwidth
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:DWIDth
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:DWIDth:SFACtor

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:DWIDth
	CONFigure:AFRF:MEASurement<Instance>:VOIP:FILTer:DWIDth:SFACtor



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.FilterPy.Dwidth.DwidthCls
	:members:
	:undoc-members:
	:noindex: