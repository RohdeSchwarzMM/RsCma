Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:MAXimum
	single: READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:MAXimum
	READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:MAXimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.Power.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: