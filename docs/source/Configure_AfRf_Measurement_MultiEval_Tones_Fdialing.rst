Fdialing
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:CFGenerator
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:EFRequency
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:TTYPe
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:SLENgth
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:MACCuracy
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:DTLength

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:CFGenerator
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:EFRequency
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:TTYPe
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:SLENgth
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:MACCuracy
	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:FDIaling:DTLength



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Fdialing.FdialingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.multiEval.tones.fdialing.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_MultiEval_Tones_Fdialing_Frequency.rst