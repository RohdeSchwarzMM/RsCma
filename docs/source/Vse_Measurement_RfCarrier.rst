RfCarrier
----------------------------------------





.. autoclass:: RsCma.Implementations.Vse.Measurement.RfCarrier.RfCarrierCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.vse.measurement.rfCarrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Vse_Measurement_RfCarrier_Current.rst
	Vse_Measurement_RfCarrier_Maximum.rst