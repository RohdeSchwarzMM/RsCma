Gcoupling
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:GCOupling

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:GCOupling



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.Gcoupling.GcouplingCls
	:members:
	:undoc-members:
	:noindex: