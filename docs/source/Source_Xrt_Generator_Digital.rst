Digital
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:XRT:GENerator<Instance>:DIGital:FILE

.. code-block:: python

	SOURce:XRT:GENerator<Instance>:DIGital:FILE



.. autoclass:: RsCma.Implementations.Source.Xrt.Generator.Digital.DigitalCls
	:members:
	:undoc-members:
	:noindex: