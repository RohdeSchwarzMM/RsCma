Hpass
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:HPASs

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:FILTer:HPASs



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.FilterPy.Hpass.HpassCls
	:members:
	:undoc-members:
	:noindex: