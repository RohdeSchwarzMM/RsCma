Demodulation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation:MODE

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation:MODE



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Tones.Demodulation.DemodulationCls
	:members:
	:undoc-members:
	:noindex: