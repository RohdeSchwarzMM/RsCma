FilterPy
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:FILTer:DISable

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:FILTer:DISable



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_FilterPy_Notch.rst