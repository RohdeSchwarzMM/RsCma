Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:SINRight:AFSignal:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifRight.AfSignal.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: