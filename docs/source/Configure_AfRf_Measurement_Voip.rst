Voip
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:GCOupling
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:ENABle
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:PCODec
	single: CONFigure:AFRF:MEASurement<Instance>:VOIP:FID

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:VOIP:GCOupling
	CONFigure:AFRF:MEASurement<Instance>:VOIP:ENABle
	CONFigure:AFRF:MEASurement<Instance>:VOIP:PCODec
	CONFigure:AFRF:MEASurement<Instance>:VOIP:FID



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Voip.VoipCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.voip.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Voip_FilterPy.rst
	Configure_AfRf_Measurement_Voip_Frequency.rst
	Configure_AfRf_Measurement_Voip_Level.rst
	Configure_AfRf_Measurement_Voip_Rssi.rst
	Configure_AfRf_Measurement_Voip_Sip.rst
	Configure_AfRf_Measurement_Voip_Squelch.rst
	Configure_AfRf_Measurement_Voip_Uri.rst