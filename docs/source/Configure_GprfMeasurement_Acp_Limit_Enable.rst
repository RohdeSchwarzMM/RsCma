Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:ACP:LIMit:ENABle

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:ACP:LIMit:ENABle



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Acp.Limit.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: