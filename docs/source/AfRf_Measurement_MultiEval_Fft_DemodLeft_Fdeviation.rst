Fdeviation
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.DemodLeft.Fdeviation.FdeviationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.fft.demodLeft.fdeviation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Fft_DemodLeft_Fdeviation_Average.rst
	AfRf_Measurement_MultiEval_Fft_DemodLeft_Fdeviation_Current.rst
	AfRf_Measurement_MultiEval_Fft_DemodLeft_Fdeviation_Maximum.rst
	AfRf_Measurement_MultiEval_Fft_DemodLeft_Fdeviation_Minimum.rst