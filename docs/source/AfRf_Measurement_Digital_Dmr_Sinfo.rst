Sinfo
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:DMR:SINFo
	single: READ:AFRF:MEASurement<Instance>:DIGital:DMR:SINFo

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:DMR:SINFo
	READ:AFRF:MEASurement<Instance>:DIGital:DMR:SINFo



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Dmr.Sinfo.SinfoCls
	:members:
	:undoc-members:
	:noindex: