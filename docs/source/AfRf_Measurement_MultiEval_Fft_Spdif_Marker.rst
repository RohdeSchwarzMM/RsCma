Marker
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SIN<nr>:MARKer<mnr>

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SIN<nr>:MARKer<mnr>



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Spdif.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.fft.spdif.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Fft_Spdif_Marker_Absolute.rst
	AfRf_Measurement_MultiEval_Fft_Spdif_Marker_Relative.rst