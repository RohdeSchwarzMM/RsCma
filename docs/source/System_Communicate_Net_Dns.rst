Dns
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:COMMunicate:NET:DNS:ENABle

.. code-block:: python

	SYSTem:COMMunicate:NET:DNS:ENABle



.. autoclass:: RsCma.Implementations.System.Communicate.Net.Dns.DnsCls
	:members:
	:undoc-members:
	:noindex: