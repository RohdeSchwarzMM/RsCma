Power
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:REVerse:LIMit:POWer

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRT:REVerse:LIMit:POWer



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.Reverse.Limit.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: