System
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:PRESet
	single: SYSTem:PRESet:ALL
	single: SYSTem:PRESet:BASE
	single: SYSTem:RESet
	single: SYSTem:RESet:ALL
	single: SYSTem:RESet:BASE

.. code-block:: python

	SYSTem:PRESet
	SYSTem:PRESet:ALL
	SYSTem:PRESet:BASE
	SYSTem:RESet
	SYSTem:RESet:ALL
	SYSTem:RESet:BASE



.. autoclass:: RsCma.Implementations.System.SystemCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Base.rst
	System_Communicate.rst
	System_DeviceFootprint.rst
	System_Display.rst
	System_Error.rst
	System_Help.rst
	System_Option.rst
	System_Password.rst
	System_Update.rst