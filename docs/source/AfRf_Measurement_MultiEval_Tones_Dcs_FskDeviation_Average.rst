Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.FskDeviation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: