Relay<Relay>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix2
	rc = driver.configure.base.relay.repcap_relay_get()
	driver.configure.base.relay.repcap_relay_set(repcap.Relay.Ix1)



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:RELay<Index>

.. code-block:: python

	CONFigure:BASE:RELay<Index>



.. autoclass:: RsCma.Implementations.Configure.Base.Relay.RelayCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.base.relay.clone()