Attenuation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:ATTenuation:STATe
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:ATTenuation

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:EPSensor:ATTenuation:STATe
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:ATTenuation



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.ExtPwrSensor.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex: