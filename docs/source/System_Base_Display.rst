Display
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:DISPlay:LANGuage

.. code-block:: python

	SYSTem:BASE:DISPlay:LANGuage



.. autoclass:: RsCma.Implementations.System.Base.Display.DisplayCls
	:members:
	:undoc-members:
	:noindex: