Spdif
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.SpdifCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.spdif.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Spdif_Enable.rst
	Configure_AfRf_Measurement_Spdif_FilterPy.rst
	Configure_AfRf_Measurement_Spdif_Frequency.rst
	Configure_AfRf_Measurement_Spdif_Gcoupling.rst
	Configure_AfRf_Measurement_Spdif_Level.rst
	Configure_AfRf_Measurement_Spdif_Tmode.rst