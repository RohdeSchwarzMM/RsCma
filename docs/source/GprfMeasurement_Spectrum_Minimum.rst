Minimum
----------------------------------------





.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.spectrum.minimum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Spectrum_Minimum_Average.rst
	GprfMeasurement_Spectrum_Minimum_Current.rst
	GprfMeasurement_Spectrum_Minimum_Maximum.rst
	GprfMeasurement_Spectrum_Minimum_Minimum.rst