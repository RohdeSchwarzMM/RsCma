Vxi
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:COMMunicate:VXI:VRESource
	single: SYSTem:COMMunicate:VXI:GTR

.. code-block:: python

	SYSTem:COMMunicate:VXI:VRESource
	SYSTem:COMMunicate:VXI:GTR



.. autoclass:: RsCma.Implementations.System.Communicate.Vxi.VxiCls
	:members:
	:undoc-members:
	:noindex: