GprfMeasurement
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:CREPetition

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:CREPetition



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.GprfMeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_Acp.rst
	Configure_GprfMeasurement_ExtPwrSensor.rst
	Configure_GprfMeasurement_FftSpecAn.rst
	Configure_GprfMeasurement_IqRecorder.rst
	Configure_GprfMeasurement_Nrt.rst
	Configure_GprfMeasurement_Power.rst
	Configure_GprfMeasurement_RfSettings.rst
	Configure_GprfMeasurement_Spectrum.rst