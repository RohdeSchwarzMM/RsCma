Fdeviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:FDEViation:THReshold

.. code-block:: python

	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:FDEViation:THReshold



.. autoclass:: RsCma.Implementations.Trigger.AfRf.Measurement.MultiEval.Oscilloscope.Demodulation.Fdeviation.FdeviationCls
	:members:
	:undoc-members:
	:noindex: