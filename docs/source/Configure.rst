Configure
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.ConfigureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf.rst
	Configure_Base.rst
	Configure_Display.rst
	Configure_GprfMeasurement.rst
	Configure_Sequencer.rst
	Configure_Vse.rst