Result
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:VSE:MEASurement<Instance>:RESult:EDIagram
	single: CONFigure:VSE:MEASurement<Instance>:RESult:PVTime
	single: CONFigure:VSE:MEASurement<Instance>:RESult:CONS
	single: CONFigure:VSE:MEASurement<Instance>:RESult:SDIS

.. code-block:: python

	CONFigure:VSE:MEASurement<Instance>:RESult:EDIagram
	CONFigure:VSE:MEASurement<Instance>:RESult:PVTime
	CONFigure:VSE:MEASurement<Instance>:RESult:CONS
	CONFigure:VSE:MEASurement<Instance>:RESult:SDIS



.. autoclass:: RsCma.Implementations.Configure.Vse.Measurement.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: