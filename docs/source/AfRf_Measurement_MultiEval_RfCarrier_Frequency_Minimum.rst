Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Frequency.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: