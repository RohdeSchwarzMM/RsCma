Sequencer
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.Sequencer.SequencerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sequencer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sequencer_Tplan.rst