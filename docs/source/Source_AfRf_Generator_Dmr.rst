Dmr
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DMR:PATTern
	single: SOURce:AFRF:GENerator<Instance>:DMR:SVALue
	single: SOURce:AFRF:GENerator<Instance>:DMR:CCODe
	single: SOURce:AFRF:GENerator<Instance>:DMR:SADDress
	single: SOURce:AFRF:GENerator<Instance>:DMR:GADDress
	single: SOURce:AFRF:GENerator<Instance>:DMR:MODE
	single: SOURce:AFRF:GENerator<Instance>:DMR:SDEViation
	single: SOURce:AFRF:GENerator<Instance>:DMR:SRATe
	single: SOURce:AFRF:GENerator<Instance>:DMR:FILTer
	single: SOURce:AFRF:GENerator<Instance>:DMR:ROFactor

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DMR:PATTern
	SOURce:AFRF:GENerator<Instance>:DMR:SVALue
	SOURce:AFRF:GENerator<Instance>:DMR:CCODe
	SOURce:AFRF:GENerator<Instance>:DMR:SADDress
	SOURce:AFRF:GENerator<Instance>:DMR:GADDress
	SOURce:AFRF:GENerator<Instance>:DMR:MODE
	SOURce:AFRF:GENerator<Instance>:DMR:SDEViation
	SOURce:AFRF:GENerator<Instance>:DMR:SRATe
	SOURce:AFRF:GENerator<Instance>:DMR:FILTer
	SOURce:AFRF:GENerator<Instance>:DMR:ROFactor



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dmr.DmrCls
	:members:
	:undoc-members:
	:noindex: