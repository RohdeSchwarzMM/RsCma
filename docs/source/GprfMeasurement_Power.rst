Power
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:POWer
	single: STOP:GPRF:MEASurement<Instance>:POWer
	single: ABORt:GPRF:MEASurement<Instance>:POWer

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:POWer
	STOP:GPRF:MEASurement<Instance>:POWer
	ABORt:GPRF:MEASurement<Instance>:POWer



.. autoclass:: RsCma.Implementations.GprfMeasurement.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_Power_Average.rst
	GprfMeasurement_Power_Current.rst
	GprfMeasurement_Power_ElapsedStats.rst
	GprfMeasurement_Power_Maximum.rst
	GprfMeasurement_Power_Minimum.rst
	GprfMeasurement_Power_Peak.rst
	GprfMeasurement_Power_StandardDev.rst
	GprfMeasurement_Power_State.rst