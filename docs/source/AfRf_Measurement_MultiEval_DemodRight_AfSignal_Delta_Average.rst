Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:DEMRight:AFSignal:DELTa:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.DemodRight.AfSignal.Delta.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: