Tx
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:MODE
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:SETime
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:MLEVel
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:AFSource
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:RFDeviation
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:RPDeviation
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:RMDepth

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:MODE
	CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:SETime
	CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:MLEVel
	CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:AFSource
	CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:RFDeviation
	CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:RPDeviation
	CONFigure:AFRF:MEASurement<Instance>:SROutines:TX:RMDepth



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Tx.TxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.searchRoutines.tx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_SearchRoutines_Tx_Demod.rst