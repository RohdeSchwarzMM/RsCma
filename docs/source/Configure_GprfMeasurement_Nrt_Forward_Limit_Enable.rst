Enable
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRT:FWARd:LIMit:ENABle

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRT:FWARd:LIMit:ENABle



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.Forward.Limit.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: