Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SINRight:PVTime:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SINRight:PVTime:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SINRight:PVTime:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:SINRight:PVTime:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.SpdifRight.PowerVsTime.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: