Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:FDEViation:TRACe
	single: READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:FDEViation:TRACe

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:FDEViation:TRACe
	READ:AFRF:MEASurement<Instance>:SROutines:TSENsitivity:FDEViation:TRACe



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Tsensitivity.Fdeviation.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: