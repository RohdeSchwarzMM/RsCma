LsbPower
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.LsbPower.LsbPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.fft.demodulation.lsbPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Fft_Demodulation_LsbPower_Average.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation_LsbPower_Current.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation_LsbPower_Maximum.rst
	AfRf_Measurement_MultiEval_Fft_Demodulation_LsbPower_Minimum.rst