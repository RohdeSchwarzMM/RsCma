Extreme
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:EXTReme
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:EXTReme
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:EXTReme

.. code-block:: python

	CALCulate:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:EXTReme
	FETCh:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:EXTReme
	READ:AFRF:MEASurement<Instance>:MEValuation:SQUality:DEMRight:EXTReme



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SignalQuality.DemodRight.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: