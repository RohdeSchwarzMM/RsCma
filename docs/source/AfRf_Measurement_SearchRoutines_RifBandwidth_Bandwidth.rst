Bandwidth
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:BANDwidth
	single: READ:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:BANDwidth

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:BANDwidth
	READ:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:BANDwidth



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.RifBandwidth.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: