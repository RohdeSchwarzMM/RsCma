Level
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:MTONe:LEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator<nr>:MTONe:LEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.MultiTone.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: