Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:MAXimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:MAXimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:MAXimum



.. autoclass:: RsCma.Implementations.GprfMeasurement.Spectrum.Rms.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: