Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:LEVel:DELTa:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:LEVel:DELTa:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:LEVel:DELTa:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN:FIRSt:LEVel:DELTa:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.First.Level.Delta.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: