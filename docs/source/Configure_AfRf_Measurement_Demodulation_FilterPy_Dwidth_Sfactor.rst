Sfactor
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:DWIDth:SFACtor

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FILTer:DWIDth:SFACtor



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.FilterPy.Dwidth.Sfactor.SfactorCls
	:members:
	:undoc-members:
	:noindex: