AfSignal
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifRight.AfSignal.AfSignalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.spdifRight.afSignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_SpdifRight_AfSignal_Average.rst
	AfRf_Measurement_MultiEval_SpdifRight_AfSignal_Current.rst
	AfRf_Measurement_MultiEval_SpdifRight_AfSignal_Deviation.rst
	AfRf_Measurement_MultiEval_SpdifRight_AfSignal_Maximum.rst