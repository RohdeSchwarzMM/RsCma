Application
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: DISPlay:AFRF:MEASurement<Instance>:ROUTines:APPLication:SELect

.. code-block:: python

	DISPlay:AFRF:MEASurement<Instance>:ROUTines:APPLication:SELect



.. autoclass:: RsCma.Implementations.Display.AfRf.Measurement.Routines.Application.ApplicationCls
	:members:
	:undoc-members:
	:noindex: