Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:SINLeft:AFSignal:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifLeft.AfSignal.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: