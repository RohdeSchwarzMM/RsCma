RfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:CONNector
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:FREQuency
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:ENPower
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:EATTenuation
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:RFCoupling

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:RFSettings:CONNector
	CONFigure:GPRF:MEASurement<Instance>:RFSettings:FREQuency
	CONFigure:GPRF:MEASurement<Instance>:RFSettings:ENPower
	CONFigure:GPRF:MEASurement<Instance>:RFSettings:EATTenuation
	CONFigure:GPRF:MEASurement<Instance>:RFSettings:RFCoupling



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: