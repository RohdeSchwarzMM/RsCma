Voip
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.AfRf.Generator.Voip.VoipCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.generator.voip.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Generator_Voip_Frequency.rst