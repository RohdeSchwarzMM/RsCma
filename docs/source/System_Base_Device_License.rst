License
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:DEVice:LICense

.. code-block:: python

	SYSTem:BASE:DEVice:LICense



.. autoclass:: RsCma.Implementations.System.Base.Device.License.LicenseCls
	:members:
	:undoc-members:
	:noindex: