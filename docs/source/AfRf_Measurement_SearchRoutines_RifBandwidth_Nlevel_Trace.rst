Trace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:NLEVel:TRACe
	single: READ:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:NLEVel:TRACe

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:NLEVel:TRACe
	READ:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:NLEVel:TRACe



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.RifBandwidth.Nlevel.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex: