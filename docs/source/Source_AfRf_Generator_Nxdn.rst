Nxdn
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:NXDN:PATTern
	single: SOURce:AFRF:GENerator<Instance>:NXDN:SVALue
	single: SOURce:AFRF:GENerator<Instance>:NXDN:RAN
	single: SOURce:AFRF:GENerator<Instance>:NXDN:SUID
	single: SOURce:AFRF:GENerator<Instance>:NXDN:DUID
	single: SOURce:AFRF:GENerator<Instance>:NXDN:TRANsmission
	single: SOURce:AFRF:GENerator<Instance>:NXDN:MODE
	single: SOURce:AFRF:GENerator<Instance>:NXDN:SDEViation
	single: SOURce:AFRF:GENerator<Instance>:NXDN:SRATe
	single: SOURce:AFRF:GENerator<Instance>:NXDN:FILTer
	single: SOURce:AFRF:GENerator<Instance>:NXDN:ROFactor

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:NXDN:PATTern
	SOURce:AFRF:GENerator<Instance>:NXDN:SVALue
	SOURce:AFRF:GENerator<Instance>:NXDN:RAN
	SOURce:AFRF:GENerator<Instance>:NXDN:SUID
	SOURce:AFRF:GENerator<Instance>:NXDN:DUID
	SOURce:AFRF:GENerator<Instance>:NXDN:TRANsmission
	SOURce:AFRF:GENerator<Instance>:NXDN:MODE
	SOURce:AFRF:GENerator<Instance>:NXDN:SDEViation
	SOURce:AFRF:GENerator<Instance>:NXDN:SRATe
	SOURce:AFRF:GENerator<Instance>:NXDN:FILTer
	SOURce:AFRF:GENerator<Instance>:NXDN:ROFactor



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Nxdn.NxdnCls
	:members:
	:undoc-members:
	:noindex: