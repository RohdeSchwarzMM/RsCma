BwDisplace
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:RIFBandwidth:BWDisplace

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:RIFBandwidth:BWDisplace



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Limit.RifBandwidth.BwDisplace.BwDisplaceCls
	:members:
	:undoc-members:
	:noindex: