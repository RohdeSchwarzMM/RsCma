Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Power.Delta.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: