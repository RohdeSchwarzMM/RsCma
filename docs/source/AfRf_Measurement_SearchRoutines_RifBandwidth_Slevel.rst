Slevel
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:SLEVel
	single: READ:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:SLEVel

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:SLEVel
	READ:AFRF:MEASurement<Instance>:SROutines:RIFBandwidth:SLEVel



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.RifBandwidth.Slevel.SlevelCls
	:members:
	:undoc-members:
	:noindex: