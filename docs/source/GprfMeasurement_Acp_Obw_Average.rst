Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:ACP:OBW:AVERage
	single: READ:GPRF:MEASurement<Instance>:ACP:OBW:AVERage
	single: CALCulate:GPRF:MEASurement<Instance>:ACP:OBW:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:ACP:OBW:AVERage
	READ:GPRF:MEASurement<Instance>:ACP:OBW:AVERage
	CALCulate:GPRF:MEASurement<Instance>:ACP:OBW:AVERage



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Obw.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: