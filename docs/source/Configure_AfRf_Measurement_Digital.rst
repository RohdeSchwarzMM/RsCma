Digital
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:SCOunt
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:STANdard
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:REPetition
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:SCONdition
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:CREPetition
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:MOEXception
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:RCOupling
	single: CONFigure:AFRF:MEASurement<Instance>:DIGital:TOUT

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DIGital:SCOunt
	CONFigure:AFRF:MEASurement<Instance>:DIGital:STANdard
	CONFigure:AFRF:MEASurement<Instance>:DIGital:REPetition
	CONFigure:AFRF:MEASurement<Instance>:DIGital:SCONdition
	CONFigure:AFRF:MEASurement<Instance>:DIGital:CREPetition
	CONFigure:AFRF:MEASurement<Instance>:DIGital:MOEXception
	CONFigure:AFRF:MEASurement<Instance>:DIGital:RCOupling
	CONFigure:AFRF:MEASurement<Instance>:DIGital:TOUT



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Digital.DigitalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.digital.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Digital_Dmr.rst
	Configure_AfRf_Measurement_Digital_Limit.rst
	Configure_AfRf_Measurement_Digital_PtFive.rst
	Configure_AfRf_Measurement_Digital_Result.rst
	Configure_AfRf_Measurement_Digital_Rf.rst
	Configure_AfRf_Measurement_Digital_Sync.rst
	Configure_AfRf_Measurement_Digital_Tetra.rst
	Configure_AfRf_Measurement_Digital_Ttl.rst