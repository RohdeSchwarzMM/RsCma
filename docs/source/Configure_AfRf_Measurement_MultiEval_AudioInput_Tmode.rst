Tmode
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:TMODe

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:TMODe



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.AudioInput.Tmode.TmodeCls
	:members:
	:undoc-members:
	:noindex: