Voip
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Voip.VoipCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.tones.voip.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Tones_Voip_Repetitions.rst
	AfRf_Measurement_MultiEval_Tones_Voip_Sequence.rst