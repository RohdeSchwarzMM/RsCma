Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:MERRor:CURRent
	single: READ:VSE:MEASurement<Instance>:MERRor:CURRent
	single: CALCulate:VSE:MEASurement<Instance>:MERRor:CURRent

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:MERRor:CURRent
	READ:VSE:MEASurement<Instance>:MERRor:CURRent
	CALCulate:VSE:MEASurement<Instance>:MERRor:CURRent



.. autoclass:: RsCma.Implementations.Vse.Measurement.Merror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: