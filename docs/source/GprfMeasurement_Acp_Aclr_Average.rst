Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:ACP:ACLR:AVERage
	single: READ:GPRF:MEASurement<Instance>:ACP:ACLR:AVERage
	single: CALCulate:GPRF:MEASurement<Instance>:ACP:ACLR:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:ACP:ACLR:AVERage
	READ:GPRF:MEASurement<Instance>:ACP:ACLR:AVERage
	CALCulate:GPRF:MEASurement<Instance>:ACP:ACLR:AVERage



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Aclr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: