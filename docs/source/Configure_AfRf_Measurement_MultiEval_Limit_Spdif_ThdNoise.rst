ThdNoise
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:SIN:THDNoise

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:SIN:THDNoise



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Spdif.ThdNoise.ThdNoiseCls
	:members:
	:undoc-members:
	:noindex: