Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Frequency.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: