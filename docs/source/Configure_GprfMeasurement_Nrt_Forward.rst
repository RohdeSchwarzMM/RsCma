Forward
----------------------------------------





.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Nrt.Forward.ForwardCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprfMeasurement.nrt.forward.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_GprfMeasurement_Nrt_Forward_Limit.rst
	Configure_GprfMeasurement_Nrt_Forward_Value.rst