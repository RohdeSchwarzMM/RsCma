Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DCS:FSKDeviation:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Dcs.FskDeviation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: