Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:LSBPower:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.LsbPower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: