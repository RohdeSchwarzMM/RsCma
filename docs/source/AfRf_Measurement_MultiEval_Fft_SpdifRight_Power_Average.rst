Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:SINRight:POWer:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.SpdifRight.Power.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: