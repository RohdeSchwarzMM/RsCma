Gauss
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:FILTer:GAUSs:BWIDth

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:FILTer:GAUSs:BWIDth



.. autoclass:: RsCma.Implementations.Configure.GprfMeasurement.Power.FilterPy.Gauss.GaussCls
	:members:
	:undoc-members:
	:noindex: