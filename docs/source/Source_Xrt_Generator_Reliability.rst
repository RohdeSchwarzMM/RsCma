Reliability
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:XRT:GENerator<Instance>:RELiability
	single: SOURce:XRT:GENerator<Instance>:RELiability:ALL

.. code-block:: python

	SOURce:XRT:GENerator<Instance>:RELiability
	SOURce:XRT:GENerator<Instance>:RELiability:ALL



.. autoclass:: RsCma.Implementations.Source.Xrt.Generator.Reliability.ReliabilityCls
	:members:
	:undoc-members:
	:noindex: