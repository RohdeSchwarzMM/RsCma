Attenuator
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:AIN<nr>:ZBOX:ATTenuator

.. code-block:: python

	CONFigure:BASE:AIN<nr>:ZBOX:ATTenuator



.. autoclass:: RsCma.Implementations.Configure.Base.AudioInput.Zbox.Attenuator.AttenuatorCls
	:members:
	:undoc-members:
	:noindex: