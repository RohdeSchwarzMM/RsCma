Arb
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:ARB:CRATe
	single: SOURce:AFRF:GENerator<Instance>:ARB:CRCProtect
	single: SOURce:AFRF:GENerator<Instance>:ARB:FOFFset
	single: SOURce:AFRF:GENerator<Instance>:ARB:LOFFset
	single: SOURce:AFRF:GENerator<Instance>:ARB:POFFset
	single: SOURce:AFRF:GENerator<Instance>:ARB:REPetition

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:ARB:CRATe
	SOURce:AFRF:GENerator<Instance>:ARB:CRCProtect
	SOURce:AFRF:GENerator<Instance>:ARB:FOFFset
	SOURce:AFRF:GENerator<Instance>:ARB:LOFFset
	SOURce:AFRF:GENerator<Instance>:ARB:POFFset
	SOURce:AFRF:GENerator<Instance>:ARB:REPetition



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Arb.ArbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.afRf.generator.arb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AfRf_Generator_Arb_File.rst
	Source_AfRf_Generator_Arb_Marker.rst
	Source_AfRf_Generator_Arb_Samples.rst