Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:FREQuency:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Frequency.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: