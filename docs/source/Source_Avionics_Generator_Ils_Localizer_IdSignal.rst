IdSignal
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:IDSignal:ENABle
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:IDSignal:MDEPth
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:IDSignal:FREQuency

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:IDSignal:ENABle
	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:IDSignal:MDEPth
	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:IDSignal:FREQuency



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Localizer.IdSignal.IdSignalCls
	:members:
	:undoc-members:
	:noindex: