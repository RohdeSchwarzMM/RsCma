Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:DEModulation:USBPower:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.Demodulation.UsbPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: