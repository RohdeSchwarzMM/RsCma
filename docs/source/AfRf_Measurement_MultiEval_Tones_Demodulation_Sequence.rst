Sequence
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation:SEQuence
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation:SEQuence

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation:SEQuence
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:DEModulation:SEQuence



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Demodulation.Sequence.SequenceCls
	:members:
	:undoc-members:
	:noindex: