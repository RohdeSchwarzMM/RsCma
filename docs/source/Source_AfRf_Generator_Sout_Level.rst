Level
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:SOUT:LEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:SOUT:LEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Sout.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: