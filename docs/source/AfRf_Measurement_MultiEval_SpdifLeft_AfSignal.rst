AfSignal
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.SpdifLeft.AfSignal.AfSignalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.spdifLeft.afSignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_SpdifLeft_AfSignal_Average.rst
	AfRf_Measurement_MultiEval_SpdifLeft_AfSignal_Current.rst
	AfRf_Measurement_MultiEval_SpdifLeft_AfSignal_Deviation.rst
	AfRf_Measurement_MultiEval_SpdifLeft_AfSignal_Maximum.rst