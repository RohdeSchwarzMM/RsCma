Dtone
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:FREQuency<Nr>:DTONe

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:DIALing:FDIaling:FREQuency<Nr>:DTONe



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.Dialing.Fdialing.Frequency.Dtone.DtoneCls
	:members:
	:undoc-members:
	:noindex: