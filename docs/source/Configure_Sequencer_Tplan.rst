Tplan
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:SEQuencer:TPLan:RUN
	single: CONFigure:SEQuencer:TPLan:ABORt

.. code-block:: python

	CONFigure:SEQuencer:TPLan:RUN
	CONFigure:SEQuencer:TPLan:ABORt



.. autoclass:: RsCma.Implementations.Configure.Sequencer.Tplan.TplanCls
	:members:
	:undoc-members:
	:noindex: