Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:EVM:MAXimum
	single: READ:VSE:MEASurement<Instance>:EVM:MAXimum
	single: CALCulate:VSE:MEASurement<Instance>:EVM:MAXimum

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:EVM:MAXimum
	READ:VSE:MEASurement<Instance>:EVM:MAXimum
	CALCulate:VSE:MEASurement<Instance>:EVM:MAXimum



.. autoclass:: RsCma.Implementations.Vse.Measurement.Evm.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: