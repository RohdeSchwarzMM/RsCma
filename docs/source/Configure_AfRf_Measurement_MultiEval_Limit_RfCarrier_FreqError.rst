FreqError
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:RFCarrier:FERRor

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:RFCarrier:FERRor



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.RfCarrier.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex: