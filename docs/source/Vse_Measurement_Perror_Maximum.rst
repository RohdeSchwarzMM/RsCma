Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:VSE:MEASurement<Instance>:PERRor:MAXimum
	single: READ:VSE:MEASurement<Instance>:PERRor:MAXimum
	single: CALCulate:VSE:MEASurement<Instance>:PERRor:MAXimum

.. code-block:: python

	FETCh:VSE:MEASurement<Instance>:PERRor:MAXimum
	READ:VSE:MEASurement<Instance>:PERRor:MAXimum
	CALCulate:VSE:MEASurement<Instance>:PERRor:MAXimum



.. autoclass:: RsCma.Implementations.Vse.Measurement.Perror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: