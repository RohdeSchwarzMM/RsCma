Dmr
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Dmr.DmrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.digital.dmr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_Digital_Dmr_BitErrorRate.rst
	AfRf_Measurement_Digital_Dmr_PoOff.rst
	AfRf_Measurement_Digital_Dmr_Power.rst
	AfRf_Measurement_Digital_Dmr_Sinfo.rst