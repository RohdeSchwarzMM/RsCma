Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.ModDepth.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: