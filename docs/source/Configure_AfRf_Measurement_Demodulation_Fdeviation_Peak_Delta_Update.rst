Update
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:DEModulation:FDEViation:PEAK:DELTa:UPDate

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:DEModulation:FDEViation:PEAK:DELTa:UPDate



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Demodulation.Fdeviation.Peak.Delta.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: