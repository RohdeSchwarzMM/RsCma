Delta
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:AIN:FIRSt:LEVel:DELTa:USER
	single: CONFigure:AFRF:MEASurement<Instance>:AIN:FIRSt:LEVel:DELTa:MEASured

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:AIN:FIRSt:LEVel:DELTa:USER
	CONFigure:AFRF:MEASurement<Instance>:AIN:FIRSt:LEVel:DELTa:MEASured



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.AudioInput.First.Level.Delta.DeltaCls
	:members:
	:undoc-members:
	:noindex: