RepCaps
=========

Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Range:
	Inst1 .. Inst32
	# All values (32x):
	Inst1 | Inst2 | Inst3 | Inst4 | Inst5 | Inst6 | Inst7 | Inst8
	Inst9 | Inst10 | Inst11 | Inst12 | Inst13 | Inst14 | Inst15 | Inst16
	Inst17 | Inst18 | Inst19 | Inst20 | Inst21 | Inst22 | Inst23 | Inst24
	Inst25 | Inst26 | Inst27 | Inst28 | Inst29 | Inst30 | Inst31 | Inst32

AudioInput
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.AudioInput.Nr1
	# Values (2x):
	Nr1 | Nr2

AudioOutput
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.AudioOutput.Nr1
	# Values (2x):
	Nr1 | Nr2

Battery
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Battery.Ix1
	# Values (2x):
	Ix1 | Ix2

Bit
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Bit.Nr8
	# Range:
	Nr8 .. Nr12
	# All values (5x):
	Nr8 | Nr9 | Nr10 | Nr11 | Nr12

Channel
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Channel.Nr1
	# Values (2x):
	Nr1 | Nr2

Connector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Connector.Nr1
	# Range:
	Nr1 .. Nr8
	# All values (8x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8

FrequencyLobe
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.FrequencyLobe.Nr1
	# Values (2x):
	Nr1 | Nr2

Instrument
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Instrument.Nr1
	# Values (2x):
	Nr1 | Nr2

InternalGen
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.InternalGen.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

Marker
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Marker.Nr1
	# Range:
	Nr1 .. Nr5
	# All values (5x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5

MarkerOther
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.MarkerOther.Nr2
	# Values (4x):
	Nr2 | Nr3 | Nr4 | Nr5

Notch
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Notch.Nr1
	# Values (3x):
	Nr1 | Nr2 | Nr3

Relay
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Relay.Ix1
	# Values (2x):
	Ix1 | Ix2

ToneNumber
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.ToneNumber.Nr1
	# Range:
	Nr1 .. Nr20
	# All values (20x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20

TTL
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.TTL.Ix1
	# Values (2x):
	Ix1 | Ix2

Window
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Window.Win1
	# Range:
	Win1 .. Win32
	# All values (32x):
	Win1 | Win2 | Win3 | Win4 | Win5 | Win6 | Win7 | Win8
	Win9 | Win10 | Win11 | Win12 | Win13 | Win14 | Win15 | Win16
	Win17 | Win18 | Win19 | Win20 | Win21 | Win22 | Win23 | Win24
	Win25 | Win26 | Win27 | Win28 | Win29 | Win30 | Win31 | Win32

