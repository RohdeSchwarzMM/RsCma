MultiTone
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AFRF:GENerator<Instance>:IGENerator:SECond:MTONe:TLEVel

.. code-block:: python

	SOURce:AFRF:GENerator<Instance>:IGENerator:SECond:MTONe:TLEVel



.. autoclass:: RsCma.Implementations.Source.AfRf.Generator.InternalGenerator.Second.MultiTone.MultiToneCls
	:members:
	:undoc-members:
	:noindex: