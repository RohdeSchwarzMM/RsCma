Rsensitivity
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:SROutines:RSENsitivity
	single: CALCulate:AFRF:MEASurement<Instance>:SROutines:RSENsitivity
	single: READ:AFRF:MEASurement<Instance>:SROutines:RSENsitivity

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:SROutines:RSENsitivity
	CALCulate:AFRF:MEASurement<Instance>:SROutines:RSENsitivity
	READ:AFRF:MEASurement<Instance>:SROutines:RSENsitivity



.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsensitivity.RsensitivityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.searchRoutines.rsensitivity.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_SearchRoutines_Rsensitivity_RfLevel.rst
	AfRf_Measurement_SearchRoutines_Rsensitivity_Sensitivity.rst
	AfRf_Measurement_SearchRoutines_Rsensitivity_SignalQuality.rst