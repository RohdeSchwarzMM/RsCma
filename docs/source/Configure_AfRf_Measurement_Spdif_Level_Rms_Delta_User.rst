User
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:RMS:DELTa:USER

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SIN:LEVel:RMS:DELTa:USER



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Spdif.Level.Rms.Delta.User.UserCls
	:members:
	:undoc-members:
	:noindex: