Level
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SOUT:LEVel

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SOUT:LEVel



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Sout.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: