AudioInput<AudioInput>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.afRf.measurement.multiEval.oscilloscope.audioInput.repcap_audioInput_get()
	driver.afRf.measurement.multiEval.oscilloscope.audioInput.repcap_audioInput_set(repcap.AudioInput.Nr1)





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.AudioInput.AudioInputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.oscilloscope.audioInput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Oscilloscope_AudioInput_PowerVsTime.rst