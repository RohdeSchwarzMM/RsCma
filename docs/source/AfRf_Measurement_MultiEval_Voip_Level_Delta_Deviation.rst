Deviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:LEVel:DELTa:DEViation
	single: READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:LEVel:DELTa:DEViation

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:VOIP:LEVel:DELTa:DEViation
	READ:AFRF:MEASurement<Instance>:MEValuation:VOIP:LEVel:DELTa:DEViation



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Voip.Level.Delta.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex: