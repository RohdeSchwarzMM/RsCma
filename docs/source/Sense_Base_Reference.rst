Reference
----------------------------------------





.. autoclass:: RsCma.Implementations.Sense.Base.Reference.ReferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.base.reference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Base_Reference_Frequency.rst