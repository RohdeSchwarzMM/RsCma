StandardDev
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:SDEViation
	single: FETCh:GPRF:MEASurement<Instance>:POWer:SDEViation
	single: READ:GPRF:MEASurement<Instance>:POWer:SDEViation

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:SDEViation
	FETCh:GPRF:MEASurement<Instance>:POWer:SDEViation
	READ:GPRF:MEASurement<Instance>:POWer:SDEViation



.. autoclass:: RsCma.Implementations.GprfMeasurement.Power.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: