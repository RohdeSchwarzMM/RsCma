Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:RFCarrier:POWer:DELTa:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.Power.Delta.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: