FreqError
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.RfCarrier.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.rfCarrier.freqError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_RfCarrier_FreqError_Delta.rst