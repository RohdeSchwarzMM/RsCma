Marker
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:MARKer<nr>

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:MARKer<nr>



.. autoclass:: RsCma.Implementations.GprfMeasurement.FftSpecAn.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprfMeasurement.fftSpecAn.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	GprfMeasurement_FftSpecAn_Marker_Absolute.rst
	GprfMeasurement_FftSpecAn_Marker_Relative.rst