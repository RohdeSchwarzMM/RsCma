Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:DIGital:TETRa:POWer:CURRent
	single: READ:AFRF:MEASurement<Instance>:DIGital:TETRa:POWer:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:DIGital:TETRa:POWer:CURRent
	READ:AFRF:MEASurement<Instance>:DIGital:TETRa:POWer:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.Tetra.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: