Fmoddepth
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:FMODdepth<nr>

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:ILS:LOCalizer:AFSettings:FMODdepth<nr>



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Ils.Localizer.AfSettings.Fmoddepth.FmoddepthCls
	:members:
	:undoc-members:
	:noindex: