Maximum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:MAXimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:MAXimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:MAXimum
	READ:AFRF:MEASurement<Instance>:MEValuation:DEModulation:MDEPth:DELTa:MAXimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Demodulation.ModDepth.Delta.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: