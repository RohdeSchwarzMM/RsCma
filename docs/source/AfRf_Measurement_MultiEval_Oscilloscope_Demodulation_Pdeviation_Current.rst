Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:PDEViation:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:PDEViation:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:PDEViation:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:PDEViation:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.Demodulation.Pdeviation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: