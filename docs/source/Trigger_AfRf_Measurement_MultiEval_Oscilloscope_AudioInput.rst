AudioInput
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:SOURce
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:COUPling
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:STATe
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:ENABle
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:OFFSet
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:SLOPe
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:MODE
	single: TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:THReshold

.. code-block:: python

	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:SOURce
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:COUPling
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:STATe
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:ENABle
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:OFFSet
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:SLOPe
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:MODE
	TRIGger:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:AIN:THReshold



.. autoclass:: RsCma.Implementations.Trigger.AfRf.Measurement.MultiEval.Oscilloscope.AudioInput.AudioInputCls
	:members:
	:undoc-members:
	:noindex: