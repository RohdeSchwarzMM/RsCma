Current
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:USBLevel:CURRent
	single: READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:USBLevel:CURRent

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:USBLevel:CURRent
	READ:AFRF:MEASurement<Instance>:MEValuation:OSCilloscope:DEModulation:USBLevel:CURRent



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Oscilloscope.Demodulation.UsbLevel.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: