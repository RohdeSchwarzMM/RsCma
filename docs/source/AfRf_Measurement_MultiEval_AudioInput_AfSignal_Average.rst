Average
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:AVERage
	single: READ:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:AVERage

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:AVERage
	READ:AFRF:MEASurement<Instance>:MEValuation:AIN<Nr>:AFSignal:AVERage



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.AudioInput.AfSignal.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: