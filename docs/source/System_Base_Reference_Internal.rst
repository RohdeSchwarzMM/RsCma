Internal
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SYSTem:BASE:REFerence:INTernal:LIRange

.. code-block:: python

	SYSTem:BASE:REFerence:INTernal:LIRange



.. autoclass:: RsCma.Implementations.System.Base.Reference.Internal.InternalCls
	:members:
	:undoc-members:
	:noindex: