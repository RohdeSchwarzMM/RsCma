Display
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:BASE:DISPlay:STATe

.. code-block:: python

	CONFigure:BASE:DISPlay:STATe



.. autoclass:: RsCma.Implementations.Configure.Base.Display.DisplayCls
	:members:
	:undoc-members:
	:noindex: