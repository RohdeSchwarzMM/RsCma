Tones
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.TonesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.multiEval.tones.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_MultiEval_Tones_AudioInput.rst
	AfRf_Measurement_MultiEval_Tones_Dcs.rst
	AfRf_Measurement_MultiEval_Tones_Demodulation.rst
	AfRf_Measurement_MultiEval_Tones_SpdifLeft.rst
	AfRf_Measurement_MultiEval_Tones_SpdifRight.rst
	AfRf_Measurement_MultiEval_Tones_Voip.rst