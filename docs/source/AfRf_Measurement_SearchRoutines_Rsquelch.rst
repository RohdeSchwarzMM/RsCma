Rsquelch
----------------------------------------





.. autoclass:: RsCma.Implementations.AfRf.Measurement.SearchRoutines.Rsquelch.RsquelchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.searchRoutines.rsquelch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_SearchRoutines_Rsquelch_Hysteresis.rst
	AfRf_Measurement_SearchRoutines_Rsquelch_Llist.rst
	AfRf_Measurement_SearchRoutines_Rsquelch_OfLevel.rst
	AfRf_Measurement_SearchRoutines_Rsquelch_OfsQuality.rst
	AfRf_Measurement_SearchRoutines_Rsquelch_OnLevel.rst
	AfRf_Measurement_SearchRoutines_Rsquelch_OnsQuality.rst
	AfRf_Measurement_SearchRoutines_Rsquelch_SignalQuality.rst
	AfRf_Measurement_SearchRoutines_Rsquelch_Tlevel.rst