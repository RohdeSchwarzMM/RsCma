Fdeviation
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:REFerence:FDEViation:ENABle
	single: SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:REFerence:FDEViation

.. code-block:: python

	SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:REFerence:FDEViation:ENABle
	SOURce:AVIonics:GENerator<Instance>:VOR:AFSettings:REFerence:FDEViation



.. autoclass:: RsCma.Implementations.Source.Avionics.Generator.Vor.AfSettings.Reference.Fdeviation.FdeviationCls
	:members:
	:undoc-members:
	:noindex: