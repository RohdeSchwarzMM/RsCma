Sout
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SOUT:SOURce

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SOUT:SOURce



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.Sout.SoutCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.afRf.measurement.sout.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_AfRf_Measurement_Sout_Enable.rst
	Configure_AfRf_Measurement_Sout_Level.rst