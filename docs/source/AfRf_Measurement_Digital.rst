Digital
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: INITiate:AFRF:MEASurement<Instance>:DIGital
	single: STOP:AFRF:MEASurement<Instance>:DIGital
	single: ABORt:AFRF:MEASurement<Instance>:DIGital

.. code-block:: python

	INITiate:AFRF:MEASurement<Instance>:DIGital
	STOP:AFRF:MEASurement<Instance>:DIGital
	ABORt:AFRF:MEASurement<Instance>:DIGital



.. autoclass:: RsCma.Implementations.AfRf.Measurement.Digital.DigitalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.afRf.measurement.digital.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	AfRf_Measurement_Digital_Dmr.rst
	AfRf_Measurement_Digital_PtFive.rst
	AfRf_Measurement_Digital_State.rst
	AfRf_Measurement_Digital_Tetra.rst
	AfRf_Measurement_Digital_Ttl.rst