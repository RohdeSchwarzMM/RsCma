Minimum
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:MINimum
	single: READ:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:MINimum

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:MINimum
	READ:AFRF:MEASurement<Instance>:MEValuation:FFT:AIN<Nr>:POWer:MINimum



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Fft.AudioInput.Power.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: