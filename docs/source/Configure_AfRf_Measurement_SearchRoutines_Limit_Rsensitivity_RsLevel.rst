RsLevel
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:RSENsitivity:RSLevel

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:SROutines:LIMit:RSENsitivity:RSLevel



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.SearchRoutines.Limit.Rsensitivity.RsLevel.RsLevelCls
	:members:
	:undoc-members:
	:noindex: