ThDistortion
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:VOIP:THDistortion

.. code-block:: python

	CONFigure:AFRF:MEASurement<Instance>:MEValuation:LIMit:VOIP:THDistortion



.. autoclass:: RsCma.Implementations.Configure.AfRf.Measurement.MultiEval.Limit.Voip.ThDistortion.ThDistortionCls
	:members:
	:undoc-members:
	:noindex: