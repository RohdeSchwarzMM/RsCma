Sequence
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP:SEQuence
	single: READ:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP:SEQuence

.. code-block:: python

	FETCh:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP:SEQuence
	READ:AFRF:MEASurement<Instance>:MEValuation:TONes:VOIP:SEQuence



.. autoclass:: RsCma.Implementations.AfRf.Measurement.MultiEval.Tones.Voip.Sequence.SequenceCls
	:members:
	:undoc-members:
	:noindex: