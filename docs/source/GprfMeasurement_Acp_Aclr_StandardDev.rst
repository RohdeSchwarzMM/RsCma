StandardDev
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:ACP:ACLR:SDEViation
	single: READ:GPRF:MEASurement<Instance>:ACP:ACLR:SDEViation
	single: CALCulate:GPRF:MEASurement<Instance>:ACP:ACLR:SDEViation

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:ACP:ACLR:SDEViation
	READ:GPRF:MEASurement<Instance>:ACP:ACLR:SDEViation
	CALCulate:GPRF:MEASurement<Instance>:ACP:ACLR:SDEViation



.. autoclass:: RsCma.Implementations.GprfMeasurement.Acp.Aclr.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: