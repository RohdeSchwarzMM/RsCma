RfSettings
----------------------------------------



.. rubric:: SCPI Command:

.. index::
	single: SOURce:XRT:GENerator<Instance>:RFSettings:FREQuency
	single: SOURce:XRT:GENerator<Instance>:RFSettings:LEVel
	single: SOURce:XRT:GENerator<Instance>:RFSettings:PEPower

.. code-block:: python

	SOURce:XRT:GENerator<Instance>:RFSettings:FREQuency
	SOURce:XRT:GENerator<Instance>:RFSettings:LEVel
	SOURce:XRT:GENerator<Instance>:RFSettings:PEPower



.. autoclass:: RsCma.Implementations.Source.Xrt.Generator.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.xrt.generator.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Xrt_Generator_RfSettings_Connector.rst